import data

rule eddy_flux_plot:
    input: budget=data.files_budget('2018-05-12-A', dim=3),
           stat=data.files_stat("2018-05-12-A"),
           tb="data/output/TB.2018-05-12-A.nc"
    output: "reports/paper/eddy.pdf"
    script: "../scripts/plots/wave_frame_avg.py"

