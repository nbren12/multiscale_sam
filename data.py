import os, re, glob
from toolz import curry


def snakemake_decorator(fun):
    def f(arg, **kwargs):
        try:
            run = arg.run
        except AttributeError:
            run = arg
        return fun(run, **kwargs)

    return curry(f)


data_path = os.path.join(os.path.dirname(__file__), 'data', 'raw')

dirs_dict = {2: 'OUT_2D', 3: 'OUT_3D'}


@snakemake_decorator
def files(run, dim=2):
    d = os.path.join(data_path, run, dirs_dict[dim], '*.nc')
    pat = re.compile(r"MSE")
    for f in sorted(glob.glob(d)):
        if not pat.search(f):
            yield f


@snakemake_decorator
def files_budget(run, dim=2):
    d = os.path.join(data_path, run, dirs_dict[dim], '*.nc')
    pat = re.compile(r"MSE")
    for f in sorted(glob.glob(d)):
        if pat.search(f):
            yield f


@snakemake_decorator
def files_stat(run):
    d = os.path.join(data_path, run, 'OUT_STAT', '*.nc')
    return glob.glob(d)


def list_files_recursive(path):
    files = []
    for dirpath, dirnames, filenames in os.walk(path):
        for file in filenames:
            files.append(os.path.join(dirpath, file))
    return files


def files_archive(run):
    path = os.path.join(data_path, run)
    filenames = set(list_files_recursive(path))
    # remove full 3D outputs
    return filenames - set(files(run, dim=3))


