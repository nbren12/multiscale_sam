import os
import subprocess
import argparse
import time
import data


def parse_arguments():
    parser = argparse.ArgumentParser(description='Archive run')
    parser.add_argument("run")
    parser.add_argument("-z", help="Compress archive", action='store_true')
    return parser.parse_args()


args = parse_arguments()

tar_root = "data/raw"

# get files to archive
files = data.files_archive(args.run)
total_size = sum(map(os.path.getsize, files))
print(f"Archiving {len(files)} files")

files = [os.path.relpath(file, tar_root)
         for file in files]
print(f"Total Archive Size: {total_size/1e9} GB")

tar_opts = ['-v']
if args.z:
    ext = 'tar.gz'
    tar_opts.append('-z')
else:
    ext = 'tar'

tarball = os.path.join(f"data/{args.run}.{ext}")

print(f"Saving Archive to {tarball}")
subprocess.call(['tar', '-C', tar_root] +  tar_opts + ['-cf', tarball] + list(files))
