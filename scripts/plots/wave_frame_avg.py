from functools import partial

import matplotlib as mpl
mpl.use("Agg")

import matplotlib.pyplot as plt
import numpy as np
from toolz import curry

import data
import xarray as xr
import xnoah
from lib import anelastic_eddy_flux_div, coarsen, lowpass
from lib.budget import mass_weighted_integral, vertical_average


def open_data(budget, stat, tb):
    """Open and merge neccessary netCDF files
    """
    # open 3d files
    fields = ['U', 'p', 'W']
    path = list(budget)
    ds = xr.open_mfdataset(path)

    # get rho and p from stat file
    stat = xr.open_mfdataset(stat)
    rho = stat.RHO.isel(time=0)
    p = stat.p

    # open and coarsen brightness temperature
    TB = coarsen(xr.open_dataset(tb).TB, x=64)
    TB['x'] = ds.x

    return ds[fields].assign(p=p, rho=rho, TB=TB)


def calculations(ds, dof_synoptic=26):
    """Compute eddy flux and KE^M"""
    rho = ds.rho
    wm = ds.W - lowpass(ds.W, dof=dof_synoptic)
    um = ds.U - lowpass(ds.U, dof=dof_synoptic)
    ef = -anelastic_eddy_flux_div(wm, um, rho) * 86400
    ef.attrs['units'] = 'm/s/d'
    ef.attrs['long_name'] = 'Mesoscale vertical eddy flux convergence'
    ke_m = vertical_average(um**2/2, rho)\
           .assign_attrs(units='J/kg')

    return ds.assign(KEM=ke_m, EDDY=ef, UP=ds.U - ds.U.mean('x'))


@curry
def wave_mean(x, speed):
    ds = xnoah.xarray.phaseshift_regular_grid(x, speed)
    return ds.mean('time')


def subplot_up_eddy(wave_avgs, ax=None, cax=None):

    if ax is None:
        ax = plt.gca()

    x, p = wave_avgs.x, wave_avgs.p

    div_levels = np.r_[-3:4] * 1
    u_levels = np.r_[-3:4] * 5
    im = ax.contourf(x, p, wave_avgs.EDDY.T, levels=div_levels, cmap='RdBu_r')
    plt.colorbar(im, cax=cax)
    ax.contour(x, p, wave_avgs.UP, levels=u_levels, colors='black')

    ax.set_ylim([p.max(), p.min()])

    return im


def plot_wave_avgs(wave_avgs):
    # make units of x in 1000 km
    wave_avgs = wave_avgs.assign_coords(x=wave_avgs.x / 1e6)

    fig = plt.figure(figsize=(3.9, 5))
    gs = plt.GridSpec(
        3, 2, width_ratios=[.95, .05], height_ratios=[1, 1, 2], wspace=.05)

    # setup axes
    ax_contour = fig.add_subplot(gs[2, 0])
    ax_cbar = fig.add_subplot(gs[2, 1])
    ax_tb = fig.add_subplot(gs[1, 0])
    ax_kem = fig.add_subplot(gs[0, 0])

    # plot contour
    subplot_up_eddy(wave_avgs, ax=ax_contour, cax=ax_cbar)

    ax_kem.plot(wave_avgs.x, wave_avgs.KEM)
    ax_tb.plot(wave_avgs.x, wave_avgs.TB)

    # styling axes
    for ax in [ax_tb, ax_kem]:
        ax.set_xlim([0, wave_avgs.x.max()])
        ax.set_xticklabels('')
        for spine in ['bottom', 'top', 'right']:
            ax.spines[spine].set_visible(False)

    # Add labels
    ax_kem.text(.03, .85, 'a) $KE^M$', size=12, transform=ax_kem.transAxes)
    ax_kem.set_ylabel('J/kg')

    ax_tb.set_ylabel('K')
    ax_tb.text(.03, .85, 'b) TB', size=12, transform=ax_tb.transAxes)

    ax_contour.text(
        .03,
        .85,
        'c)',
        size=12,
        transform=ax_contour.transAxes,
        bbox=dict(fc='white', ec='w'))
    ax_contour.set_ylabel('p (hPa)')
    ax_contour.set_xlabel('x (1000 km)')
    ax_cbar.set_label("m/s/day")


def main(output, *args):
    ds = (open_data(*args).sel(time=slice(40, 65)).load().pipe(calculations))

    # wave speed
    c = (2.8e7 - .25e7) / (65 - 40)

    wave_avgs = (ds[['TB', 'UP', 'EDDY', 'KEM']].apply(wave_mean(speed=c))
                 .assign(p=ds.p))

    plot_wave_avgs(wave_avgs)
    plt.subplots_adjust(left=.19, right=.9, top=.95)
    plt.savefig(output)


if __name__ == '__main__':
    i = snakemake.input
    main(snakemake.output[0], i.budget, i.stat, i.tb)
