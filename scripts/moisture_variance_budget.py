import xarray as xr
from lib.budget import compute_budget
from lib.calculus import divergences

files = snakemake.input
output = snakemake.output[0]

# files = snakemake.input
budget = xr.open_mfdataset(files)\
           .isel(time=slice(0, None, 5))\
           .load()

# get variables
rho = budget.RHO[0, :, 0].drop('time')
q = budget.QTOT
u = budget.U
w = budget.W
eddy = budget.QTOTEDDY

# compute derivatives
F = budget.QTOTFLUX + rho * q.mean('x') * w
VDIV, HDIV = divergences(u, q, F, rho)
out = compute_budget(
    q,
    MISC=budget.QTOTMISC,
    V=-VDIV,
    H=-HDIV,
    TURB=HDIV + VDIV + eddy,
    LSF=budget.QTOTLSF,
    SED=budget.QTOTSED)
out.to_netcdf(output)
