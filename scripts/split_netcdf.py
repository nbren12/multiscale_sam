#!/usr/bin/env python
import os
import xarray as xr
import argparse

def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('netcdfs', nargs='+')
    parser.add_argument('-v', '--variables')
    parser.add_argument('-o', '--output', help="output directory",
                        default=".")

    return parser.parse_args()

args = parse_arguments()
output = args.output

if len(args.netcdfs) > 1:
    ds = xr.open_mfdataset(args.netcdfs)
else:
    ds = xr.open_dataset(args.netcdfs[0])


try:
    os.mkdir(output)
except FileExistsError:
    pass


if args.variables:
    variables = args.variables.split(',')
else:
    variables = ds.variables

for variable in variables:
    print(f"Saving {variable}")
    ds[[variable]].to_netcdf(os.path.join(output, str(variable) + ".nc"))

