import xarray as xr
from lib.material import matderiv

input = snakemake.input
output = snakemake.output

fld = xr.open_dataset(input.fld)
vel = xr.open_dataset(input.vel)
u = vel.U1
w = vel.W1

out = {}
for v in fld.data_vars:
    f = fld[v].squeeze()
    if set(f.dims) == set(['x', 'time', 'z']):
        out[v] = matderiv(u, w, f)

xr.Dataset(out)\
    .to_netcdf(output[0])
