import xarray as xr
from lib import budget
from lib.calculus import divergences, centderiv


def ape_budget(ds, rho):
    t0 = ds.TABS.mean('x')
    tz = centderiv(t0, 'z')
    g = 9.81
    gammd = g/1004
    coef = g /t0 / (tz + gammd)

    # only compute APE for points with stable base state
    coef = coef.where(tz + gammd > 1e-5, 0)

    # get dataarray with budget terms alone
    fields = ['SLIEDDY', 'SLIMISC', 'SLILSF', 'SLISED', 'QRADLW', 'QRADSW',
              'SLISTOR']
    sli = ds['SLI']
    sli_budget = ds[fields]
    sli_budget = [(key, sli_budget[key], {}) for key in sli_budget.data_vars]

    # call tracer budget
    sli_budget_scales = budget.tracer_budget(sli, sli_budget)

    # compute the vertical integral
    apeb_scales = budget.vertical_average(coef*sli_budget_scales, rho)

    # compute KE->APE Conversion
    wb = budget.product(ds.WBUOY, ds.W)
    CON = - budget.vertical_average(wb, rho)
    ADV = apeb_scales['SLIEDDY'] - CON
    apeb_scales = apeb_scales.drop('SLIEDDY')\
                             .assign(ADV=ADV, CON=CON)

    # only plot largest scales
    apeb_scales = apeb_scales.sel(scale=['3', '26'])

    return apeb_scales


def preprocess(ds):
    rho = ds.RHO.isel(x=0,time=0)
    b = ape_budget(ds, rho)
    return b


outputs = []

for file in sorted(snakemake.input):
    print(f"Processing {file}")
    # needed fields
    fields = ['SLIEDDY', 'SLIMISC', 'SLILSF', 'SLISED', 'QRADLW', 'QRADSW',
              'SLISTOR', 'SLI', 'WBUOY', 'W', 'RHO', 'TABS']

    ds = xr.open_dataset(file)[fields].load()
    rho = ds.RHO.isel(x=0,time=0)
    outputs.append(ape_budget(ds, rho))


xr.concat(outputs, 'time').sortby('time')\
    .to_netcdf(snakemake.output[0])

