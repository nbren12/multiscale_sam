#!/bin/sh
# This script downloads the dataset from prince using globus
# It should be executed from the root of this project

# Insert the names of the ids that should be downloaded
# using globus
transfer_ids=1

# this is the id of the new york university prince supercomputer
# end point in globus
prince_id=83640fe8-c7a7-11e6-9c35-22000a1e3b52

# you should change this match the id for your personal computer
local_id=f4356b9c-c9a2-11e7-9596-22000a8cbd7d


src_dir=/archive/ndb245/Data/id
dest_dir=$(pwd)/data/raw/


for id in $transfer_ids
do
    globus transfer -r -s checksum $prince_id:$src_dir/$id $local_id:$dest_dir/$id
done
