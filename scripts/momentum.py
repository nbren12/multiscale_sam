import xarray as xr
from lib.budget import compute_budget
from lib.calculus import divergences

files = snakemake.input
output = snakemake.output[0]

# files = snakemake.input
budget = xr.open_mfdataset(files)\
           .isel(time=slice(0, None, 10))\
           .load()

# get variables
rho = budget.RHO[0, :, 0].drop('time')
u = budget.U
w = budget.W
eddy = budget.UEDDY

# compute derivatives
rhowu = budget.RHOWU + rho * u.mean('x') * w
VDIV, HDIV = divergences(u, u, rhowu, rho)
out = compute_budget(
    u,
    PGRAD=budget.UPGRAD,
    UMISC=budget.UMISC,
    V=-VDIV,
    H=-HDIV,
    TURB=HDIV + VDIV + eddy)
out.to_netcdf(output)
