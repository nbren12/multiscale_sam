#!/usr/bin/env python
"""Multiscale decomposition using penalized fourier method
"""
import time

import xarray as xr

from lib.filter import lowpass


def main(infile, outfile, dim, dof):
    if isinstance(dof, str):
        dof = float(dof)

    d = xr.open_dataset(infile)

    def f(A):
        if dim in A.dims:
            return lowpass(A, dim, dof)
        else:
            return A

    t1 = time.time()
    d.apply(f).to_netcdf(outfile)
    print("Elapsed: ", time.time() - t1)


try:
    main(snakemake.input[0], snakemake.output[0], snakemake.params.dim,
         snakemake.params.dof)
except NameError:
    pass
