import xarray as xr
from lib.budget import compute_vertical_triads


def open_budget_data():
    i=snakemake.input
    return xr.Dataset(dict(
    w = xr.open_dataarray(i.w),
    u = xr.open_dataarray(i.u),
    rho = xr.open_mfdataset(i.stat).RHO[-1]
    ))

ds = open_budget_data()
df = compute_vertical_triads(ds.rho, ds.w, ds.u, dofs=(3, 26))
df.to_csv(snakemake.output[0])
