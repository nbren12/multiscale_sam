#!/bin/sh
# filename='bigfile.txt'
# r = requests.put('%s/%s' % (bucket_url,filename),
#                  data=open(filename, 'rb'),
#                  headers={"Accept":"application/json",
#                           "Authorization":"Bearer %s" % ACCESS_TOKEN,
#                           "Content-Type":"application/octet-stream"})

file=$1
name=$(basename file)

bucket=https://zenodo.org/api/files/3533bb8c-8bcf-4f97-826b-f8a852d20b15
curl  -H "Authorization: Bearer $ZENODO_TOKEN" -H "Accept: application/json" -H "Content-Type: application/octet-stream" \
      --progress-bar \
      -o $name.curl.out \
      -X PUT $bucket/$basename -T $file
