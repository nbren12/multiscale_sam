import xarray as xr
from lib.thermo import virtual_temperature, moist_static_energy

def load_3d_data_with_calcs(id="1"):
    """Load all the 3d fields and compute some additional thermodynamic quantities.

    These quantities included are U,V,W,TABS,MSE,Bouyancy(B)
    """

    # open all the 3d data
    data = xr.open_mfdataset(
        f"data/raw/{id}/data/3d/*.nc", chunks={'time': 1})
    # open pressure
    p = xr.open_dataset(f"data/raw/{id}/data/stat.nc").p

    # patch the 3d data with the z variable from p
    data['z'] = p.z

    # compute bouyancy
    tv = virtual_temperature(data.TABS, data.QV)
    tv0 = tv.mean(['x'])
    B = 9.81 * (tv - tv0) / tv0
    B.attrs['units'] = 'm/s^2'

    data = data.assign(B=B, MSE=moist_static_energy(data.TABS, data.QV))

    return data



run = snakemake.wildcards.run
data = load_3d_data_with_calcs(run)
data.to_netcdf(snakemake.output[0])
