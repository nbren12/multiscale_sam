import glob
from lib.patch_z import patch_z_data

id = snakemake.wildcards.id
statfile =  f"data/raw/{id}/data/stat.nc"

for f in glob.glob(f"data/raw/{id}/data/3d/*.nc"):
    print(f"Patching z from {statfile} into {f}")
    patch_z_data(f, statfile, 'z')
