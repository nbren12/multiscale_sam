import xarray as xr
from xnoah.sam import coarsen

blocks = snakemake.config['blocks']
input = snakemake.input
output = snakemake.output

name = snakemake.wildcards.f

kwargs = {}

if name == '3d/U':
    kwargs.update(dict(stagger_dim='x', mode='wrap'))

A = xr.open_dataarray(input[0])
coarsen(A, blocks=blocks, **kwargs)\
    .to_netcdf(output[0])
