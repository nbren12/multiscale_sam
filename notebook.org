#+LATEX_HEADER: \usepackage{siunitx}
* INBOX
** DONE run SAM on slurm
   CLOSED: [2017-12-15 Fri 01:33]

***  How many processors to use? 
    [[https://wikis.nyu.edu/display/NYUHPC/Clusters+-+Prince][This html table]] has a 
   
   
   
** DONE longer homogeneous SST run
   CLOSED: [2017-12-15 Fri 01:33]
* Journal
** 2017-03-02 

   Worked on thermodynamics of SAM. I implemented a python script which can
   compute the partial derivatives of T with respect to the prognostic variables
   of SAM.

** 2017-03-05

   Started a bunch of runs: see [[*SAM runs][SAM runs]]

   Worked on processing 3D output and computing virtual temperature using dask.
   The code took almost 40  minutes to run when using =da.to_netcdf()= as
   opposed to 15 minutes for =da.data.to_hdf5()=  run individually.

** 2017-03-06

   Fixed the SAM runs to output THV and use bin format.

   dB = d((Tv-Tv0)/Tv0) = d(Tv/Tv0 -1) \approx  1/TV0 dTv - Tv dTv0 / TV0^2
      = 1/tv0 (dTv - Tv/Tv0 dTv0) = 1/ tv0/cp ( cp dTv - Tv/Tv0 dTv0) 
     = 1/ tv0/cp ( Q + LC + g Tv/Tv0 w  - Tv/Tv0 dTv0)

   cp/Tv0 dTv - Tv     cp Tv dTv0/Tv0^2

** 2017-03-15

   1. Fixed the large scale forcings file in SAM to have radiative cooling
      throughout the troposphere. Previously, the -1.5 K/day cooling was only
      below 200 hPa, but the tropopause is at about 100 hPA in the model.
   2. worked on snakemake scripts:
      1. output coarse-grained OUT_3D data
      2. looked at Q_1 and Q_2 as inferred from the data.
   3. Learned more about the SAM output:
      1. RHO in stats is constant in time. Use this to convert precipitation to
         W/m2: $P L \rho_0$ has units \SI{}{\watt\per m^2}
      


   #+BEGIN_SRC python
     from metpy.constants import Lv, Cp_d
     from metpy.units import units

     Q_ = units.Quantity

     P = Q_(10, "mm/day")  * Q_(1.2, "kg/m^3") * Lv 
     P.to("W/m^2")
   #+END_SRC

** 2017-03-20 

   Showed that B-splines do not work very well for the planetary-synoptic scale
   decomposition. I am going to use EOFs for the spatial scale and B-splines for
   the temporal scale instead.

   
