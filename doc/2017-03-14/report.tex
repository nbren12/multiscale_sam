% Created 2017-02-27 Mon 11:04
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{subfig}
\graphicspath{{figs/}}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
% \usepackage{palatino}
 \usepackage{amsmath}
\usepackage{kpfonts}
%\usepackage{textcomp}
\usepackage{todonotes}
\usepackage{amssymb}
%\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{siunitx}
 \usepackage[margin=1in]{geometry}
\usepackage[backend=biber,
style=ieee,
isbn=false,
url=false
]{biblatex}
\addbibresource{zotero.bib}


\usepackage{rotating} % for landscape figures
\usepackage{dot2texi}

\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepgfplotslibrary{groupplots}
\author{Noah D. Brenowitz and Andrew J. Majda}
\date{March 22, 2017}
\title{Multiscale Interactions in Two-dimensional Cloud-resolving Models II}
\hypersetup{
 pdfauthor={Noah D. Brenowitz},
 pdftitle={Multiscale Interactions in Two-dimensional Cloud-resolving Models},
 pdfkeywords={},
 pdfsubject={},
 pdflang={English}}

\newcommand{\totd}[2]{\frac{D #1}{D #2}}
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\bu}{\mathbf{u}}
\newcommand{\ub}{\bar{u}}
\newcommand{\tavg}[1]{\langle #1 \rangle}
\newcommand{\bavg}[1]{\langle \overline{#1} \rangle}
\newcommand{\brunt}{Brunt-V\"ais\"al\"a\ }

\newcommand\inputpgf[2]{{
\let\pgfimageWithoutPath\pgfimage
\renewcommand{\pgfimage}[2][]{\pgfimageWithoutPath[##1]{#1/##2}}
\input{#1/#2}
}}

\graphicspath{{./}{2017-03-09/}}
\usepackage{layouts}
\begin{document}
\maketitle

\section*{Summary and Conclusions}

\begin{itemize}
\item Exact version of the buoyancy eqaution is derived based on the appropriate
  anelastic energy equation as discussed in \textcite{pauluis2008}. 

  
\item Non-dimensionalized the output of the SAM model.

\item Spline-based averaging is effective at separating the mesocale and the
  synoptic scale, but is not good at synoptic-planetary decomposition in the
  Walker cell simulation.
\item To address the problems with spline-based averaging, an EOF based
  projection technique is introduced. This techinque is able to adapt to the
  fact that the planetary-scales seems slightly smaller in the warm-pool than
  elsewhere in the domain. By adapting to the scales which are already present
  in the data, this approach achieves good planetary-synoptic scale separation
  for $u$ and $B$.

\item The $H$ and $w$ which are averaged over \SI{64}{\km} subdomains have very
  strong nondimensional amplitudes of $1-10$, which violates the basic IMMD
  ansatz. As expected, the amplitude decreases when a synoptic-scale
  coarse-graining is applied. This implies that a synoptic-scale
  smoothing/coarse-graining should be applied to the data prior to analysis. It
  is not as important that this synoptic-scale coarse graining be a Reynold's
  average.
\end{itemize}

\section*{Questions}
\label{sec:quest}

\begin{itemize}
\item Does the synoptic scale smoothing need to be a Reynold's average even if
  we are not using it for eddy fluxes?
\item How important are the effects of the surface pressure gradients  $p_s$
  compared to the buoyancy driven hydrostatic pressure gradient?
\end{itemize}

\newpage
\tableofcontents



\section{Intraseasonal multi-scale moist dynamics (IMMD)}
\label{sec:orgd8d1518}


\subsection{Parent Model}

\textcite{biello_intraseasonal_2010} presents an asymptotic theory for
multi-scale interactions between the planetary and synoptic scale flows in the
tropical atmosphere. This theory is valid in the presence of moisture and a
planetary scale circulation with a magnitude of around
\SI{50}{\meter\per\second}. The basic non-dimensional equations used as a starting
point for the theory are given by
\begin{subequations}
    \label{eq:immd-nondim}
  \begin{align}
    \totd{u}{t}  -yv =  - p_x - \epsilon d u \\
    \totd{v}{t}  +yu =  - p_y - \epsilon d v \\
    \totd{\theta}{t}  +w =  \epsilon H - \epsilon d_{\theta} \theta \\
    \totd{q}{t}  -Q_0 w =  - \epsilon H\\
    p_z = \theta\\
    u_x+v_y + w_z = 0.
  \end{align}
\end{subequations}
The Newtonian relaxation terms in the velocity and temperature equations are
simply meant to account for sub-grid-scale drag and radiative cooling. Their
precise mathematical form is not important, and they can be seen as a
placeholder for more complex radiative transfer and cumulus drag dynamics.

In two dimensions without rotation, it is not clear what the scales are that
should be used for non-dimensionalization. In
\textcite{biello_intraseasonal_2010}, the authors use the equatorial deformation
scale scale $L_d= \sqrt{NH/\beta}$ to nondimensionalize the equations, so I
guess I will use the same scales, just to check that the asympotics work. The
non-dimensional equations are obtained assuming that $L\sim L_d=\SI{1500}{\km}$,
$U\sim NH_T=\SI{50}{\m\per\s}$, and $T=L_d/c=\SI{8.3}{\hour}$.

Even though there is no coriolis force in some of the 2D simulations, the
asymptotics should still be valid if these scales are present in the 2D
simulations. Thus, the most important question is: \emph{are these scales
consistent with those seen in the two dimensional walker circulation and
homogeneous SST simulations?}


\subsection{Multi-scale decomposition (3D)}
\citetitle{biello_intraseasonal_2010} is the foundational paper for this section
of the report. Of key relevance is the basic asympotic assumptions they make
about the velocity, temperature, and humidity fields. They begin by introducing
the ratio of synoptic scale latent heating to the non-dimensional unit for
latent heating as a crucial small parameter. This quantity can be more
conveniently expressed in humidity units the output variables of a CRM by
\begin{equation}
  \label{eq:eps}
  \epsilon = \frac{\text{Measured $C_d - E_r$  on synoptic scales}}{\SI{40}{\g\per\kg\per\day}}.
\end{equation}

\begin{description}
\item[Assumption 1] $\epsilon \ll 1$
\item[Assumption 2] The following asymptotic expansion is assumed for the
  various model variables:
  \begin{subequations}
      \label{eq:ansatz}
    \begin{alignat}{3}
      u^{\epsilon} &= U(X,T) +& \epsilon (u'(x,X,t,T) + \ub(X,t,T)) + \epsilon^2 u_2\\
      v^{\epsilon} &= & \epsilon (v'(x,X,t,T) + \bar{v}(X,t,T)) + \epsilon^2 v_2\\
      w^{\epsilon} &= & \epsilon (w'(x,X,t,T) + \bar{w}(X,t,T)) + \epsilon^2 w_2\\
      \theta^{\epsilon} &= \Theta(X,T) +& \epsilon (\theta'(x,X,t,T) + \bar{\theta}(X,t,T)) + \epsilon^2 \theta_2\\
      q^{\epsilon} &= Q(X,T) +& \epsilon (q'(x,X,t,T) + \bar{q}(X,t,T)) +
      \epsilon^2 q_2
    \end{alignat}
  \end{subequations}
\end{description}

Using these assumptions, and a simplified non-dimensional form of the equations
of motion, the authors separate the dynamics into the first order Trade Winds
and Hadley Circulation (TH) component, a synoptic (S) component, and a planetary
fast-time component (P).

\paragraph{Trade-wind Hadley-cell (TH)}

\begin{subequations}
    \label{eq:th}
  \begin{align}
    \totd{U}{T} - y V + P_X = -dU\\
    y U + P_y = 0\\
    P_z = \Theta \\
    \totd{\Theta}{T} + W = \tavg{\bar{H}} - d_{\theta} \Theta\\
    \totd{Q}{T} - Q_0 W = -\tavg{\bar{H}}\\
    U_X + V_y + W_z = 0
  \end{align}
\end{subequations}
where $V = \tavg{\bar{v}}$ and $W = \tavg{\bar{w}}$.

\paragraph{Synoptic Fluctuation (S)}

\newcommand{\lhssyn}[2]{%
  #1'_t + U #1'_x +(u'#2)_x+ (v'#2)_y + (w'#2)_z}

\begin{subequations}
    \label{eq:S}
  \begin{alignat}{3}
    \lhssyn{u}{U} - y v' + p'_x &= 0\\
    v'_t + U v'_x +yu' + p'_y &= 0\\
    \lhssyn{\theta}{\Theta}  &= H' \\
    \lhssyn{Q}{\Theta}  &= Q_0 w' - H' \\
    p'_z &= \theta'\\
    u'_x  +v'_y + w'_z &= 0\\
  \end{alignat}
\end{subequations}
\paragraph{Planetary scale anomalies from TH (P)}

Equation (3.18) gives the fast time scale modulation of the planetary anomalies.
These equations are reproduced here:
\newcommand{\tb}[1]{\tilde{\bar{#1}}}
\renewcommand{\lhssyn}[2]{%
  #1_t + (\tilde{\bar{v}}#2)_y+ (\tilde{\bar w} #2)_z }
\begin{subequations}
    \label{eq:immd-P}
  \begin{alignat}{3}
    \lhssyn{\tb u}{U} - y \tb v  &= 0\\
    \tb v _t + y\tb u + \tb{p}_y &= 0\\
    \lhssyn{\tb\theta}{\Theta}  + \tb{w} &= \tb{H} \\
    \lhssyn{\tb q}{Q}  -Q_0 \tb w &=- \tb H' \\
    \tb p_z &= \tb \theta\\
    \tb v_y + \tb w_z &= 0
  \end{alignat}
\end{subequations}
These equations notably, do not describe zonally propagating disturbance, but
they do allow for a response to a zonally propagating heat source.

The final important set of equations describe the intraseasonal dynamics of the
planetary scale $O(\epsilon)$ contributions. Introducing a slight notational
simplification, we let $u = \tavg{\bar u}$, and similarly for $p$, $q$, and
$\theta$.
The next order contributions are used to define $v= \tavg{\bar v_2}$ and $w =
\tavg{\bar w_2}$.
Then, the solvability condition for the $O(\epsilon^2)$ dynamics requires that
\renewcommand{\lhssyn}[2]{%
  \frac{D #1}{D T} + (u #2)_X + (v #2)_y+ (w #2)_z}
\begin{subequations}
  \begin{align}
    &\lhssyn{u}{U} - y v + p_X = F^u - du\\
    &y u + p_y = -d V\\
    &\lhssyn{\theta}{\Theta} + w = F^{\theta}  +  \tavg{\bar{H}_2}- d_\theta \theta\\
    &\lhssyn{q}{Q} - Q_0 w = F^{q} - \tavg{\bar{H}_2}\\
    &p_z  = \theta\\
    &u_X + v_y + w_z = 0,
  \end{align}
\end{subequations}
where the forcing terms on the right hand side are given by
\newcommand{\bbavg}[1]{\tavg{\overline{#1}}}
\newcommand{\fluximmd}[1]{ -\bbavg{\tilde v \tilde{#1} + v' #1 '}_y  -
  \bbavg{\tilde w \tilde{#1} + w' #1 '}_z}
\begin{subequations}
  \label{eq:fluxes-immd}
  \begin{align}
    F^u        &=\fluximmd{u}\\
    F^{\theta} &=\fluximmd{\theta}\\
    F^{q}      &=\fluximmd{q}.
  \end{align}
\end{subequations}

\subsection{Specialization to 2D}
\label{sec:org0a5c317}

The three dimensional equations have a simpler specialization to two dimensions.

\paragraph{Trade-wind Hadley-cell (TH)}

\begin{subequations}
    \label{eq:th2d}
  \begin{align}
    \totd{U}{T}  + P_X = -dU\\
    P_z = \Theta \\
    \totd{\Theta}{T} + W = \tavg{\bar{H}} - d_{\theta} \Theta\\
    \totd{Q}{T} - Q_0 W = -\tavg{\bar{H}}\\
    U_X  + W_z = 0
  \end{align}
\end{subequations}

\paragraph{Synoptic Fluctuation (S)}

\renewcommand{\lhssyn}[2]{%
  #1'_t + U #1'_x +(u'#2)_x+  (w'#2)_z}

\begin{subequations}
    \label{eq:S2d}
  \begin{alignat}{3}
    \lhssyn{u}{U}  + p'_x &= 0\\
    \lhssyn{\theta}{\Theta}  &= H' \\
    \lhssyn{Q}{\Theta}  &= Q_0 w' - H' \\
    p'_z &= \theta'\\
    u'_x  + w'_z &= 0\\
  \end{alignat}
\end{subequations}
\paragraph{Planetary scale anomalies from TH (P)}

\renewcommand{\lhssyn}[2]{%
  #1_t +  (\tilde{\bar w} #2)_z }
\begin{subequations}
    \label{eq:immd-P2d}
  \begin{alignat}{3}
    \lhssyn{\tb u}{U}   &= 0\\
    \lhssyn{\tb\theta}{\Theta}  + \tb{w} &= \tb{H} \\
    \lhssyn{\tb q}{Q}  -Q_0 \tb w &=- \tb H \\
    \tb p_z &= \tb \theta\\
    \tb w_z &= 0
  \end{alignat}
\end{subequations}

The following equation is for the first order correction to the planetary scale
circulation. Unfortunately it cannot be verified because it involves the second order heating,
which cannot be deduced from the data.
\renewcommand{\lhssyn}[2]{%
  \frac{D #1}{D T} + (u #2)_X +  (w #2)_z}
\begin{subequations}
  \begin{align}
    &\lhssyn{u}{U}  + p_X = F^u - du\\
    &\lhssyn{\theta}{\Theta} + w = F^{\theta}  +  \tavg{\bar{H}_2}- d_\theta \theta\\
    &\lhssyn{q}{Q} - Q_0 w = F^{q} - \tavg{\bar{H}_2}\\
    &p_z  = \theta\\
    &u_X + w_z = 0,
  \end{align}
\end{subequations}
It is mainly interesting because it is influenced by the following fluxes from the
synoptics scales.
\renewcommand{\bbavg}[1]{\tavg{\overline{#1}}}
\renewcommand{\fluximmd}[1]{- \bbavg{\tilde w \tilde{#1} + w' #1 '}_z}
\begin{subequations}
  \label{eq:fluxes-immd}
  \begin{align}
    F^u        &=\fluximmd{u}\\
    F^{\theta} &=\fluximmd{\theta}\\
    F^{q}      &=\fluximmd{q}.
  \end{align}
\end{subequations}

\section{Equations used by SAM}
\label{sec:sam}

The System for Atmospheric Modeling (SAM) uses a different set of prognostic
equations \autocite{khairoutdinov_cloud_2003}. These are given by

\begin{subequations}
    \label{eq:sam}
  \begin{align}
    &\totd{\bu}{t} + ( f  + \beta y) \mathbf{k}\times \bu = - \nabla \phi \\
    &\totd{w}{t} = -\nabla \phi + g B \label{eq:w-full}\\
    &\totd{h_L}{t}  = Q_{rad}  - \frac{1}{\rho_0} \pd{}{z} \left( L_c P_r + L_s P_s + L_s P_g \right)\\
    &\totd{q_T}{t} = -  \dot{q}_p^{\text{micro}} \label{eq:sam-qT}\\
    &\totd{q_p}{t} = \frac{1}{\rho_0} \pd{}{z} \left(  P_r +  P_s + P_g \right) + \dot{q}_p^{\text{micro}}\\
    &\nabla \cdot \bu + \frac{1}{\rho_0} \pd{\rho_0 w}{z}  = 0.
  \end{align}
\end{subequations}
I have neglected the sub-grid-scale flux terms in the formulation above since
they are not relevant to the problem considered in this report. The temperature
and humidity variables introduced in the previous section have been replaced
here by
\begin{description}
\item[Liquid/ice water static energy] $h_L = c_p T +gz - L_c (q_c + q_r) - L_s
  (q_i + q_s + q_g)$,
\item[Total nonprecipitating water] $q_T = q_v + q_c + q_i$, and
\item[Total precipitating water] $q_p = q_r + q_s + q_g$.
\end{description}
Note that $h_L$ is only an appropriate variable when hydrostatic balance or
anelastic equations are satisfied. Otherwise, potential temperature should be
used. There are a number of diagnostic equations which can be solved to
determine the temperature, and all the mixing ratios for the different species
of precipitating and non-precipitating water. Perhaps most importantly, the
water vapor is assumed to be at or below an approximate saturation value so that
\[q_v = \min(q_T, q_{sat}(p_0, T)),\]
where
\[ q_{sat} = q_{sw} (1-\omega_i) + \omega_i q_{si}\]
is a convex combination of the saturation mixing ratios of vapor with respect to
ice and liquid water. 

A consequence of this diagnostic relation is that the latent
heating term due to condensation is not explicitly calculated. Instead, SAM
models the microphysical processes which govern the conversion from condensate
($\text{cloud water} + \text{cloud ice}$) to precipitation ($\text{rain} +
\text{snow} + \text{grauppel}$). These processes are given by
\[
\dot{q}_p^{\text{micro}} = \text{accretion} - \text{evaporation} + \text{autoconversion}.
\]
A nice paper by \textcite{hernandez-duenas_minimal_2013} contains a more thorough
discussion of a similar set of microphysical equations, which can be helpful for
gaining intuition. Additional details about the diagnostic relations linking the
water species and the prognostic variables $q_T$, $h_L$, and $q_p$ are available
in Appendix \ref{sec:diag-q} and in the original SAM paper by \textcite{khairoutdinov_cloud_2003}.


This formulation is convenient for numerical modeling purposes, but
moist-convective processes are often  understood as a coupling
between dry dynamics and moist variables because the bouyancy is related
primarily to temperature, rather than moist static energy.
For instance, the circulation due to the monsoons and MJO can  be understood roughly as the response to a imposed heating pattern \autocite{gill_simple_1980}, and the multiscale theories of Biello and Majda rely upon asympotic analysis of the latent heating.

\subsection{Buoyancy equation}
\label{sec:buoyancy}

The appropriate thermodynamic equation for an anelastic atmosphere is given by
\autocite{pauluis2008}
\begin{equation}
  \label{eq:thermo-eq}
  c_p \totd{T}{t} + w (g  + B) = \dot{Q}.
\end{equation}
The bouyancy defined in terms of (virtual) temperature is given by
\begin{equation}
  \label{eq:bouyancy}
  B = g\frac{T_v- T_{v0}}{T_{v0}}.
\end{equation}
Assuming that the vitual temperature effect is small gives $T_v \approx T$, and
the material derivative of buoyancy can be computed by
\begin{align*}
  \totd{B}{t} &=  \frac{g}{T_0} \totd{T}{t}   -  \frac{gT}{T_0^2} \totd{T_0}{t} \\
              & =  \frac{g}{T_0} \totd{T}{t}   -  \frac{g  + B}{T_0} \totd{T_0}{t}\\
              & =  \frac{g}{c_p T_0} \left[ \dot{Q} - w ( g + B ) \right]   -  \frac{g  + B}{T_0} \pd{T_0}{z} w.
\end{align*}
Using the definition $N^2 = \frac{g}{T_0} \left[ \pd{T_0}{z} + \frac{g}{c_p}\right] $, and
employing \eqref{eq:thermo-eq}, the buoyancy equation can be written in the form
given by
\begin{equation}
  \label{eq:b2}
  \totd{B}{t} +  N^2\left( 1 + g^{-1} B \right) w     = \frac{g}{c_p T_0} \dot{Q}.
\end{equation}
Due to the anelastic approximation in this eqution, the $g^{-1} B$ term on the
left hand side is very small.

\subsubsection{Apparent Buoyance Source}
\label{sec:apparent-buoyance}

% TODO Add definition of HBAR

\section{Computational Methods}
\label{sec:diagnostics}

The parent model equations we wish to diagnose from the SAM simulations are given in
dimensional form by \eqref{eq:sam} and \eqref{eq:b2}. The multiscale analysis,
however, requires these equations to be cast in non-dimensional form. It turns
out the scales used to obtain the non-dimensional IMMD parent model
equations \eqref{eq:immd-nondim} match the scales observed in the SAM
simulations very well.

While the two-dimensional simulations do not have a prefered horizontal
length-scale due to rotation, the observed synoptic length-scale is around
$L=\SI{1500}{\km}$.
Given the horizontal length-scale, the tropopause height, and the Brunt-Vaisala frequency, it is possible to generate the other relevant scales in the following manner:

\begin{itemize}
\item
  \(L\): horizontal length scale
\item
  \(H= H_T/ \pi\): vertical length scale. The tropopause height will be
  inferred from the temperature data.
\item
  Horizontal velocity scale: this scale is given by the dry wave speed
  \(c= N H\). The timescale will then by \(T = L/U\).
\item
  Vertical velocity scales: \(W = H/T = c \frac{H}{L}\)
\item
  Bouyancy and temperature scales: $B = N^2 H $
\end{itemize}

Then, if the mean stratification and tropause height are the same as the IMMD
scales---$N = \SI{.01}{\per \s}$ and $H_T = \SI{16}{\km}$---all the
nondimensional scales are the same.
This is the case as can be seen in Figure \ref{fig:n2}.

\begin{figure}
  \centering
  \includegraphics{./2017-03-09/n2.pdf}
  \caption{Mean stratification profile ($N^2$) observed in the Walker cell
    setup. The vertical black line is given $N^2 =
    \SI{1e-4}{\per\square\second}$. The stratification shows an substantial
    increase around \SI{16}{\km}, which indicates that this is the tropopause.}
  \label{fig:n2}
\end{figure}

\subsection{Averaging operators and multi-scale decomposition}
\label{sec:orge0d3cb2}

When analyzing the output of a simulation, the full asymptotic expansion is not
available to us. Thus, the task is to reconstruct as many of the individual
terms in (\ref{eq:ansatz}) as possible using 1) the full fields
($u^{\epsilon}$), 2) an empirical spatial Reynold's average ( $\bar{\cdot}$ ),
and 3) an empirical temporal Reynold's average ($\tavg{\cdot}$).

By taking the temporal and spatial averages of the asympotic expansion, we
obtain
\begin{equation}
  \label{eq:xjfkadf}
  \bavg{u^{\epsilon}} = U(X,T) + \epsilon \tavg{\bar{u}}(x,X,t,T) + o(\epsilon).
\end{equation}
This indicates, that the zeroth and first order stationary components of the
planetary scale circulation cannot be separated. Subtracting this from the full
expansion gives
\begin{equation}
  \label{eq:kadvf}
  u^{\epsilon}- \bavg{u^{\epsilon}} =  \epsilon (\tilde{\bar u} + u') + o(\epsilon).
\end{equation}
The two components on the right hand side of this equation can be separated by
taking zonal averages. In summary, the only three significant quantities which
can be obtained from data are given by
\begin{alignat}{3}
  \label{eq:ansatz-from-data}
  &U(X,T)  &=& \bavg{u^{\epsilon}} + o(1)\\
  &\epsilon \tilde{\bar u} &=& \overline{u^{\epsilon}}- \bavg{u^{\epsilon}} + o(\epsilon)\\
  &\epsilon u' &=&  u^{\epsilon} - \overline{u^{\epsilon}} + o (\epsilon).
\end{alignat}
Therefore, the P, TH, and S equations above must be recombined into versions
involving only these quantities. 

% TODO add discussion of the derivative calculation.
% Note that the terms in the TH calculation balance because
% spatvg(tavg(W)) = O(eps) and U_T = O(eps) = U_X

\subsection{Spline-based averaging}
\label{sec:splines}

The simplest approach to approximating the multi-scale averaging operators
$\tavg{\cdot}$ and $\bar{\cdot}$ is to truncate at some fixed spatial scale.
For example, the planetary-scale spatial average could be defined by the simple
moving average
\[\bar{f}(x) = \frac{1}{L}\int_{x- L/2}^{x +L/2}
    f dx.\] 
However, this operation is not practically useful for computing the average of
quadratic quantities such as the flux terms in (\ref{eq:fluxes-immd}) because it
is not idempotent.
This operation can be made into a projection be defining dividing the domain
into disjoint intervals $I_j = [(j-1) L , j L)$, and then defining the averaging
operation as 
\[\bar{f}(x) = \sum_j \chi_{I_j}(x) \bar{f}_j,\quad \bar{f}_j=\frac{1}{L} \int_{I_j} f(x) dx,\]
where $\chi_A: \mathbb{R} \rightarrow \{0,1\}$ is the characteristic function of the set $A$.
This approach is indeed a projection, but representing $\bar{f}$ as a piecewise
constant function can introduce large errors at the cell boundaries as can be
seen in Figure \ref{fig:coarse-avg}. Moreover, these errors appear to be biased. 
\begin{figure}
  \centering
  % this figure originall comes from journal/2017-02-16
  \includegraphics{oldfigs/coarse-avg-bad}
  \caption{\label{fig:coarse-avg}. This figure plots $u'-\bar{u}$ where
    $\bar{u}$ is the simple piecewise constant coarse-grained averaging
    operator. The error is larger at the boundaries  than the centers of the subdomains.}
\end{figure}

\begin{figure}
  \centering
  % this figure originally comes from journal/2017-02-16
  \includegraphics[width=4in]{oldfigs/SAM Walker Cell and Homogeneous SST_17_1}
  \caption{Cardinal B-splines of order 3.}
  \label{fig:card-b-splines}
\end{figure}

The failure of piecewise constant averaging operators suggests that we should
seek a higher degree of smoothness in our ``averaged'' field.
A natural generalization of piecewise constant functions are piecewise smooth polynomial splines.
The so-called B-splines provide a convenient basis for the splines, and the
piecewise constant functions are actually B-splines of order 0.
I have plotted the cardinal cubic B-splines in Figure \ref{fig:card-b-splines}
to demonstrate the degree of smoothness I am talking about.
Like the piecewise constant averaging, spline based smoothing is a least-squares regression operator, so it is a projection operator. Let $A_h$ be the matrix of periodic B-splines defined on knots seperated by a distance of $h$. Then, the operator which projects data onto the subspace spanned by the columns of $A_h$ is given by 
$$ S_h = A_h (A_h^T A_h)^{-1} A_h^T.$$

Given this expression, it is trivial to show that $S_h^2 = S_h$. This property is *not* satisfied for many other smoothing operations like the Gaussian filter I was using in the previous report. Other bases, such as the Fourier basis, satisfy this property, but the cubic spline basis is much more robust because the meso-scale spatial structure is non-stationary.

Finally, if there are a set of operators $S_{h_1}$, $S_{h_2}$, where $h_1 > h_2 > \ldots > h_n$ are different smoothing bandwidths. The data $x$ can be decomposed into separate scales in the following manner inductive manner

$x_n = S_{h_n} r_n, \quad r_n = r_{n-1} - x_n.$
Where $r_n$ is the residual after n steps, and $r_0 = x$ is the original data. I show below that the Perseval's relationship holds for this multiscale decomposition. Therefore, it will be possible to use this decomposition to assess quadratic eddy quantities in a parsimonuous fashion. The main downside of this approach is that desired scales need to be supplied externally, and are not calculated automatically from the data.

% \begin{figure}
%   \centering
%   % \includegraphics[]{ms-decomp-u1.pdf}

%   \inputpgf{../../results/2017-02-28}{ms-decomp-u1.pgf}
%   \caption{Multiscale decomposition of the velocity field at $z=\SI{1000}{\m}$ using
%     B-splines. With the exception of some large amplitude synoptic waves in the
%     $u'$ field, the asymptotic scaling of these terms is consistent with
%     \eqref{eq:ansatz}. The synoptic time fluctuation of the planetary scale flow
%     shows an intriguing propagating structure.}
%   \label{fig:multi-decomp-u}
% \end{figure}


\subsection{EOF-based averaging}
\label{sec:eof}

As will be seen below, spline based averaging is not effective at separating the
planetary and synoptic scales in the Walker cell simulations. Because the walker
cell simulation is not shift-invariant. The exact size of the meso-scales
depends on the location. This spatial non-stationarity is difficult to capture
with regularly spaced B-spline functions, so a more adapative technique is
needed. Empirical orthogonal functions (EOFs) are a particularly convenient set
of basis functions to use, and were used by \autocite{slawinska_multiscale_2013}
in their multi-scale analysis of a similar simulation.

This approach is the following. Let a given cell-centered field (e.g. $u(x,z,t)$) be
represented in a data matrix $X$ with dimensions $n_x \cdot n_z \times n_t$.
And let $W$ be a diagonal matrix which weights the data in $X$ by mass. The
weight $W$ is given the difference of $p_0$ of a given grid cell. This weight is
then used to define the mass weighted inner product given by
\[ (u,v)_W = \sum_i W_i u_i v_i.\]

A singular value decomposition (SVD) of $X$ with respect to this inner product  can be
carried out by performing the SVD of $\sqrt{W}X$, which gives
\[\sqrt{W}X = \tilde{U} \tilde{S}\tilde{V}^T. \]
Then, the EOFs are given by the columns of $U=W^{-\frac{1}{2}} \tilde{U}$, which
are orthonormal with respect to $(\cdot, \cdot)_W$.
The first few EOFs are a natural basis for the planetary scale flow of
our data, and can be used as a data-aware projection basis much as the B-spline
basis dicussed above.
The first four EOFs for the velocity, buoyancy, and heating source fields are
shown in Figure \ref{fig:EOFs}.

\begin{sidewaysfigure}
  \centering
  \subfloat[$u$] {\includegraphics[width=.40\textwidth]{eof-U.pdf}}
  \subfloat[$B$] {\includegraphics[width=.40\textwidth]{eof-B.pdf}}\\
  \subfloat[$w$] {\includegraphics[width=.40\textwidth]{eof-W.pdf}}
  \subfloat[$H$] {\includegraphics[width=.40\textwidth]{eof-H.pdf}}
  \caption{First four EOFs for the four relevant kinematic fields.}
  \label{fig:EOFs}
\end{sidewaysfigure}

By projecting the data $X$ onto a small number of these EOFs, we obtain a smooth
estimate of the data . Therefore, the EOF-based planetary averaged is defined
by
\begin{equation}
  \label{eq:eof-avg}
  \bar{X} =  U_{\ell} U_{\ell}^T W X,
\end{equation}
where $U_{\ell}$  consists of the first $\ell$ columns (EOFs) of $U$.

The performance of EOF-based averaging is shown below in Section \ref{sec:eof-v-spline}.


\section{Results}
\label{sec:results}


\subsection{Comparison of Splines  and EOF averaging}
\label{sec:eof-v-spline}

The spline based averaging approach is tested on the non-rotating Walker cell
simulation. It is very sensitive to the location and spacing of the splines
knots (discontinuities). If a synoptic length-scale $L=1$ and planetary time
scale $T=10$ are used, then the scale-separation is good. However, using a
planetary length-scale $L=10$ results in a poor decomposition. 

Unfortunately, the temporal direction is much better sampled than the spatial
direction, so we have to use two different truncation levels. This seems to
indicate a fundamental problem with either the spline based decomposition
method, or with the basic scale separation ansatz used by the IMMD.

The spline based approach could be failing because the spatial scale is smaller
in the center of the domain that it is in the exterior. This approach could be
using a more adaptive scale seperation technique, such as EOF analysis, or
manually placing the knot points.

This indicates, that the spline approach is suitable for the meso-scale
averaging (MESD), but should not be used for planetary-scale decompositions.
This failure probably happens because of the spatial non-stationarity of the
Walker cell simulations. The spline approach is more likely to succeed on the
homegeneous SST simulations because of the shift-invariance.


\begin{figure}[ht]
  \centering
  \includegraphics[]{fig-ms-1-10.pdf}
  \caption{\label{fig:ms-1-10} Multiscale decomposition of $u$ using B-splines with a
    knots spaced every $L=1$ and $T=10$ non-dimensional units. This is a
    synoptic scale spatial average and planetary scale temporal average. The
    scale separation is good}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics[]{fig-ms-5-5.pdf}
  \caption{\label{fig:ms-5-5} Multiscale decomposition of $u$ using B-splines with a
    knots spaced every $L=5$ and $T=5$ non-dimensional units. This is a
    synoptic scale spacial and temporal average. The scale separation is poor.}
\end{figure}


\begin{figure}[ht]
  \newcommand{\myrowlab}[1]{\raisebox{.80in}{\rotatebox[origin=c]{90}{#1}\quad}}
  \centering
  \myrowlab{$u$ at $z=\pi/5$}
  \includegraphics[width=.8\textwidth]{ms-eof-u}\\
  \myrowlab{$B$ at $z=\pi/2$}
  \includegraphics[width=.8\textwidth]{ms-eof-B}\\
  \myrowlab{$w$ at $z=\pi/2$}
  \includegraphics[width=.8\textwidth]{ms-eof-w}\\
  \myrowlab{$H$ at $z=\pi/2$}
  \includegraphics[width=.8\textwidth]{ms-eof-h}
  \caption{\label{fig:ms-eof} EOF based multiscale decomposition of using 4 EOFs
    for spatial averaging and B-splines with knots every $T=10$ nondimensional
    time units for temporal averaging. This form of spatial averaging
    outperforms the B-spline approach for $u$ and $B$ (c.f. Figure
    \ref{fig:eof-ms}).}
\end{figure}

\subsection{Validating the IMMD Ansatz}
\label{sec:ansatz-validation}

\begin{figure}[ht]
  \centering
  \subfloat[$\Delta x = 0.04=\SI{64}{\km}$]{\includegraphics{w-meso.pdf}}
  \subfloat[$\Delta x = .03=\SI{500}{\km}$]{\includegraphics{w-syn.pdf}}
  \caption{\label{fig:coarse-w}Vertical velocity at $z=\pi/10$ coarse-grained over subdomains
    of different sizes.
    As can be seen from the colorbar, there are extremely large amplitude mesoscale structures in the $w$ field which violate the basic ansatz of IMMD.
    The same is true of the apparent heating field $H$.
    This can be ameliorated by coarse graining the data (right).}
\end{figure}


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
