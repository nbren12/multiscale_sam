
% Default to the notebook output style

    


% Inherit from the specified cell style.




    
\documentclass[11pt]{article}

    
    
    \usepackage[T1]{fontenc}
    % Nicer default font (+ math font) than Computer Modern for most use cases
    \usepackage{mathpazo}

    % Basic figure setup, for now with no caption control since it's done
    % automatically by Pandoc (which extracts ![](path) syntax from Markdown).
    \usepackage{graphicx}
    % We will generate all images so they have a width \maxwidth. This means
    % that they will get their normal width if they fit onto the page, but
    % are scaled down if they would overflow the margins.
    \makeatletter
    \def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth
    \else\Gin@nat@width\fi}
    \makeatother
    \let\Oldincludegraphics\includegraphics
    % Set max figure width to be 80% of text width, for now hardcoded.
    \renewcommand{\includegraphics}[1]{\Oldincludegraphics[width=.8\maxwidth]{#1}}
    % Ensure that by default, figures have no caption (until we provide a
    % proper Figure object with a Caption API and a way to capture that
    % in the conversion process - todo).
    \usepackage{caption}
    \DeclareCaptionLabelFormat{nolabel}{}
    \captionsetup{labelformat=nolabel}

    \usepackage{adjustbox} % Used to constrain images to a maximum size 
    \usepackage{xcolor} % Allow colors to be defined
    \usepackage{enumerate} % Needed for markdown enumerations to work
    \usepackage{geometry} % Used to adjust the document margins
    \usepackage{amsmath} % Equations
    \usepackage{amssymb} % Equations
    \usepackage{textcomp} % defines textquotesingle
    % Hack from http://tex.stackexchange.com/a/47451/13684:
    \AtBeginDocument{%
        \def\PYZsq{\textquotesingle}% Upright quotes in Pygmentized code
    }
    \usepackage{upquote} % Upright quotes for verbatim code
    \usepackage{eurosym} % defines \euro
    \usepackage[mathletters]{ucs} % Extended unicode (utf-8) support
    \usepackage[utf8x]{inputenc} % Allow utf-8 characters in the tex document
    \usepackage{fancyvrb} % verbatim replacement that allows latex
    \usepackage{grffile} % extends the file name processing of package graphics 
                         % to support a larger range 
    % The hyperref package gives us a pdf with properly built
    % internal navigation ('pdf bookmarks' for the table of contents,
    % internal cross-reference links, web links for URLs, etc.)
    \usepackage{hyperref}
    \usepackage{longtable} % longtable support required by pandoc >1.10
    \usepackage{booktabs}  % table support for pandoc > 1.12.2
    \usepackage[inline]{enumitem} % IRkernel/repr support (it uses the enumerate* environment)
    \usepackage[normalem]{ulem} % ulem is needed to support strikethroughs (\sout)
                                % normalem makes italics be italics, not underlines
    
    % commands and environments needed by pandoc snippets
    % extracted from the output of `pandoc -s`
    \providecommand{\tightlist}{%
      \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
    \DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
    % Add ',fontsize=\small' for more characters per line

    % Define a nice break command that doesn't care if a line doesn't already
    % exist.
    \def\br{\hspace*{\fill} \\* }
    % Math Jax compatability definitions
    \def\gt{>}
    \def\lt{<}
    % Document parameters
    \title{Preliminary Report on Homogeneous SST simulations}
    \date{Thursday, May 4, 2017}
    \author{Noah D. Brenowitz}
    
    
    

    % Pygments definitions
    
\makeatletter
\makeatother


    % Exact colors from NB
    \definecolor{incolor}{rgb}{0.0, 0.0, 0.5}
    \definecolor{outcolor}{rgb}{0.545, 0.0, 0.0}



    
    % Prevent overflowing lines due to hard-to-break entities
    \sloppy 
    % Setup hyperref package
    \hypersetup{
      breaklinks=true,  % so long urls are correctly broken across lines
      colorlinks=true,
      urlcolor=urlcolor,
      linkcolor=linkcolor,
      citecolor=citecolor,
      }
    % Slightly bigger margins than the latex defaults
    
    \geometry{verbose,tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in}
    
    

    \begin{document}
    
    
    \maketitle
    
    
\section{Wave-frame averages}
\input{wave+frame+averages.tex}
    
\section{Performance of Different Averaging Procedures}

This section of the report uses non-dimensional units.

I compare four different multi-scale decomposition methods on the velocity and heating data.
Performance of penalized fourier method \(N=(1+\alpha |k|^4)^{-1}\) (A),
tanh weighted filtering (B), bare Fourier truncation (C), and splines
(D) for different effective degrees of freedom (dof).  The effective degrees of freedom is defined as $tr(S)$ where $S$ is the linear smoothing operator which implements the low-pass filters mentioned in the previous sentences.


These methods all
can capture the synoptic scale (dof=20) fluctations well, but the
planetary scale pattern obtained is substantially different between the
methods. The spline and Fourier penalization methods allow for an
attractive adaptivity in the inferred planetary scale structure, but the
benefit is not large.

    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.4\paperheight}}{performance+of+gaussian+averaging_files/performance+of+gaussian+averaging_19_0.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    How do these same methods perform on the much noiser \(H\) variable? We
are forced to use a logarithmic axis for the these plots.


    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{performance+of+gaussian+averaging_files/performance+of+gaussian+averaging_21_0.png}
    \end{center}
    
    The performance is similar for low dof, but the Fourier penalization
approach (A) yields substantially less noisy results for dof=21. For
instance, look at the large oscillations in (B) and (C) for
\(300 < x<400\), where there is subtantially lower variance in \(H\). In
this region, the estimated curves for (B) and (C) are very sensitive to
the number of dof, while the penalization method's estimates vary
continuously with dof.

For these reasons, the penalized method seems to perform the best on
both the horizontal velocity, which varies smoothly, and the noisy \(w\)
and \(B\) fields. A similar penalization approach could be used with the
spline basis, but the splines do not perform any better than the Fourier
basis, and are more unfamiliar to an AOS audience.

Here is how the multiscale decompositions performs on the heating field:
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{performance+of+gaussian+averaging_files/performance+of+gaussian+averaging_26_0.png}
    \end{center}

and the zonal velocity:

    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{performance+of+gaussian+averaging_files/performance+of+gaussian+averaging_27_0.png}
    \end{center}
 
The intense synoptic scale wind convergence around $x=10$ coincides with a peak in the synoptic-scale heating patterns. 
    
Here is a hovmoller diagram of the deviation from the planetary scale pattern obtained using the penalized Fourier approach.
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{performance+of+gaussian+averaging_files/performance+of+gaussian+averaging_30_1.png}
    \end{center}
        
    \end{document}
