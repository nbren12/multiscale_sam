% Created 2017-02-27 Mon 11:04
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{subfig}
\graphicspath{{figs/}}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
% \usepackage{palatino}
 \usepackage{amsmath}
\usepackage{kpfonts}
%\usepackage{textcomp}
\usepackage{todonotes}
\usepackage{amssymb}
%\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{siunitx}
 \usepackage[margin=1in]{geometry}
\usepackage[backend=biber,
style=ieee,
isbn=false,
url=false
]{biblatex}
\addbibresource{zotero.bib}

\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepgfplotslibrary{groupplots}
\author{Noah D. Brenowitz and Andrew J. Majda}
\date{\today}
\title{Multiscale Interactions in Two-dimensional Cloud-resolving Models}
\hypersetup{
 pdfauthor={Noah D. Brenowitz},
 pdftitle={Multiscale Interactions in Two-dimensional Cloud-resolving Models},
 pdfkeywords={},
 pdfsubject={},
 pdflang={English}}

\newcommand{\totd}[2]{\frac{D #1}{D #2}}
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\bu}{\mathbf{u}}
\newcommand{\ub}{\bar{u}}
\newcommand{\tavg}[1]{\langle #1 \rangle}
\newcommand{\bavg}[1]{\langle \overline{#1} \rangle}
\newcommand{\brunt}{Brunt-V\"ais\"al\"a\ }

\newcommand\inputpgf[2]{{
\let\pgfimageWithoutPath\pgfimage
\renewcommand{\pgfimage}[2][]{\pgfimageWithoutPath[##1]{#1/##2}}
\input{#1/#2}
}}


     \usepackage{layouts}
\begin{document}
\maketitle

\section*{Summary}

\begin{itemize}
  \item Formulated a practical methodology using B-splines to perform a multiscale decomposition of the input data.
  \item Derived a buoyancy equation which is analogous to the potential temperature equation of the IMMD models.
  \item Reran the SAM runs with fixed radiative forcing of \SI{-1.5}{\kelvin\per\day} in the troposphere. This test case shows that the multi-scale interactions and emergence of a planetary scale flow  in the homogeneous SST case are not a cloud-radiative effect like the case of three-dimensional ``self-aggregation'' at RCE.
  \item Ran the SAM model for homogeneous and warm-pool SST patterns for various zero and non-zero latitudes. The results with coriolis force are similar to results using the multicloud model.
\end{itemize}

\clearpage
\tableofcontents
\clearpage


\section{Introduction}
\label{sec:intro}

\subsection{Equations and numerical methods for the tropical atmosphere}
\label{sec:intro-eq}

The basic equations which govern the flow of the tropical atmosphere on scales
larger than 1km are the anelastic equations are the anelastic equations
equations with bulk cloud microphysics. In dimensional form, these are given by

\begin{align}
  \label{eq:euler}
  &\totd{\bu}{t} + ( f  + \beta y) \mathbf{k}\times \bu = - \nabla \phi \\
  &\totd{w}{t} = -\nabla \phi + g B \label{eq:w-full}\\
  &\totd{\theta}{t}  = Q_{rad}  + \frac{L \theta}{T c_p} \left( C_d - E_r\right)\\
  &\totd{q_v}{t} = E_r - C_d\\
  &\totd{q_r}{t}  -  \alpha \pd{\rho v_T q_r}{z} = C_r + A_r - E_r\\
  &\totd{q_c}{t} = C_d - A_r - C_r\\
  &\nabla \cdot \bu + \frac{1}{\rho_0} \pd{\rho_0 w}{z}  = 0.
\end{align}

In the above equations, $\bu = (u,v)$, $\nabla = (\partial_x, \partial_y)$, and
$\totd{}{t}= \pd{}{t} + \bu \cdot \nabla + \pd{}{z}$. The specific humidities
for the three water species are water vapor $q_v$, rain $q_r$, and cloud water
$q_c$. The bouyancy is given by $B = g (\alpha - \alpha_0)\alpha_0^{-1}$ and the
dynamic pressure by $\phi = \alpha_0(p - p_0)$. See the paper by
\textcite{pauluis2008} for more details on these equations of motion.

For mesoscales and larger, hydrostatic balance is approximately true, so
\eqref{eq:w-full} can be replaced by
\begin{equation}
  \label{eq:hsb}
  \phi_z = B.
\end{equation}

\subsection{Equations used by SAM}
\label{sec:sam}

The System for Atmospheric Modeling (SAM) uses a different set of prognostic
equations \autocite{khairoutdinov_cloud_2003}. These are given by

\begin{subequations}
    \label{eq:sam}
  \begin{align}
    &\totd{\bu}{t} + ( f  + \beta y) \mathbf{k}\times \bu = - \nabla \phi \\
    &\totd{w}{t} = -\nabla \phi + g B \label{eq:w-full}\\
    &\totd{h_L}{t}  = Q_{rad}  - \frac{1}{\rho_0} \pd{}{z} \left( L_c P_r + L_s P_s + L_s P_g \right)\\
    &\totd{q_T}{t} = -  \dot{q}_p^{\text{micro}} \label{eq:sam-qT}\\
    &\totd{q_p}{t} = \frac{1}{\rho_0} \pd{}{z} \left(  P_r +  P_s + P_g \right) + \dot{q}_p^{\text{micro}}\\
    &\nabla \cdot \bu + \frac{1}{\rho_0} \pd{\rho_0 w}{z}  = 0.
  \end{align}
\end{subequations}
I have neglected the sub-grid-scale flux terms in the formulation above since
they are not relevant to the problem considered in this report. The temperature
and humidity variables introduced in the previous section have been replaced
here by
\begin{description}
\item[Liquid/ice water static energy] $h_L = c_p T +gz - L_c (q_c + q_r) - L_s
  (q_i + q_s + q_g)$,
\item[Total nonprecipitating water] $q_T = q_v + q_c + q_i$, and
\item[Total precipitating water] $q_p = q_r + q_s + q_g$.
\end{description}
Note that $h_L$ is only an appropriate variable when hydrostatic balance or
anelastic equations are satisfied. Otherwise, potential temperature should be
used. There are a number of diagnostic equations which can be solved to
determine the temperature, and all the mixing ratios for the different species
of precipitating and non-precipitating water. Perhaps most importantly, the
water vapor is assumed to be at or below an approximate saturation value so that
\[q_v = \min(q_T, q_{sat}(p_0, T)),\]
where
\[ q_{sat} = q_{sw} (1-\omega_i) + \omega_i q_{si}\]
is a convex combination of the saturation mixing ratios of vapor with respect to
ice and liquid water. 

A consequence of this diagnostic relation is that the latent
heating term due to condensation is not explicitly calculated. Instead, SAM
models the microphysical processes which govern the conversion from condensate
($\text{cloud water} + \text{cloud ice}$) to precipitation ($\text{rain} +
\text{snow} + \text{grauppel}$). These processes are given by
\[
\dot{q}_p^{\text{micro}} = \text{accretion} - \text{evaporation} + \text{autoconversion}.
\]
A nice paper by \textcite{hernandez-duenas_minimal_2013} contains a more thorough
discussion of a similar set of microphysical equations, which can be helpful for
gaining intuition. Additional details about the diagnostic relations linking the
water species and the prognostic variables $q_T$, $h_L$, and $q_p$ are available
in Appendix \ref{sec:diag-q} and in the original SAM paper by \textcite{khairoutdinov_cloud_2003}.


This formulation is convenient for numerical modeling purposes, but
moist-convective processes are often  understood as a coupling
between dry dynamics and moist variables because the bouyancy is related
primarily to temperature, rather than moist static energy.
For instance, the circulation due to the monsoons and MJO can  be understood roughly as the response to a imposed heating pattern \autocite{gill_simple_1980}, and the multiscale theories of Biello and Majda rely upon asympotic analysis of the latent heating.


\subsection{Overview of multiscale theories for the atmosphere}
\label{sec:org253f7be}

\begin{figure}
  \centering
  \begin{tikzpicture}[ ylab/.style={xshift=-1em, yshift=1cm, rotate=90},
    xlab/.style={yshift=-1em, xshift=1cm}, ]

    \draw[step=2cm,gray,thin] (0,0) grid (8,8);

    \node[yshift=-1em] at (8.5,0) {$x$}; \node[xshift=-1em] at (0,8.5) {$t$};

    \draw[->,thick] (0,0) -- (9,0); \draw[->,thick] (0,0) -- (0,9);

    \node[xlab] at (0,0) {micro}; \node[xlab] at (2,0) {meso}; \node[xlab] at
    (4,0) {synoptic}; \node[xlab] at (6,0) {planetary};


    \node[ylab] at (0,0) {micro}; \node[ylab] at (0,2) {meso}; \node[ylab] at
    (0,4) {synoptic}; \node[ylab] at (0,6) {planetary};

    \node[draw,ellipse,fill=white, minimum height=2cm] at (6,6) {IPESD/IMMD};
    \node[draw,ellipse,fill=white, minimum height=2cm] at (4,4) {MESD};

  \end{tikzpicture}

  \caption{\label{fig:scales}Stommel diagram of scales in the tropical
    atmosphere and associated multiscale models. Adapated from
    \textcite{majda_new_2007}. MESD is ``Mesocale-equatorial Synoptic-scale
    dynamics'' and IPESD is ``Intra-seasonal planetary equatorial synoptic
    dynamics''.}
\end{figure}

See Figure \ref{fig:scales} for an overview of the typical scales of motion of
the tropical atmosphere. Over the past decade Majda and coathors have developed
many models for describing the interactions between the various scales depicted
in this figure.

\section{SAM Simulations}
\label{sec:sam-sims}


\begin{figure}
  \centering
  \includegraphics{2017-03-07/profs}
  \caption{Thermodynamic profiles for SAM runs. The runs are forced with a
    uniform cooling (left). The middle panel shows the initial potential
    temperature profile, and the right panel shows the water vapor mixing ratio
    profile.}
  \label{fig:profs}
\end{figure}

\begin{figure}
  \centering
   \includegraphics{2017-03-07/tb-homo-xt}
  \caption{Homogeneous SST  TB Hovmoller diagrams for
    \SIlist{0;6}{\degree N}.}
  \label{fig:tb-xt-homo}
\end{figure}

\begin{figure}
  \centering
  \includegraphics{oldfigs/homo-tb}
  \caption{Hovmoller diagram of TB for simulation with active long-wave
    radiation. Compare to \ref{fig:tb-xt-homo}}
  \label{fig:old-homo-tb}
\end{figure}


\begin{figure}
  \centering
  \includegraphics{2017-03-07/u1-homo-xt}
  \caption{Homogeneous SST  U at \SI{4000}{\meter} Hovmoller diagrams for
    \SIlist{0;6}{\degree N}.}
  \label{fig:u-xt-homo}
\end{figure}

\begin{figure}
  \centering
   \includegraphics{2017-03-07/tb-walker-xt}
  \caption{Warm-pool SST  TB Hovmoller diagrams for
    \SIlist{0;6;10}{\degree N}.}
  \label{fig:tb-xt-warm}
\end{figure}

\begin{figure}
  \centering
  \includegraphics{2017-03-07/u4000m-walker-xt}
  \caption{Warm-pool SST  U at \SI{4000}{\meter} Hovmoller diagrams for
    \SIlist{0;6;10}{\degree N}.}
  \label{fig:u4000-xt-warm}
\end{figure}

\begin{figure}
  \centering
  \includegraphics{2017-03-07/u12500m-walker-xt}
  \caption{Warm-pool SST  U at \SI{12500}{\meter} Hovmoller diagrams for
    \SIlist{0;6;10}{\degree N}.}
  \label{fig:u12.5-xt-homo}
\end{figure}

\begin{figure}
  \subfloat[Zonal winds]{\includegraphics{2017-03-07/clim-u.pdf}}
  \subfloat[Meridional winds]{\includegraphics{2017-03-07/clim-v.pdf}}
  \caption{Wind x-z climatologies for varying latitudes. The data are averaged
    over the final 80 days of each simulation. From top to bottom, each row
    plots data for latitudes \SIlist{0;6;10}{\degree N}, respectively. The
    meridional component of the winds becomes increasingly strong with non zero
    coriolis forces.}
  \label{fig:walker-clims}
\end{figure}

This report presents a series of new simulations using the SAM model.
All of the new simulations have large-scale radiative forcing of
\SI{-1.5}{\kelvin\per\day} in the troposphere.
The simulations have  resolution of \SI{2}{km}.
The homogeneous SST (warm-pool) experiments are nudged towards a barotropic zonal wind of
\SI{-10}{\meter\per\second} (\SI{0}{m/s}) with a time-scale of \SI{1}{\day}.

\section{Intraseasonal multi-scale moist dynamics (IMMD)}
\label{sec:orgd8d1518}


\subsection{Parent Model}

\textcite{biello_intraseasonal_2010} presents an asymptotic theory for
multi-scale interactions between the planetary and synoptic scale flows in the
tropical atmosphere. This theory is valid in the presence of moisture and a
planetary scale circulation with a magnitude of around
\SI{50}{\meter\per\second}. The basic non-dimensional equations used as a starting
point for the theory are given by
\begin{subequations}
    \label{eq:immd-nondim}
  \begin{align}
    \totd{u}{t}  -yv =  - p_x - \epsilon d u \\
    \totd{v}{t}  +yu =  - p_y - \epsilon d v \\
    \totd{\theta}{t}  +w =  \epsilon H - \epsilon d_{\theta} \theta \\
    \totd{q}{t}  -Q_0 w =  - \epsilon H\\
    p_z = \theta\\
    u_x+v_y + w_z = 0.
  \end{align}
\end{subequations}
The Newtonian relaxation terms in the velocity and temperature equations are
simply meant to account for sub-grid-scale drag and radiative cooling. Their
precise mathematical form is not important, and they can be seen as a
placeholder for more complex radiative transfer and cumulus drag dynamics.

In two dimensions without rotation, it is not clear what the scales are that
should be used for non-dimensionalization. In
\textcite{biello_intraseasonal_2010}, the authors use the equatorial deformation
scale scale $L_d= \sqrt{NH/\beta}$ to nondimensionalize the equations, so I
guess I will use the same scales, just to check that the asympotics work. The
non-dimensional equations are obtained assuming that $L\sim L_d=\SI{1500}{\km}$,
$U\sim NH_T=\SI{50}{\m\per\s}$, and $T=L_d/c=\SI{8.3}{\hour}$.

Even though there is no coriolis force in some of the 2D simulations, the
asymptotics should still be valid if these scales are present in the 2D
simulations. Thus, the most important question is: \emph{are these scales
consistent with those seen in the two dimensional walker circulation and
homogeneous SST simulations?}


\subsection{Multi-scale decomposition}
\citetitle{biello_intraseasonal_2010} is the foundational paper for this section
of the report. Of key relevance is the basic asympotic assumptions they make
about the velocity, temperature, and humidity fields. They begin by introducing
the ratio of synoptic scale latent heating to the non-dimensional unit for
latent heating as a crucial small parameter. This quantity can be more
conveniently expressed in humidity units the output variables of a CRM by
\begin{equation}
  \label{eq:eps}
  \epsilon = \frac{\text{Measured $C_d - E_r$  on synoptic scales}}{\SI{40}{\g\per\kg\per\day}}.
\end{equation}

\begin{description}
\item[Assumption 1] $\epsilon \ll 1$
\item[Assumption 2] The following asymptotic expansion is assumed for the
  various model variables:
  \begin{subequations}
      \label{eq:ansatz}
    \begin{alignat}{3}
      u^{\epsilon} &= U(X,T) +& \epsilon (u'(x,X,t,T) + \ub(X,t,T)) + \epsilon^2 u_2\\
      v^{\epsilon} &= & \epsilon (v'(x,X,t,T) + \bar{v}(X,t,T)) + \epsilon^2 v_2\\
      w^{\epsilon} &= & \epsilon (w'(x,X,t,T) + \bar{w}(X,t,T)) + \epsilon^2 w_2\\
      \theta^{\epsilon} &= \Theta(X,T) +& \epsilon (\theta'(x,X,t,T) + \bar{\theta}(X,t,T)) + \epsilon^2 \theta_2\\
      q^{\epsilon} &= Q(X,T) +& \epsilon (q'(x,X,t,T) + \bar{q}(X,t,T)) +
      \epsilon^2 q_2
    \end{alignat}
  \end{subequations}
\end{description}

Using these assumptions, and a simplified non-dimensional form of the equations
of motion, the authors separate the dynamics into the first order Trade Winds
and Hadley Circulation (TH) component, a synoptic (S) component, and a planetary
fast-time component (P).

\paragraph{Trade-wind Hadley-cell (TH)}

\begin{subequations}
    \label{eq:th}
  \begin{align}
    \totd{U}{T} - y V + P_X = -dU\\
    y U + P_y = 0\\
    P_z = \Theta \\
    \totd{\Theta}{T} + W = \tavg{\bar{H}} - d_{\theta} \Theta\\
    \totd{Q}{T} - Q_0 W = -\tavg{\bar{H}}\\
    U_X + V_y + W_z = 0
  \end{align}
\end{subequations}
where $V = \tavg{\bar{v}}$ and $W = \tavg{\bar{w}}$.

\paragraph{Synoptic Fluctuation (S)}

\newcommand{\lhssyn}[2]{%
  #1'_t + U #1'_x +(u'#2)_x+ (v'#2)_x + (w'#2)_z}

\begin{subequations}
    \label{eq:S}
  \begin{alignat}{3}
    \lhssyn{u}{U} - y v' + p'_x &= 0\\
    v'_t + U v'_x +yu' + p'_y &= 0\\
    \lhssyn{\theta}{\Theta}  &= H' \\
    \lhssyn{Q}{\Theta}  &= Q_0 w' - H' \\
    p'_z &= \theta'\\
    u'_x  +v'_y + w'_z &= 0\\
  \end{alignat}
\end{subequations}
\paragraph{Planetary scale anomalies from TH (P)}

Equation (3.18) gives the fast time scale modulation of the planetary anomalies.
These equations are reproduced here:
\newcommand{\tb}[1]{\tilde{\bar{#1}}}
\renewcommand{\lhssyn}[2]{%
  #1_t + (\tilde{\bar{v}}#2)_y+ (\tilde{\bar w} #2)_z }
\begin{subequations}
    \label{eq:immd-P}
  \begin{alignat}{3}
    \lhssyn{\tb u}{U} - y \tb v  &= 0\\
    \tb v _t + y\tb u + \tb{p}_y &= 0\\
    \lhssyn{\tb\theta}{\Theta}  + \tb{w} &= \tb{H} \\
    \lhssyn{\tb q}{Q}  -Q_0 \tb w &=- \tb H' \\
    \tb p_z &= \tb \theta\\
    \tb v_y + \tb w_z &= 0
  \end{alignat}
\end{subequations}
These equations notably, do not describe zonally propagating disturbance, but
they do allow for a response to a zonally propagating heat source.

The final important set of equations describe the intraseasonal dynamics of the
planetary scale $O(\epsilon)$ contributions. Introducing a slight notational
simplification, we let $u = \tavg{\bar u}$, and similarly for $p$, $q$, and
$\theta$.
The next order contributions are used to define $v= \tavg{\bar v_2}$ and $w =
\tavg{\bar w_2}$.
Then, the solvability condition for the $O(\epsilon^2)$ dynamics requires that
\renewcommand{\lhssyn}[2]{%
  \frac{D #1}{D T} + (u #2)_X + (v #2)_y+ (w #2)_z}
\begin{subequations}
  \begin{align}
    &\lhssyn{u}{U} - y v + p_X = F^u - du\\
    &y u + p_y = -d V\\
    &\lhssyn{\theta}{\Theta} + w = F^{\theta}  +  \tavg{\bar{H}_2}- d_\theta \theta\\
    &\lhssyn{q}{Q} - Q_0 w = F^{q} - \tavg{\bar{H}_2}\\
    &p_z  = \theta\\
    &u_X + v_y + w_z = 0,
  \end{align}
\end{subequations}
where the forcing terms on the right hand side are given by
\newcommand{\bbavg}[1]{\tavg{\overline{#1}}}
\newcommand{\fluximmd}[1]{ -\bbavg{\tilde v \tilde{#1} + v' #1 '}_y  -
  \bbavg{\tilde w \tilde{#1} + w' #1 '}_z}
\begin{subequations}
  \label{eq:fluxes-immd}
  \begin{align}
    F^u        &=\fluximmd{u}\\
    F^{\theta} &=\fluximmd{\theta}\\
    F^{q}      &=\fluximmd{q}.
  \end{align}
\end{subequations}

\subsection{Specialization to 2D}
\label{sec:org0a5c317}

Write the important equations in vorticity stream form.

\section{Connnecting IMMD and SAM}
\label{sec:sam-v-immd}

The objective of this section is to use the TH, S, and P dynamics derived in 
\textcite{biello_intraseasonal_2010} and described briefly above to elucidate
the nature of multiscale interactions in the SAM simulations described in
Section \ref{sec:sam-sims}.
Another goal will be to diagnose the individual terms in the IMMD equations
using the SAM model.

The most important task is to connect the IMMD parent model given by \eqref{eq:immd-nondim} to the dynamical equations of SAM, which are given by \eqref{eq:sam}.
Some of the key differences between these two sets of equations have been
highlighted in Table \ref{tab:immd-v-sam}.
While IMMD and SAM have similar momentum equations, the IMMD equations have
substantially simpler thermodynamics than SAM does.
The objective of the following section is to connect the thermodynamics of these
two sets of equations.

\subsection{Thermodynamics}

\begin{table}
  \centering
  \begin{tabular}{lcc}
    &SAM& IMMD\\
    \hline
    Incompressibility condition& Anelastic& Boussinesq\\
    Virtual temperature effect on buoyancy & yes & no \\
    Buoyancy variable & $\theta$ &   $\theta_v'/\theta_{v0}$\\
    Hydrostatic balance & no & yes  \\
    Prognostic thermodynamic variables & $h_L$, $q_T$, and $q_p$& $\theta$ and $q_v$  \\
  \end{tabular}
  \caption{Key differences between SAM \eqref{eq:sam} and the IMMD dynamics \eqref{eq:immd-nondim}.}
  \label{tab:immd-v-sam}
\end{table}

The strongest connection between the two systems of equations is that both feature a buoyancy
term in their vertical momentum or hydrostatic equations.
Therefore, to begin with we draw the analogy between $\theta$ \eqref{eq:immd-nondim} and $B$ in
\eqref{eq:sam}.
Then, we can formally calculate a prognostic equation for bouyancy
\begin{equation}
  \label{eq:buoy}
  \totd{B}{t} = \left( \totd{B}{t} \right)_{rad} + \left( \totd{B}{t} \right)_{con}.
\end{equation}

In the presence of moisture, the buoyancy is defined by
\[ B = g \frac{T_v - T_{v0}}{T_{v0}},\]
where the virtual temperature is given by $T_v = T(1 +\epsilon_v r_v - r_p - r_n)$.
The bouyancy source due to cumulus convection can then by obtained if the virtual temperature forcing is known.
However, $r_v\lesssim.04$ so that the density variations due to the virtual temperature effect are smaller than the asymptotic effects we are trying to identify.
Thus, for the asymptotic purposes and simplicity of analysis, we approximate the buoyancy by the dry formula
\begin{equation}
  \label{eq:Bapprox}
  B \approx g(T-T_0)/T_0=  g(\theta - \theta_0)/\theta_0.
\end{equation}
The potential temperature is defined  using dry thermodynamic constants by
\[\theta = T \left( p_s/p_0(z)\right)^{R_d/c_p}.\]
The forcing bouyancy forcing is then related to the first law of thermodynamics, so that
\begin{align}
  \label{eq:dbdt}
  \totd{B}{t} &= \frac{g}{\theta_0}\totd{\theta}{t}  -\frac{g\theta}{\theta_0^2} \totd{\theta_0}{t}  \nonumber \\
              &=  \frac{g}{\theta_0} \totd{\theta}{t} - \frac{g \theta}{\theta_0^2}  w \partial_z \theta_0 
\end{align}
The first term is given approximately by the first law of thermodynamics expressed in terms of potential temperature
\begin{equation}
\totd{\log \theta}{t} = \frac{1}{c_pT} \left( Q_{rad} + L_v C  \right). \label{eq:first-law-pot-temp}
\end{equation}
The first term $Q_{rad}$ is the contribution of radiation and other external heating  (in Watts) and the second term is the latent due to the net condensation of water vapor (condensation minus evaporation) $C$.
\textcite{pauluis2008} raises the important point that this dry formulation is not strictly valid in the presence of moisture, but as above we are only seeking formulas which are first order accurate.
These considerations are of course important when writing a thermodynamically consistent cloud-resolving model.

Continuing with the approximations,  we can employ the anelastic approximation to conclude that $\theta/\theta_0 \approx 1 $.
Therefore, the second term in \eqref{eq:dbdt} can be identified as $N^2 w$ where $N^2  = g \partial_z \log(\theta_0)$ is the squared (dry) \brunt frequency.
Now, we put these approximations together and see that
\begin{equation}
  \label{eq:dbdt1}
  \totd{B}{t} + N^2 w \approx \frac{g}{c_p T} \left(  Q_{rad} + L_v C \right).
\end{equation}
This equation provides a connection to the crucial non-dimensional quantity $H$ discussed in the IMMD paper.

For data analysis purposes, we will identify
\begin{equation}
  \label{eq:sam-H}
  H = \totd{B}{t} +  N^2 w - \frac{g}{c_p T} Q_{rad} \approx \frac{g L_{v}}{c_{p} T} C,
\end{equation}
as the ``apparent latent heating''. The total derivatives can be approximated from the output. This implies that estimated $H$  will contain all non-radiative heating, including sub-grid-scale fluxes of temperature. This is not a philisophical problem, because most of these sub-grid-scale effects are due to convection, and we do not make any distinction per se between Latent heating and convective temperature fluxes within clouds.

Even though the SAM model does not output latent heating directly, it can be calculated with some effort from the model output if the ``apparent latent heating'' approach proves inadequate. In particular, the net condensation is given by
\begin{equation}
C = C_d - E_r = \totd{q_v}{t} =
  \begin{cases}
    \totd{q_T}{t} & if  q_T < q_{sat}(T, p_0) \\
    \totd{q_{sat}}{t} & if  q_T \geq q_{sat}(T, p_0) 
  \end{cases}.
\end{equation}
If the air is unsaturated, then the net condensation is given by the microphysical processes specified in \eqref{eq:sam-qT}, namely the evaporation rain, snow, and graupel. For saturated parcels, the condensation is given by the total derivative of the saturation mixing ratio, which using the chain rule is equal to
\begin{equation}
  \label{eq:dqsdt}
  \totd{q_{sat}}{t} = \alpha_T \totd{T}{t} + \alpha_{p_0} \totd{p_0}{t},
\end{equation}
where $\alpha_T = \left( \pd{q_{sat}}{T} \right)_{p_0}$ and $\alpha_T = \left( \pd{q_{sat}}{p_0} \right)_{T}$. This equation can then be combined with the first law of thermodynamics \eqref{eq:first-law-pot-temp}, which specifies $\totd{T}{t}$. While the approach is thermodynamically consistent with the model, it presents a substantial overhead,  so we proceed with the ``apparent latent heating'' approach defined in \eqref{eq:sam-H}.

\begin{figure}
  \centering
  \includegraphics{2017-03-07/H-zoom-walker}
  \caption{Hovmoller diagram of apparent latent heating at
    $z=\SI{2500}{\meter}$. The units of the colorbar are \SI{}{m/s^2/d} }
  \label{fig:apparent-h-xt. This shows that the new simulations with fixed
    radiative cooling have enhanced multiscale structure inside the warm pool.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics{2017-03-07/clim-h}
  \caption{Vertical-horizontal climatology of the apparent buoyancy source. The units of the colorbar are \SI{}{m/s^2/d}.}
  \label{fig:h-clim}
\end{figure}

\subsection{Averaging operators and multi-scale decomposition}
\label{sec:orge0d3cb2}

When analyzing the output of a simulation, the full asymptotic expansion is not
available to us. Thus, the task is to reconstruct as many of the individual
terms in (\ref{eq:ansatz}) as possible using 1) the full fields
($u^{\epsilon}$), 2) an empirical spatial Reynold's average ( $\bar{\cdot}$ ),
and 3) an empirical temporal Reynold's average ($\tavg{\cdot}$).

By taking the temporal and spatial averages of the asympotic expansion, we
obtain
\begin{equation}
  \label{eq:xjfkadf}
  \bavg{u^{\epsilon}} = U(X,T) + \epsilon \tavg{\bar{u}}(x,X,t,T) + o(\epsilon).
\end{equation}
This indicates, that the zeroth and first order stationary components of the
planetary scale circulation cannot be separated. Subtracting this from the full
expansion gives
\begin{equation}
  \label{eq:kadvf}
  u^{\epsilon}- \bavg{u^{\epsilon}} =  \epsilon (\tilde{\bar u} + u') + o(\epsilon).
\end{equation}
The two components on the right hand side of this equation can be separated by
taking zonal averages. In summary, the only three significant quantities which
can be obtained from data are given by
\begin{alignat}{3}
  \label{eq:ansatz-from-data}
  &U(X,T)  &=& \bavg{u^{\epsilon}} + o(1)\\
  &\epsilon \tilde{\bar u} &=& \overline{u^{\epsilon}}- \bavg{u^{\epsilon}} + o(\epsilon)\\
  &\epsilon u' &=&  u^{\epsilon} - \overline{u^{\epsilon}} + o (\epsilon).
\end{alignat}
Therefore, the P, TH, and S equations above must be recombined into versions
involving only these quantities. 

\subsection{Multi-resolution analysis using splines}


The simplest approach to approximating the multi-scale averaging operators
$\tavg{\cdot}$ and $\bar{\cdot}$ is to truncate at some fixed spatial scale.
For example, the planetary-scale spatial average could be defined by the simple
moving average
\[\bar{f}(x) = \frac{1}{L}\int_{x- L/2}^{x +L/2}
    f dx.\] 
However, this operation is not practically useful for computing the average of
quadratic quantities such as the flux terms in (\ref{eq:fluxes-immd}) because it
is not idempotent.
This operation can be made into a projection be defining dividing the domain
into disjoint intervals $I_j = [(j-1) L , j L)$, and then defining the averaging
operation as 
\[\bar{f}(x) = \sum_j \chi_{I_j}(x) \bar{f}_j,\quad \bar{f}_j=\frac{1}{L} \int_{I_j} f(x) dx,\]
where $\chi_A: \mathbb{R} \rightarrow \{0,1\}$ is the characteristic function of the set $A$.
This approach is indeed a projection, but representing $\bar{f}$ as a piecewise
constant function can introduce large errors at the cell boundaries as can be
seen in Figure \ref{fig:coarse-avg}. Moreover, these errors appear to be biased. 
\begin{figure}
  \centering
  % this figure originall comes from journal/2017-02-16
  \includegraphics{oldfigs/coarse-avg-bad}
  \caption{\label{fig:coarse-avg}. This figure plots $u'-\bar{u}$ where
    $\bar{u}$ is the simple piecewise constant coarse-grained averaging
    operator. The error is larger at the boundaries  than the centers of the subdomains.}
\end{figure}

\begin{figure}
  \centering
  % this figure originally comes from journal/2017-02-16
  \includegraphics[width=4in]{oldfigs/SAM Walker Cell and Homogeneous SST_17_1}
  \caption{Cardinal B-splines of order 3.}
  \label{fig:card-b-splines}
\end{figure}

The failure of piecewise constant averaging operators suggests that we should
seek a higher degree of smoothness in our ``averaged'' field.
A natural generalization of piecewise constant functions are piecewise smooth polynomial splines.
The so-called B-splines provide a convenient basis for the splines, and the
piecewise constant functions are actually B-splines of order 0.
I have plotted the cardinal cubic B-splines in Figure \ref{fig:card-b-splines}
to demonstrate the degree of smoothness I am talking about.
Like the piecewise constant averaging, spline based smoothing is a least-squares regression operator, so it is a projection operator. Let $A_h$ be the matrix of periodic B-splines defined on knots seperated by a distance of $h$. Then, the operator which projects data onto the subspace spanned by the columns of $A_h$ is given by 
$$ S_h = A_h (A_h^T A_h)^{-1} A_h^T.$$

Given this expression, it is trivial to show that $S_h^2 = S_h$. This property is *not* satisfied for many other smoothing operations like the Gaussian filter I was using in the previous report. Other bases, such as the Fourier basis, satisfy this property, but the cubic spline basis is much more robust because the meso-scale spatial structure is non-stationary.

Finally, if there are a set of operators $S_{h_1}$, $S_{h_2}$, where $h_1 > h_2 > \ldots > h_n$ are different smoothing bandwidths. The data $x$ can be decomposed into separate scales in the following manner inductive manner

$x_n = S_{h_n} r_n, \quad r_n = r_{n-1} - x_n.$
Where $r_n$ is the residual after n steps, and $r_0 = x$ is the original data. I show below that the Perseval's relationship holds for this multiscale decomposition. Therefore, it will be possible to use this decomposition to assess quadratic eddy quantities in a parsimonuous fashion. The main downside of this approach is that desired scales need to be supplied externally, and are not calculated automatically from the data.

\begin{figure}
  \centering
  % \includegraphics[]{ms-decomp-u1.pdf}

  \inputpgf{../../results/2017-02-28}{ms-decomp-u1.pgf}
  \caption{Multiscale decomposition of the velocity field at $z=\SI{1000}{\m}$ using
    B-splines. With the exception of some large amplitude synoptic waves in the
    $u'$ field, the asymptotic scaling of these terms is consistent with
    \eqref{eq:ansatz}. The synoptic time fluctuation of the planetary scale flow
    shows an intriguing propagating structure.}
  \label{fig:multi-decomp-u}
\end{figure}


% \section{Multiscale theory for homogeneous SST}
% \label{sec:orgd3372dd}

% This case is tricky because there are three predominant scales rather than
% just two.

% \subsection{IPESD}
% \label{sec:org1b14c63}
% \subsection{IMESD}
% \label{sec:org1de362e}

\section{References}
\label{sec:org7ca0b7b}
\printbibliography

\appendix
\section{Thermodynamic Relations in SAM}
\label{sec:diag-q}

All thermodynamic quantities in SAM are related to the three prognostic
variables $h_L$, $q_T$, and $q_p$, as well as the anelastic reference profiles
like $p_0(z)$. In all, these relationships are given by
\begin{subequations}
  \begin{align*}
    h_L &= c_p T +gz - L_c (q_c + q_r) - L_s (q_i + q_s + q_g) \\
      q_v &= \omega_v q_T\\
    q_c &= (1-\omega_v)\omega_n  q_T \\
    q_i&= (1-\omega_v)(1-\omega_n) q_T \\
    q_s &= (1-\omega_p)(1-\omega_g) q_p \\
    q_g &= (1-\omega_p) \omega_g q_p \\
    q_r &= \omega_p q_p.
  \end{align*}
\end{subequations}
The water phase partitioning functions are all functions of temperature alone
which have the form given by
\[
  \omega_m = \max \left[  0, \min \left( 1, \frac{T- T_{00m}}{T_{0m}-T_{00m}}
    \right) \right]  \text{ for } m \in \{n,p,g\}.
\]
\[
  \omega_v = \min \left(1,   q_{sat}(T, p_0)/q_T\right)
\]
The definitions $q_T = q_v + q_c$ and $q_p = q_r + q_s + q_g $ are automatically
satisfied by the above set of equations, and are therefore linearly dependent,
so we do not include them in the list.

There are seven equations above and there are eleven unknowns quantities $h_L$,
$ q_p$, $ q_T$, $ T$, $p_0$, $ q_v$, $ q_c$, $ q_i$, $ q_r$, $ q_g$, and $ q_s$.
Thus, the implicit function theorem can be invoked to find any thermodynamic
quantity as a function of any four variables. The most convenient such set is
comprised by the three prognostic thermodynamic variables $q_T$, $h_L$, and
$q_p$ and the background pressure profile $p_0$. In fact, SAM contains a
nonlinear solver which finds the $T$ in exactly this way.

\begin{figure}[ht]
  \centering
  % \input{../../results/2017-03-02/pdplot.tikz}
  \inputpgf{../../results/2017-03-02}{pdplot.pgf}
  \caption{\label{fig:label} Partial derivatives of $T$ with respect to
    prognostic and/or easily accessible variables of SAM for a range of $T$. 
    Because of the complexity of the diagnostic system, these derivatives are
    computed using automatic differentiation.}
\end{figure}


% The ultimate quantity of interest




% In particular, the buoyancy $B=
% g \frac{\theta_v- \theta_{v0}}{\theta_{v0}}$ can be expressed in this way. Then
% the total differential of buoyancy is given by
% \newcommand{\pdfixed}[3]{\left(\pd{#1}{#2} \right)_{#3}}
% \renewcommand{\aa}{\pdfixed{B}{h_L}{q_T, q_p,p_0}}
% \newcommand{\bb}{\pdfixed{B}{q_T}{h_L, q_p,p_0}}
% \newcommand{\cc}{\pdfixed{B}{q_p}{h_L, q_T,p_0}}
% \newcommand{\dd}{\pdfixed{B}{p_0}{h_L, q_T,q_p}}

% \begin{alignat}{2}
%   \label{eq:dbdt}
%   dB =& &  \aa dh_L + \bb dq_T \nonumber\\
%          &+ &\cc dq_p + \dd d p_0.
% \end{alignat}

% \clearpage
% \noindent {\Large Useful values for making figures}\\
% \noindent Text width: \printinunitsof{in}\prntlen{\textwidth}\\
% Text height: \printinunitsof{in}\prntlen{\textheight}
\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
