\documentclass[final,hyperref={pdfpagelabels=false},20pt]{beamer}
% \mode<presentation> {
%   % \usetheme{Berlin}
% \usetheme{Dreuw} }
\usepackage{times} \usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage[english]{babel} \usepackage[latin1]{inputenc}
\usepackage[orientation=landscape,size=a0,scale=1.15,debug]{beamerposter}
\usepackage{graphicx} \usepackage{siunitx} \usepackage{calc}
\usepackage{printlen}
\usepackage{biblatex}
\addbibresource{references.bib}

% \mode<presentation> {
%   % \usetheme{Berlin}
% \usetheme{Dreuw} }

\usetheme{Dreuw}
% Font sizes

\newlength{\outercol} \newlength{\innercol} \setlength{\outercol}{.28\textwidth}
\setlength{\innercol}{\textwidth - 2\outercol-1in} \uselengthunit{in}


\newcommand{\avg}[1]{\langle \overline{#1} \rangle}
\newcommand{\whbox}[3]{\framebox{\parbox[t][#2][c]{#1}{#3}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 5
\graphicspath{{figures/}} \title[Fancy Posters]{A Framework for Assessing
  Multiscale Interactions in Cloud-Resolving Simulations of Tropical Convection}
\author[Dreuw \& Deselaers]{Noah D. 
  Brenowitz$^{*}$ and Andrew J. 
  Majda$^{*\dagger}$} \institute[]{$^{*}$Center for Atmosphere-Ocean Science,
  Courant Institute of Mathematical Sciences,
  New York University\\
  $^{\dagger}$Center for Prototype Climate Modeling, New York University Abu
  Dhabi} \date{Jul. 
  31th, 2007}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 5
\begin{document}
\begin{frame}{}
 \setlength{\TPHorizModule}{\textwidth}
 \setlength{\TPVertModule}{\textwidth}
  \begin{textblock}{.5}(.80, .03)
    \includegraphics[height=5cm]{{nyu_short_white}.eps}
  \end{textblock}

   \vspace{1ex}
  \begin{columns}[T]
    \begin{column}{\outercol}
      % column: \printlength{\columnwidth}
      \begin{block}{Motivation}
        {\bf What are the dominant organization mechanisms in realistic tropical convection?}

        A variety of physical mechanisms are known to contribute to
        organized convection of different types. 
        In cloud resolving simulations without a large-scale wind-shear
        structure, convection often self-aggregates into stationary clumps
        through surface-flux and radiative feedbacks. However, 
        the prevalence of this archetype of organized convection in the tropics
        is debatable given observed tropical wind-shears and modeling evidence.

        Other observations and theories highlight the role of multiscale interactions in phenomena like the MJO.
        Multiscale asymptotic models for the tropical atmosphere predict that
        nonlinearity plays a limited and very specific role in driving planetary-scale
        circulations. In the intraseasonal planetary equatorial synoptic
        dynamics (IPESD) models due to Majda and Biello, the nondimensionalized zonal-momentum equation for the planetary/intraseasonal spatio-temporal scale is given by
        \[ \avg{u}_T  - y \avg{v} = -\avg{P}_X - \frac{1}{\rho}(\rho \avg{u'w'})_z+ \avg{S_u},\]
        where $\overline{\cdot}$, and $\langle \cdot \rangle$ are asymptotic
        spatial and temporal averages, respectively.
        The only nonlinear term in this equation is the vertical eddy-flux divergence of horizontal velocity, which we show plays an important role in the long time behavior of 2D cloud resolving simulations.
      \end{block}

      \begin{block}{Numerical multiscale decomposition}
        We desire a numerical operator $P_n$ which approximates the
        asymptotic Reynold's average operators used in the multiscale theories.

      $P_n$  is a simple low-pass filter in zonal wavenumber space:
        \[ \frac{1}{1+\alpha_{n} |k_x|^4}, \quad \text{$\alpha_n$ is chosen so
            that $\sum_k \Lambda(k) = n$ }.\]

        The field $P_n u$ is a smoothed version of $u$, which has $n$
        \emph{effective degrees of  freedom}. $\Lambda_n = L_x/n$ is the
        effective wavelength cutoff of the filter.

        \begin{figure}[ht]
          \centering
          \includegraphics[width=\linewidth]{figs/lowpass-perf.pdf}
          \caption{Performance of $P_n$ for $u|_{z=\SI{3}{\km}}$ and
            $w|_{z=\SI{5}{\km}}$. The low-pass filter is
            performed for $n\in\{4,8,16,32\}$.}
        \end{figure}

        {\bf Multiscale decomposition using $P_n$}

          Planetary-scale: $f^P = P_{6} f$ \hspace{ 3em } Synoptic-scale: $f^{S} = P_{20}(f-f^P)$\\
          Mesoscale: $f^M = f - f^P - f^S$
      \end{block}
    \end{column}

    \begin{column}{\innercol}
      % column: \printlength{\columnwidth}

      \begin{block}{Multiscale structures in 2D cloud resolving models}

        \begin{columns}[t]
          \begin{column}{.57\linewidth}
          {\bf  Hovmoller diagrams of brightness temperature (TB) for three model
          configurations}
          \begin{center}
            \includegraphics[width=\linewidth]{figs/hov.pdf}
          \end{center}
          
          \end{column}

          \begin{column}{.4\linewidth}
            {\bf Synoptic and Planetary Scale Structures}

            Day 100 of fixed radiation simulation without rotation
            \begin{center}
              \includegraphics[width=.98\linewidth]{figs/msxz.pdf}
            \end{center}
            % {\bf Multiscale decomposition for day 100}
          \end{column}
        \end{columns}

          For simulations simulation with and without interactive radiation, an obvious planetary-scale envelope of convective activity forms. 
          These envelopes propagate with respect to the fixed frame as well as the
          reference frame which moves the vertically-averaged velocity ($\approx
          \SI{-10}{\m\per\s}$). The simulation with a rotation rate representative of summer-time position of the ITCZ has no obvious planetary scale organization. 
          The multiscale decomposition algorithm can decompose the zonal
          velocity and virtual potential temperature anomalies as well as the
          vertical velocity field into mesoscale, synoptic, and planetary-scale components.

        
      \end{block}

      \begin{block}{Upscale feedbacks on the large-scale kinetic energy budget}
        \begin{columns}
          \begin{column}{.5\textwidth}
            The column planetary scale kinetic energy budget can help reveal the
            mechanisms behind the the planetary scale convective
            organization in these simulations. Let $u= u^P + u'$, and similarly
            for all variables. The zonal momentum equation is given by

            \[\frac{\partial u^P}{\partial t} = \underbrace{- (\phi_x)^P}_G  \underbrace{-\frac{1}{\rho_0} (\rho_0 (u' w')^P )_z}_{V} + A + D.
              \]

            Define the following notation for mass-weighted vertical averaging: $\langle f \rangle = \int_0^{H} \rho_0 f dz$. Then, the column large-scale kinetic energy for $KE^P = \frac{1}{2}\langle (u^P)^2 \rangle $ is given by
            \[\frac{\partial\log{( KE^P )}^{1/2}}{\partial t} =\frac{\langle u^P G \rangle}{KE^P}+ \frac{\langle u^P V \rangle}{KE^P}  + \ldots \]

            The first term is the ``upscale feedback'' and the second term
            is the ``pressure gradient feedback''. These quantities are visualized
            in $(x,t)$ space and also as domain averaged quantities.
            
          \end{column}
          \begin{column}{.50\textwidth}
              \includegraphics[width=.9\linewidth]{figs/kehov}\\
              \includegraphics[width=.9\linewidth]{figs/ke-time}
          \end{column}
        \end{columns}
      \end{block}
          
    \end{column}
    \begin{column}{\outercol}
\begin{block}{Model configuration}
        \begin{itemize}
        \item System for Atmospheric Modeling (v6.10.9)
        \item Two-dimensional setup: $(x,z)$
          \begin{itemize}
          \item Latitude $=$ 0 $\implies$ no rotation $\implies v=0$
          \item If latitude $\ne 0$, then $v$ can be nonzero
          \end{itemize}
        \item Domain: $L_x =\SI{32000}{\km}$ and $\Delta x = \SI{2}{\km}$.
        \item Fixed SST: \SI{300.15}{\kelvin}
        \item Large-scale forcing: relaxation towards $u=\SI{-10}{\m\per\s}$
          with $\SI{1}{day}$ time-scale. Boundary layer processes $\implies$ wind shear.
        \item Rotation rates: $f(\SI{0}{\degree}) = 0$  and $f(\SI{6}{\degree}) =(\SI{18}{\hour})^{-1}$
        \item Radiation
          \begin{itemize}
          \item Active long-wave and shortwave radiation
          \item Fixed \SI{-1.5}{\kelvin\per\day} cooling in troposphere
          \end{itemize}
        \end{itemize}
      \end{block}

      \begin{block}{Conclusions}
        \begin{itemize}
        \item Planetary-scale propagating convective envelopes form in two dimensional zonal simulations of tropical convection
        \item This behavior is very sensitive to rotation. There is no
          planetary scale circulation in the \SI{6}{\degree N} 2D simulations.
        \item The planetary convective patterns are similar in
          simulations with active radiation and fixed radiative cooling. This
          implies that this organization does not result from a typical
          self-aggregation (e.g. radiative) feedback.
        \item A multiscale analysis of the kinetic energy budget  suggests that vertical momentum transports from synoptic and smaller scales play a key role in driving the planetary scale pattern.
        \end{itemize}
        
      \end{block}

      \begin{block}{Next steps}
        \begin{itemize}
        \item Analyze buoyancy (i.e. virtual temperature) and moisture budgets
          \begin{itemize}
          \item How do eddy-fluxes of temperature and moisture compare with latent heating and precipitation processes?
          \end{itemize}
          
        \item Long term ($> \SI{300}{\day}$) behavior
          \begin{itemize}
          \item Does the planetary scale pattern persist?
          \item Break-up?
          \item Re-organize?
          \end{itemize}
        \item Additional simulations at latitudes of \SIrange{1}{3}{\degree N}
          \begin{itemize}
          \item Is there planetary scale organization in simulations with weak
            but nonzero rotation?
          \end{itemize}
        \item Three-dimensional simulations
        \end{itemize}
        
      \end{block}

      \begin{block}{References}
        \renewcommand*{\bibfont}{\scriptsize}
        \nocite{Majda2007}
        \nocite{GrabowskiMoncrieff2001}
        \nocite{Khairoutdinov2013}
        \nocite{Bretherton2015}
        \printbibliography
        
      \end{block}
          \end{column}
  \end{columns}
  
  % \vfill
  % \begin{block}{\large Fontsizes}
  %   \centering
  %   {\tiny tiny}\par
  %   {\scriptsize scriptsize}\par
  %   {\footnotesize footnotesize}\par
  %   {\normalsize normalsize}\par
  %   {\large large}\par
  %   {\Large Large}\par
  %   {\LARGE LARGE}\par
  %   {\veryHuge VeryHuge}\par
  %   {\VeryHuge VeryHuge}\par
  %   {\VERYHuge VERYHuge}\par
  % \end{block}
  % \vfill
  % \vfill
  % \begin{block}{\large Fontsizes}
  %   \centering
  %   {\tiny tiny}\par
  %   {\scriptsize scriptsize}\par
  %   {\footnotesize footnotesize}\par
  %   {\normalsize normalsize}\par
  %   {\large large}\par
  %   {\Large Large}\par
  %   {\LARGE LARGE}\par
  %   {\veryHuge VeryHuge}\par
  %   {\VeryHuge VeryHuge}\par
  %   {\VERYHuge VERYHuge}\par
  % \end{block}
  % \vfill
  % \begin{columns}[t]
  %   \begin{column}{.48\linewidth}
  %     \begin{block}{Introduction}
  %       \begin{itemize}
  %       \item some items
  %       \item some items
  %       \item some items
  %       \item some items
  %       \end{itemize}
  %     \end{block}
  %   \end{column}
  %   \begin{column}{.48\linewidth}
  %     \begin{block}{Introduction}
  %       \begin{itemize}
  %       \item some items and $\alpha=\gamma, \sum_{i}$
  %       \item some items
  %       \item some items
  %       \item some items
  %       \end{itemize}
  %       $$\alpha=\gamma, \sum_{i}$$
  %     \end{block}

  %     \begin{block}{Introduction}
  %       \begin{itemize}
  %       \item some items
  %       \item some items
  %       \item some items
  %       \item some items
  %       \end{itemize}
  %     \end{block}

  %     \begin{block}{Introduction}
  %       \begin{itemize}
  %       \item some items and $\alpha=\gamma, \sum_{i}$
  %       \item some items
  %       \item some items
  %       \item some items
  %       \end{itemize}
  %       $$\alpha=\gamma, \sum_{i}$$
  %     \end{block}
  %   \end{column}
  % \end{columns}
\end{frame}
\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: t
%%% End:
