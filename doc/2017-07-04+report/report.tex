\documentclass{article}
\usepackage{graphicx}
\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage{siunitx}
\usepackage[margin=1in]{geometry}

\usepackage{subfig}

\graphicspath{{../2017-06-20-aofd-poster/}{./}}

\usepackage{biblatex}
\addbibresource{references.bib}


\title{Synoptic scale feedbacks on the planetary scale kinetic energy budget:
  summary of AOFD poster}
\author{Noah D. Brenowitz and Andrew J. Majda}
\date{July 4, 2017}


\newcommand{\avg}[1]{\langle \overline{#1} \rangle}
\newcommand{\vavg}[1]{\{#1\}}

\begin{document}
\maketitle

\begin{abstract}
  This report summarizes the results from the AOFD poster.

  A analysis of column budget of large-scale kinetic energy shows that the both
  upscale eddy momentum fluxes and large-scale pressure gradients have a large
  impact on the large-scale kinetic energy. 
  The zonally averaged large-scale pressure gradient feedback changes
  sign. On the other hand, the upscale momentum flux term almost always acts to increase the
  amplify the planetary-scale kinetic energy.

  As requested, this report also gives details about the momentum equation and
  the large scale forcing. 
  The simulations all have a Newtonian relaxation term which damps the velocity
  towards a barotropic wind profile, meant to represent the impact of a
  large-scale environment on the local convection.
  In this context, this large-scale wind structure could possibly be imposed by
  the zonal-mean trade winds.
  This setup was chosen to replicate the results of
  \textcite{GrabowskiMoncrieff2001}, and the resulting zonally-averaged
  horizontal wind is not ``equal'' to this term because of the other terms in
  the momentum equation. 
  In particular, strong shear develops in the lower atmosphere, which likely
  plays an important role in the strongly organized mesoscale structures seen in
  these simulations.

\end{abstract}

\section{Important Notation}
  
The plantary-scale average of a field $f$ is given by$f^P = \int g(x,t-s) P_{6}
f(x,s) ds$, where $g$ is a gaussian kernel with a width of $\SI{2}{\day}$ and
$P_6$ is a low-pass filter which filters out spatial wave numbers larger than
$k=6$. The residual from this planetary-scale average will be denoted using $f'
= f - f^P$. See Section \ref{sec:msdecomp} for more details about how these operators
are defined.

Averaging operators: $\langle  \cdot \rangle$, $\overline{\cdot}$, and
$\vavg{\cdot}$ are temporal, zonal, and vertical averaging operators, respectively.

\section{Motivation}

        A variety of physical mechanisms are known to contribute to organized
convection of different types. In cloud resolving simulations without a
large-scale wind-shear structure, convection often self-aggregates into
stationary clumps through surface-flux and radiative feedbacks. However, the
prevalence of this archetype of organized convection in the tropics is debatable
given observed tropical wind-shears and modeling evidence.

        Other observations and theories highlight the role of multiscale
interactions in phenomena like the MJO. Multiscale asymptotic models for the
tropical atmosphere predict that nonlinearity plays a limited and very specific
role in driving planetary-scale circulations. In the intraseasonal planetary
equatorial synoptic dynamics (IPESD) models due to Majda and Biello \autocite{Majda2007}, the
nondimensionalized zonal-momentum equation for the planetary/intraseasonal
spatio-temporal scale is given by
        \[ \avg{u}_T - y \avg{v} = -\avg{P}_X - \frac{1}{\rho}(\rho
\avg{u'w'})_z+ \avg{S_u},\] where $\overline{\cdot}$, and $\langle \cdot
\rangle$ are asymptotic spatial and temporal averages, respectively. The only
nonlinear term in this equation is the vertical eddy-flux divergence of
horizontal velocity, which we show plays an important role in the long time
behavior of 2D cloud resolving simulations.

\section{Model Configuration}


\begin{itemize}
\item System for Atmospheric Modeling (v6.10.9)
\item Two-dimensional setup: $(x,z)$
  \begin{itemize}
  \item Latitude $=$ 0 $\implies$ no rotation $\implies v=0$
  \item If latitude $\ne 0$, then $v$ can be nonzero
  \end{itemize}
\item Domain: $L_x =\SI{32000}{\km}$ and $\Delta x = \SI{2}{\km}$.
\item Fixed SST: \SI{300.15}{\kelvin}
\item Large-scale forcing: relaxation towards $u=\SI{-10}{\m\per\s}$
  with $\SI{1}{day}$ time-scale. Boundary layer processes $\implies$ wind shear.
\item Rotation rates: $f(\SI{0}{\degree}) = 0$  and $f(\SI{6}{\degree}) =(\SI{18}{\hour})^{-1}$
\item Radiation
  \begin{itemize}
  \item Active long-wave and shortwave radiation
  \item Fixed \SI{-1.5}{\kelvin\per\day} cooling in troposphere
  \end{itemize}
\end{itemize}


\subsection{Momentum equation and source terms}
\label{sec:mom-eq}

In the original paper on SAM \autocite{Khairoutdinov2003}, the momentum
equation is given in Einstein tensor notation by 

\begin{equation}\label{eq:mom}
 \frac{\partial u_i}{\partial t} + \epsilon_{ij3} 2 \Omega \sin(\varphi) u_i = - \frac{1}{\rho_0}\frac{\partial}{\partial
    x_j} (\rho_0 u_i u_j + \tau_{ij}) - \frac{\partial}{\partial x_i}
  \frac{p'}{\rho_0} + \delta_{i3} B - \frac{u_i - U_i^{LS}}{\tau_r} - \frac{u_i}{\tau_{sponge}(z)}
\end{equation}


The sub-grid-scale (SGS) stress tensor $\tau_{ij}$ is given by a
Smagorinsky-type turbulence closure. The buoyancy is given by $B= g
\frac{T_v-T_{v0}}{T_{v0}}$ where $T_v$ is the virtual temperature. The
base-state density profile is given by $\rho_0(z)$ and $p'$ is the perturbation
pressure. The effect of the earth's rotation is given by coriolis term on the
left hand side of the equation. The rotation parameter is $\Omega =
\frac{2\pi}{\SI{1}{\day}}$ and $\varphi$ is the latitude in radians. Because the
simulations presented here are two-dimensional $\varphi$ will be fixed at a
single value. The velocity field is damped in the stratosphere of the domain using
a sponge-layer, which increases in strength with height.
 
To approximate the effect of a large-scale environment on the local evolution of
the flow, a Newtonian relaxation term is included. This term relaxes the
velocity field towards a large-scale velocity field  $U_i^{LS}$ with an
e-folding time scale of $\tau_r$. The simulations here follow the setup of
\textcite{GrabowskiMoncrieff2001} very closely. All components of the
large-scale wind-field are set to zero except for the zonal component, which is
given by the barotropic forcing
\[U_1^{LS}(z) = -\SI{10}{\m\per\s} \forall z.\]
To be clear, $U_1^{LS}$ is constant for all heights including the boundary layer
and the stratosphere.
The damping time-scale is given by
$\tau_r = \SI{1}{\day}$. 
Because this damping time-scale is much longer than the times-scales of
turbulent processes in the boundary layer and the sponge layer in the upper
atmosphere, the actual zonal velocity field $\overline{u_1}$ will differ from this
hypothetical large-scale velocity field.

This large-scale wind serves two primary purposes in the simulations. 
First, in increases the surface-wind and thus the surface fluxes of temperature
and moisture. 
Second, the combined effect of the SGS turbulence scheme, resolved vertical
momentum transports, and the large-scale forcing induces a shear in the lower
atmosphere. 
This enhanced moisture and shear in combination lead to the strong mesoscale organization of
the simulations.

Taking the zonal average of the x-component of (\ref{eq:mom}) gives 
\[ 
  \frac{\partial \bar{u}}{\partial t} - 2 \Omega \sin(\varphi) \bar{v} = - \frac{1}{\rho_0}\frac{\partial}{\partial
    z} (\rho_0 \overline{w' u'} + \tau_{13}) - \frac{\bar{u} - U_1^{LS}}{\tau_r} - \frac{\bar{u}}{\tau_{sponge}(z)}
  \]
where I have adopted the convenient notation that $u=  u_1$ and $w=u_3$.



\section{Multiscale decomposition}
\label{sec:msdecomp}

\subsection{Zonal average}

        We desire a numerical operator $P_n$ which approximates the asymptotic
Reynold's average operators used in the multiscale theories.

      $P_n$ is a simple low-pass filter in zonal wavenumber space:
        \[ \frac{1}{1+\alpha_{n} |k_x|^4}, \quad \text{$\alpha_n$ is chosen so
that $\sum_k \Lambda(k) = n$ }.\]

        The field $P_n u$ is a smoothed version of $u$, which has $n$
\emph{effective degrees of freedom}. $\Lambda_n = L_x/n$ is the effective
wavelength cutoff of the filter.

        \begin{figure}[ht] \centering
          \includegraphics[width=\linewidth]{figs/lowpass-perf.pdf}
          \caption{Performance of $P_n$ for $u|_{z=\SI{3}{\km}}$ and
$w|_{z=\SI{5}{\km}}$. The low-pass filter is performed for $n\in\{4,8,16,32\}$.}
        \end{figure}

        A multiscale decomposition can be defined using the smoothing operator
        in the following manner.
        
        \framebox{
        \begin{tabular}{ll} Planetary-scale& $f^P = P_{6} f$ \hspace{ 3em }\\
Synoptic-scale& $f^{S} = P_{20}(f-f^P)$\\ Mesoscale& $f^M = f - f^P - f^S$
        \end{tabular}}

        For the two scale analyses in this report, we will alternate between the
        shorthands $f^P = \overline{f}$ for planetary scale spatial averaging.

\subsection{Temporal average}

The temporal filter we use is a simple gaussian filter. This means that
\[\langle f \rangle = g * f = \int g(s) f(x,t-s) ds \] where $g \propto
\exp{-\frac{t^2}{2 (\sigma_T^2)}} $. For planetary scale averaging, we use a
value of \SI{2}{\day} for the bandwidth parameter $\sigma_T$. Without temporal
filtering the Hovmoller diagrams can be noisy and more difficult to interpret.


\section{Results}


\subsection{Multiscale structures in 2D cloud resolving models}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{figs/hov.pdf}
  \caption{Hovmoller diagrams of brightness temperature (TB) for three model
    configurations}
  \label{fig:hob-tb}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=.98\linewidth]{figs/msxz.pdf}
  \caption{Synoptic and Planetary scale structures for single snapshot at
    $t=\SI{100}{\day}$ of simulation with no rotation and fixed-radiation. 
    This diagram shows that the decomposition procedure is effective at
    separating different scales. 
    The planetary scale pattern propagates to the west (left) and is tilted
    up-and-east. 
    The ascent/descent regions are associated with negative/positive $\theta_v$
    anomalies. 
    This has the simple interpration that the planetary scale structure is less
    stable in the ascent region.}
  \label{fig:multiscale-vert}
\end{figure}

For simulations simulation with and without interactive radiation, an obvious planetary-scale envelope of convective activity forms. 
These envelopes propagate with respect to the fixed frame as well as the
reference frame which moves the vertically-averaged velocity ($\approx
\SI{-10}{\m\per\s}$). The simulation with a rotation rate representative of summer-time position of the ITCZ has no obvious planetary scale organization. 
The multiscale decomposition algorithm can decompose the zonal
velocity and virtual potential temperature anomalies as well as the
vertical velocity field into mesoscale, synoptic, and planetary-scale components.

\subsection{Zonal mean winds}

\begin{figure}[h]
  \centering
  \subfloat[Time evolution of zonal mean wind]{\includegraphics[height=2in]{figs/u-time.pdf}}
  \subfloat[Mean final 80 days]{\includegraphics[height=2in]{figs/u-mean.pdf}}
  \caption{Zonal mean horizontal momentum for the fixed radiation simulation
    shown in the left panel of Figure \ref{fig:hob-tb}. Contours every \SI{1}{\m\per\s}.}
  \label{fig:u-zonal}
\end{figure}

As discussed in \ref{sec:mom-eq} the zonal mean winds of the simulation develop
from the interaction of the large-scale forcing, SGS turbulence, and momentum
transports. 
Here, I plot the zonally averaged horizontal winds for the simulation with fixed
radiative cooling and no rotation. 
Figure \ref{fig:u-zonal} contains a contour plot of the time evolution of the
mean zonal winds as well as the average over the final 80 days of the
simulation. 
The simulations develops very strong vertical shear between the surface and 900
hPa, which likely plays a key role in organizing the convection seen in the simulation.
The sponge-layer in the upper part of the domain strongly damps the velocity
field there resulting in very intense shear near the tropopause.


\subsection{Upscale feedbacks on the large-scale kinetic energy budget}
The column planetary scale kinetic energy budget can help reveal the
mechanisms behind the the planetary scale convective
organization in these simulations. Let $u= u^P + u'$, and similarly
for all variables. The large zonal momentum equation is given by

\[\frac{\partial u^P}{\partial t} = \underbrace{- \partial_x \left( \frac{p'}{\rho_0} \right)^P}_G  \underbrace{-\frac{1}{\rho_0} (\rho_0 (u' w')^P )_z}_{V} + A + D.
\]
The advection ($A$) and dissipation ($D$) equations are taken to include all
effects not captured by pressure gradient or the eddy-flux term. These efects
include horizontal momentum transport, SGS turbulence, and the large-scale
Rayleigh damping term.
Define the following notation for mass-weighted vertical averaging: $\vavg{ f } = \int_0^{H} \rho_0 f dz$. Then, the column large-scale kinetic energy for $KE^P = \frac{1}{2}\vavg{ (u^P)^2 } $ is given by
\[\frac{\partial\log{( KE^P )}^{1/2}}{\partial t} =\frac{\vavg{ u^P G }}{KE^P}+ \frac{\vavg{ u^P V }}{KE^P}  + \ldots \]

The first term is the ``upscale feedback'' and the second term
is the ``pressure gradient feedback''. These quantities are visualized
in $(x,t)$ space and also as domain averaged quantities.

\begin{figure}
  \subfloat[Space-time \label{fig:kext}]{\includegraphics[width=.9\linewidth]{figs/kehov}}\\
  \subfloat[Zonal mean of Figure \ref{fig:kext}]{\includegraphics[width=.9\linewidth]{figs/ke-time}} \caption{Feedbacks for the large-scale kinetic energy budget.}
\end{figure}

\section{Conclusions and next steps}

Conclusions:

\begin{itemize}
\item Planetary-scale propagating convective envelopes form in two dimensional zonal simulations of tropical convection
\item This behavior is very sensitive to rotation. There is no
  planetary scale circulation in the \SI{6}{\degree N} 2D simulations.
\item The planetary convective patterns are similar in
  simulations with active radiation and fixed radiative cooling. This
  implies that this organization does not result from a typical
  self-aggregation (e.g. radiative) feedback.
\item A multiscale analysis of the kinetic energy budget  suggests that vertical momentum transports from synoptic and smaller scales play a key role in driving the planetary scale pattern.
\end{itemize}

Next steps:

\begin{itemize}
\item Analyze buoyancy (i.e. virtual temperature) and moisture budgets
  \begin{itemize}
  \item How do eddy-fluxes of temperature and moisture compare with latent heating and precipitation processes?
  \end{itemize}
  
\item Long term ($> \SI{300}{\day}$) behavior
  \begin{itemize}
  \item Does the planetary scale pattern persist?
  \item Break-up?
  \item Re-organize?
  \end{itemize}
\item Additional simulations at latitudes of \SIrange{1}{3}{\degree N}
  \begin{itemize}
  \item Is there planetary scale organization in simulations with weak
    but nonzero rotation?
  \end{itemize}
\item Three-dimensional simulations
\end{itemize}

\printbibliography

\end{document}
