import matplotlib.pyplot as plt
import xarray as xr
import xnoah
import sys


file_to_plot = snakemake.input[0]
data = xr.open_dataset(file_to_plot, chunks={'time': 10})['TB']
tb = xnoah.coarsen(data, x=32)

fig, ax = plt.subplots(figsize=(4,7), constrained_layout=True)
tb['x'] = tb.x/1000
tb.plot.pcolormesh(x='x', y='time', cmap='inferno', ax=ax)
ax.set_xlabel('x (1000 km)')
ax.set_ylabel('time (day)')
fig.savefig(snakemake.output[0])
