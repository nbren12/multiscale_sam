data:
	snakemake data

analysis:
	snakemake data/processed/1/H.nc

setuphooks:
	rsync -a git-hooks/ .git/hooks/

.PHONY: data
