"""
D{dim} = derivative
F{dof} = filter with dof 10
"""
import os
from functools import partial
from glob import glob
from math import pi

import dask.array as da
import numpy as np
import xarray as xr
from dask.diagnostics import ProgressBar
from metpy.constants import g as grav, Cp_d, Rd
from metpy.units import units

import xnoah.xarray
import xnoah.xcalc

from lib.material import matderiv
from lib.thermo import calc_adz, calc_h, calc_n2, calc_T


# input/output

def xopen(name, nt=20):
    return xr.open_dataset(name, chunks=dict(time=nt))\
        .apply(lambda x: x.squeeze())

def stat(wildcard):
    return SamRun(wildcard.run, root=config['dataroot']).stats

def coarsen(ncfiles, threads, x=32, time=2, fields=None):
    from multiprocessing.pool import ThreadPool
    import dask
    print("Threads=", threads)
    dask.set_options(pool=ThreadPool(threads))

    return xr.open_mfdataset(ncfiles, chunks={'time':time},
                             concat_dim='time',
                             preprocess=partial(calc_coarsedata, x=x, fields=fields))

def calc_coarsedata(ds, x=32, fields=None):
    def f(A):
        try:
            return xnoah.xarray.coarsen(A, x=x)
        except ValueError:
            return A

    # this step is needed to align grid with moments data
    if fields is not None:
        ds = ds[fields]
    dc = ds.apply(f)
    dc['x'] -= dc.x[0]

    # compute dry static energy
    # dc['s'] = dc.TABS + dc.z * 9.81/1004

    return dc



# Snakefile rules below #######################################################

wildcard_constraints:
    field="[0-9,A-Z,a-z,_]+",
    run = "[0-9,a-f]+",
    moms =".*/moments",
    n = "[0-9]+"

# Operators ###############################################################################

rule vertdiv:
    input: f="{d}/{name}.nc",
           stat="data/processed/1/stat.nc"
    output: "data/processed/{run}/V/{name}.nc"
    run:
        rho = xr.open_dataset(input.stat).RHO[-1]
        d = xopen(input.f)
        xr.Dataset({k: anelastic_vertdiv(d[k], rho) for k in d.data_vars if 'z' in d[k].dims})\
          .to_netcdf(output[0])

rule filter:
    input: "{d}/{name}.nc"
    output: "{d}/F6/{name}.nc"
    params: dim="x", dof="6"
    script: "scripts/filter.py"

# Other Rules ###############################################################################

rule stat:
    input: "data/raw/{run}/data/stat.nc"
    output: "data/processed/{run}/stat.nc"
    shell: "cp {input} {output}"

rule time_mean:
    input: "data/processed/{run}/moments.nc"
    output: "data/processed/{run}/tmean.nc"
    run:
        xopen(input[0]).sel(time=slice(20, None))\
            .mean('time')\
            .to_netcdf(output[0])

rule xtmean:
    input: "data/{d}/tmean.nc"
    output:"data/{d}/xtmean.nc"
    shell: "ncwa -a x {input} {output}"

rule moms:
    output: "data/processed/{run}/moments.nc"
    run:
        files = glob("data/raw/{run}/OUT_MOMENTS/*.nc"
                     .format(run=wildcards.run))

        flds = config['moment_fields']
        xr.open_mfdataset(files, chunks={'time': 100})[flds]\
            .to_netcdf(output[0])

rule n2:
    input: "{d}/xtmean.nc", stat="{d}/stat.nc"
    output: "{d}/n2.nc"
    run:
        d = xr.open_dataset(input[0])
        p = xr.open_dataset(input.stat).p
        calc_T(d.THV1, p)\
            .pipe(calc_n2)\
            .to_netcdf(output[0])

rule adz:
    input: "{d}/stat.nc"
    output: "{d}/adz.nc"
    run:
        rho = xopen(input[0]).RHO[0,:]
        calc_adz(rho)\
            .to_dataset(name='adz')\
            .to_netcdf(output[0])

rule partial_pres:
    input: "data/raw/{run}/data/3d/PP.nc",
           stat="data/raw/{run}/data/stat.nc"
    output: temp("data/processed/{run}/pp.nc")
    threads: 4
    run:
        with ProgressBar():
            data = coarsen([input[0]], threads, x=32, fields=['PP'])
            # for some reason the z coordinate of these arrays
            # is all screwed up
            # I will just patch the correct coordinates
            # TODO do this on the top level
            data['z'] = xr.open_dataset(input.stat).z
            # save the output
            data.to_netcdf(output[0])

rule coarsen_2d:
    output: "{run}/c2d.nc"
    threads: 4
    run:
        # default 2d fields
        fields = config.get('2dfields', ['TB'])
        run = SamRun(wildcards.run, root=config['dataroot'])
        ncfiles = run.data2d
        with ProgressBar():
            coarsen(ncfiles, threads, x=16, fields=fields).to_netcdf(output[0])

rule time_interpolation:
    input: PP="data/processed/{run}/pp.nc",
           s="data/processed/{run}/stat.nc",
           mom="data/processed/{run}/moments.nc"
    output: "data/processed/{run}/phi.nc"
    run:
        PP = xr.open_dataset(input.PP).PP
        rho = xr.open_dataset(input.s).RHO[-1]
        moms = xr.open_dataset(input.mom, cache=False)
        xnoah.xarray.linint(PP/rho, moms, 'time')\
            .to_dataset(name='phi')\
            .to_netcdf(output[0])

rule bouyancy:
    input:  m="data/processed/{run}/moments.nc",
            stat="data/processed/{run}/stat.nc",
            d0="data/processed/{run}/xtmean.nc"
    output: "data/processed/{run}}/B.nc"
    # shell:
    #     """
    #     cdo mulc,9.81 -div -sub -selname,THV1 {input.m} \
    #              -enlarge,{input.m} -selname,THV1 {input.d0} \
    #              -enlarge,{input.m} -selname,THV1 {input.d0}\
    #         {output}
    #     """
    run:
        stats = xopen(input.stat)

        thv = xopen(input.m)['THV1']

        # s = stats.isel(time=0)
        # thv0 = calc_thv0(s.TABS, s.QV, s.p)

        # use spatial and temporal mean to compute reference buoyancy profile
        thv0 = xr.open_dataset(input.d0)['THV1']
        B = 9.81 * (thv - thv0)/thv0
        B = B.to_dataset(name="B")
        B['B'].attrs[ 'units' ] = "m/s^2"
        B.to_netcdf(output[0])


rule matder:
    input: vel="{d}/moments.nc",
            fld="{d}/{field}.nc",
    output:"{d}/{field}.matd.nc"
    script: "scripts/matderiv.py"

rule stream_function:
    input:  m="{d}/moments.nc", adz="{d}/adz.nc"
    output: "{d}/psi.nc"
    run:
        adz = xr.open_dataset(input.adz).adz
        u = xopen(input.m).U1

        calc_stream(u, adz)\
        .transpose('time', 'z', 'x')\
        .to_dataset(name="psi")\
        .to_netcdf(output[0])


rule apparent_buoyancy_source:
    input: dbdt="data/processed/{run}/B.matd.nc",
           m="data/processed/{run}/moments.nc",
           s="data/processed/{run}/stat.nc",
           n2="data/processed/{run}/n2.nc",
           mean="data/processed/{run}/xtmean.nc"
    output: "data/processed/{run}/H.nc"

    run:

        m = xopen(input.m)
        s = xopen(input.s)
        th  = xr.open_dataset(input.mean)['THV1']

        dbdt = xopen(input.dbdt)['B']
        qrad = s.TTEND.isel(time=-1)
        n2 = xr.open_dataset(input.n2)['N2']

        calc_h(dbdt, n2, m.W1, th, s.p, qrad)\
            .to_dataset(name="H")\
            .to_netcdf(output[0])



rule momentum_budget:
    input: f="data/processed/{run}/F6/moments.nc",
           o="data/processed/{run}/moments.nc"
    output: "data/processed/{run}/uw.nc"
    run:
        f = xopen(input.f)
        o = xopen(input.o)

        up = f.U1-o.U1
        wp = f.W1-o.W1

        (up*wp).to_dataset(name="upwp").to_netcdf(output[0])
