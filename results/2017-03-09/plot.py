import matplotlib as mpl
import matplotlib.pyplot as plt

def plot_ms(A, name='u', **kwargs):
    if A.name is not None:
        name =A.name

    A.scale.values = ['$'+name+'$',
                      r'$\tilde{\bar'+name+'}$',
                      r'$\langle\bar{' +name+r'}\rangle $']

    template = "{value}"
    fg= A.plot(col='scale', add_colorbar=False,
               rasterized=True, **kwargs)
    fg.set_titles("{value}")
    plt.subplots_adjust(wspace=.01)
    fg.set_xlabels
    fg.add_colorbar()
    fg.set_ticks(max_xticks=5)
    return fg
