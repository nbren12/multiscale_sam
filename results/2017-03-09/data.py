import os
from glob import glob
from math import pi

import numpy as np
import xarray as xr
from metpy.constants import g as grav
from metpy.constants import Cp_d, Rd
from metpy.units import units
from scipy.interpolate import UnivariateSpline

import gnl.xarray
import gnl.xcalc
from gnl.xarray import coarsen
from gnl.xspline import Spline
from gnl.xlearn import XSVD

Q_ = units.Quantity
kappa = (Rd/Cp_d).to("1").magnitude
gr  = grav.magnitude

gamma = (grav/Cp_d).to("K/m").magnitude

class SamRun(object):
    def __init__(self, rev, root="."):
        g = glob("{}/*-{}*".format(root, rev))

        if len(g) == 0:
            raise ValueError("Specified ID not found")
        elif len(g) > 1:
            raise ValueError("Specified ID is not unique")
        else:
            self.rev = g[0]

    @property
    def data2d(self):
        return self.ncglob("OUT_2D")

    @property
    def data3d(self):
        return self.ncglob("OUT_3D")

    @property
    def moments(self):
        return self.ncglob("OUT_MOMENTS")
    @property
    def stats(self):
        return self.ncglob("OUT_STAT")



    def ncglob(self,  path):
        return sorted(glob("{}/{}/*.nc".format(self.rev, path)))


def scales(L, Ht=16e3, N=.01):
    H=Ht/pi; c=.01*H; T=L/c; W=H/T; B=N*N*H;
    return dict(L=L, H=H, N=N, c=c, T=T, W=W,B=B)


def calc_coarsedata(ds, x=32):
    def f(A):
        try:
            return coarsen(A, x=x)
        except ValueError:
            return A

    # this step is needed to align grid with moments data
    dc = ds.apply(f)
    dc['x'] -= dc.x[0]

    # compute dry static energy
    # dc['s'] = dc.TABS + dc.z * 9.81/1004

    return dc

## THERMO
kappa = 287/1004
def calc_thv0(T, qv, p):
    return T * ( 1 + .622 * qv/1000) * (1000/p)**kappa

def calc_T(th, p):
    return th * (p/1000)**kappa

def calc_n2(T):
    z = T.z
    spl = UnivariateSpline(z, T, s=0).derivative(1)

    n2 = gr * (gamma + spl(z))/T
    n2.name = 'N2'
    n2.attrs['units'] = "1/s^2"

    return n2

def calc_der(f, dim):
    bdy = {'x': 'periodic', 'z':'extrap', 'time':'extrap'}
    return f.centderiv(dim, boundary=bdy[dim])

def calc_matderiv(u, w, f):
    """Compute the material derivative using centered differences """

    dx = calc_der(f, 'x')
    dz = calc_der(f, 'z')
    dt = calc_der(f, 'time')/86400.0
    return dt + u * dx + w * dz



def calc_h(dbdt, n2, w, th, p, qrad):
    """
    Parameters
    ----------
    qrad [K / day]


    Returns
    -------
    H [m/s^2/day]
    """


    T = th * (p/1000)**kappa

    H = (dbdt + n2*w  - gr / T * qrad/86400) * 86400
    H = (dbdt + n2*w) * 86400
    H.attrs['units'] = " m/s^2/d"

    return H


def calc_q1(dTdt, w, qrad):
    """ Compute apparent heat source

    Parameters
    ----------
    dTdt:
        Large-scale tempearture derivative K/s
    w:
        vertical velocity in m/s
    qrad:
        radiative heating in K/day

    Returns
    -------
    Q1:
        apparent non-radiative heating in K/day
    """
    coef = Q_(1, s.TTEND.units).to("K/s")
    Q1 = (dTdt + w * gamma - s.TTEND[0]*coef) * 86400.0
    Q1.attrs['units'] = "K/day"


def calc_adz(rho):
    z = rho.z.values
    zh = np.r_[0, (z[1:] + z[:-1])/2, z[-1]*1.5 - .5 * z[-2]]
    return xr.DataArray(np.diff(zh)* rho.values, dims='z', coords={'z': z})


def multiscale_decomp(u, adz, T=20.0, eofslice=slice(100,None)):
    u_sub = u.sel(time=eofslice)

    # compute SVD
    svd = XSVD(['x', 'z'], w=adz, n_components=4)
    svd.fit(u_sub)

    tmin = u.time.min().pipe(float)
    tmax = u.time.max().pipe(float)
    return SVDDecomp(svd, (tmin, tmax, T))(u)

class Decomp(object):
    """Object for multiscale decomposition in x-t
    """


    def __init__(self, spec):


        self.knots = {}
        for dim, (xmin, xmax, L) in spec.items():
            xmin = float(xmin)
            xmax = float(xmax)
            self.knots[dim] = np.linspace(xmin, xmax, round((xmax-xmin)/L))

        self.bc = {'x': 'periodic', 'time': 'extrap'}

    def avg(self, A, dim):
        return Spline(knots=self.knots[dim], dim=dim, bc=self.bc[dim])\
            .fit(A)\
            .predict(A[dim])


    def xavg(self, u):
        return self.avg(u, 'x')

    def tavg(self, u):
        return self.avg(u, 'time')

    def __call__(self, u):
        ubar = self.xavg(u)
        U = self.tavg(ubar)
        ut = (ubar - U)
        uprime = (u-ubar)

        scales = ['p', 't', 'b']
        scales = xr.DataArray(scales, dims='scale', name='scale',
                              coords={'scale':scales})

        return xr.concat((uprime, ut, U), dim=scales)


class SVDDecomp(object):
    """Object for multiscale decomposition in x-t
    """


    def __init__(self, svd, tspec):

        self._svd = svd
        xmin, xmax, L = tspec
        self.tknots = np.linspace(float(xmin),
                                  float(xmax),
                                  round(float((xmax-xmin))/L))

    def tavg(self, A):
        dim = 'time'
        return Spline(knots=self.tknots, dim=dim, bc='extrap')\
            .fit(A)\
            .predict(A[dim])

    def xavg(self, A):
        svd = self._svd
        return svd.inverse_transform(svd.transform(A))

    def __call__(self, u):
        ubar = self.xavg(u)
        U = self.tavg(ubar)
        ut = (ubar - U)
        uprime = (u-ubar)

        scales = ['p', 't', 'b']
        scales = xr.DataArray(scales, dims='scale', name='scale',
                              coords={'scale':scales})

        return xr.concat((uprime, ut, U), dim=scales)


## Multiscale-models

def anelastic_divxz(fx, fz, rho):
    return anelastic_vertdiv(fz, rho) + fx.centderiv(dim='x', boundary='periodic')

def anelastic_vertdiv(fz, rho):
    return (rho*fz).centderiv(dim='z', boundary='extrap')/rho

def calc_dt(th):
    return th.centderiv(dim='time', boundary='extrap')

def calc_dx(th):
    return th.centderiv(dim='x', boundary='periodic')

def synoptic_lhs(th, TH, u, U, w, rho):
    """Operator in synoptic fluctuation equations"""
    return calc_dt(th) + U * th + anelastic_divxz(u*TH, w*TH, rho)


def th_lhs(TH, U, W, rho):
    """Material derivative in the flux form
    """
    return calc_dt(TH) + anelastic_vertdiv(U*TH, W*TH, rho)


def hsbpresgx(th):
    """Hydrostatic x-pressure gradient
    """
    p = th.cumtrapz('z')
    return calc_dx(p)

class IMMD(object):
    """An object for diagnosing the equations seen in the Intraseasonal
    Multi-scale Moist Dynamics (IMMD) model.

    The various *_equations methods return dictionaries containing the LHS-RHS
    of the equations.
    """

    def __init__(self, u, w, B, H, rho, decomp=None):
        self.u = u
        self.w = w
        self.B = B
        self.H = H
        self.rho = rho

        self._decomposer = decomp


    @property
    def _decomp(self):
        """Returns complete multiscale decomposition. Depends on epsilon

        Examples
        --------
        u, w, th, h, ut, wt, tht, ht, U, TH, H = self._decomp
        """
        f = self._decomposer

        u,ut,U = f(self.u)
        w,wt,W = f(self.w)
        th,tht,TH = f(self.B)
        h,ht,H = f(self.H)

        return u, w, th, h, ut, wt, tht, ht, U, TH, H


    @property
    def qrad(self):
        raise NotImplementedError

    @property
    def synoptic_equations(self):
        u, w, th, h, ut, wt, tht, ht, U, TH, H = self._decomp
        rho = self.rho

        return dict(
            u=(synoptic_lhs(u, U, u, U, w, rho) + hsbpresgx(th)),
            th=(synoptic_lhs(th, TH, u, U, w, rho) - h))

    @property
    def planetary_equations(self):
        u, w, th, h, ut, wt, tht, ht, U, TH, H = self._decomp
        rho = self.rho

        return dict(
            u=(calc_dt(u) + anelastic_vertdiv(wt*U, rho)),
            th=(calc_dt(u) + anelastic_vertdiv(wt*TH) + wt - ht))

    @property
    def trade_hadley_equations(self):
        u, w, th, h, ut, wt, tht, ht, U, TH, H = self._decomp
        rho = self.rho

        return dict(
            u=th_lhs(U,U,W,rho) + hsbpresgx(TH),
            th=th_lhs(TH,U,W,rho) + W - H - self.qrad)

    @property
    def lsfluxes(self):
        u, w, th, h, ut, wt, tht, ht, U, TH, H = self._decomp
        rho = self.rho

        return dict(
            u=-(wt * ut + w * u),
            th=-(wt*tht + w *th))
