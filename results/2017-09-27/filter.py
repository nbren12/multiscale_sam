#!/usr/bin/env python
"""Multiscale decomposition using penalized fourier method
"""
import sys
import time
from functools import partial

import numpy as np
import pandas as pd
import xarray as xr


def compute_penal(n, dof, p=4.0):
    """Compute penalization coefficent of low-pass filter

    This function returns the operator resulting from applying the the filter
        f(k) = 1/(1+exp^a k^p)
    in spectral space.

    Parameters
    ----------
    n: int
        number of spatial grid points
    dof: int
        number of degrees of freedom
    p: float, optional
        power to be used in filter (default: p=6)

    Returns
    -------
    f: np.ndarray
         filter to apply in fourier space
    a: float
         penalization coefficent

    """
    import scipy.fftpack as fft
    from scipy.optimize import fsolve

    I = np.eye(n)
    F = fft.fft(I, axis=-1)
    IF = fft.ifft(I, axis=-1)
    k = fft.fftfreq(n, d=1 / n)

    def filt(a, p):
        # use exp(a) for numerical purposes
        return 1 / (1 + np.exp(a) * np.abs(k)**p)

    def f(a):
        N = filt(a, p)
        return np.sum(N) - dof

    a = fsolve(f, 0)
    return filt(a,p), float(a)


def lowpass(A, dim, dof):

    # dimension information
    x = A.coords[dim]
    xnum = A.get_axis_num(dim)
    n = len(x)

    # compute filter with proper shape
    f, _ = compute_penal(n, dof)
    f.shape = [n] + [None] * (A.ndim - xnum - 1)

    # # prep data
    # if A.chunks is None:
    #     da = np

    fA = np.fft.fft(A.data, axis=xnum)
    iA = np.real(np.fft.ifft(fA * f, axis=xnum))

    return xr.DataArray(iA, dims=A.dims, coords=A.coords)


def msdecomp_up(a):
    """multiscale decomposition starting with smallest scale"""
    px_1 = lowpass(a, 'x', 40)
    px_2 = lowpass(px_1, 'x', 10)


    ind = pd.Index(['p', 's', 'm'], name='scale')
    return xr.concat([px_2, px_1-px_2, a-px_1], dim=ind)


def msdecomp_down(a, n=(6, 20), demean=False):
    """multiscale decomposition starting with largest scale"""

    if demean:
        a = a-a.mean('x')

    px_1 = lowpass(a, 'x', n[0])
    a = a-px_1
    px_2 = lowpass(a, 'x', n[1])
    a = a-px_2

    ind = pd.Index(['p', 's', 'm'], name='scale')
    return xr.concat([px_1, px_2, a], dim=ind)


def quadratic(u, w, uw, P=None):
    """ Perform multiscale analysis of uw
    """
    ub = P(u)
    up = u - ub
    wb = P(w)
    wp = w - wb

    # quadratic terms
    uw_b = P(uw)
    r = P(ub*wb)
    m = P(up*wp)

    return xr.Dataset({"tot": uw_b, "large": r, "small": m})


def main(infile, outfile, dim, dof):
    if isinstance(dof, str):
        dof = float(dof)

    d =  xr.open_dataset(infile)

    def f(A):
        if dim in A.dims:
            return lowpass(A, dim, dof)
        else:
            return A


    t1 = time.time()
    d.apply(f).to_netcdf(outfile)
    print("Elapsed: ", time.time()-t1)


try:
    snakemake
except NameError:
    pass
else:
    main(snakemake.input[0], snakemake.output[0], snakemake.params.dim,
         snakemake.params.dof)
