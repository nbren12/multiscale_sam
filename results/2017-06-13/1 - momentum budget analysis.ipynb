{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "from filter import *\n",
    "import gnl.xcalc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\newcommand{\\ub}{\\overline{u}}$\n",
    "\n",
    "The horizontal momentum budget in the anelastic equations is given by\n",
    "\\begin{equation} \n",
    "u_t +  (u u)_x + \\rho_0^{-1}(\\rho_0 u w)_z = D_u - (p/\\rho_0)_x\n",
    "\\end{equation}\n",
    "where the the three dimensional velocity is given by $\\mathbf u = (u,0,w)$. $\\rho_0(z)$ is the base state density given by the anelastic equations. At this level of description, the only momentum source terms are the dissipitation terms $D_u$ due to the sub-grid-scale turbulence closure.\n",
    "\n",
    "To accomplish the multiscale analysis of these equations, we introduce a linear operater $P$, which is meant to approximate the multiscale averaging operations. Because the current analysis is not asymptotic in nature, $P$ will only satisfy the Reynold's averaging properties approximately. However, the operator will only depend on the horizontal direction, so it is interchangeble with time and vertical derivatives.\n",
    "\n",
    "The goal of this section is to analyze the large-scale budget of momentum, which can be accomplished by applying $P$ to the budget above. For ease of notation, we will define $\\overline{u} = Pu$, with the caveat that this \"averaging\" operation is not a Reynold's average in the typical sense. In particular, $P$ does not satisfy the projection property exactly so that $P^2 \\ne P$. However, $P$ should approximately satisfy this property if it is a reasonable approximation to the asymptotic operators, so that $P^2 \\approx P$. This approximate property will be used to approximately separate the vertical advection terms into small-scale and large-scale eddy-flux divergence terms. By decomposing a field $u = P u + (I-P) u$, and applying this approximate idempotentcy property, we see that \n",
    "$$\\overline{u w} \\approx \\overline{\\bar u \\bar w} + \\overline{u' w'}.$$\n",
    "The accuracy of this relation needs to be assessed from the data.\n",
    "\n",
    "To make the discussion simpler, we will represent the large-scale momentum budget symbolically as\n",
    "\\begin{equation}\n",
    "\\ub_t = \\bar V + V'+ G +R_u.\n",
    "\\end{equation}\n",
    "This budgets consists of the resolved vertical transports of horizontal momentum, $\\bar{V} =  -\\rho_0^{-1}(\\rho_0 \\bar u \\bar w)_z$, and the contribution due to interactions between the small-scale momentum and between the large-scale and small-scale advection, $V'$. The latter is given by the formula \n",
    "\n",
    "\\begin{equation}\n",
    "V' = -\\rho_0^{-1}(\\rho_0 \\overline{uw})_z - \\bar{V}.\n",
    "\\end{equation}\n",
    "\n",
    "The remaining two terms in the momentum budget are the large-scale pressure gradient $G = \\overline{- (p/\\rho_0)_x}$, and the combined effect of horizontal momentum transports and dissipation $R_u = \\overline{-(u u)_x + D_u}$. In practice, we compute $R_u$ by taking the numerical residual of the momentum budget, so that $R_u = u_t - \\bar V -  V' - G$.\n",
    "\n",
    "\n",
    "The terms in this momentum budget can be visualized in a few ways.\n",
    "\n",
    "1. Kinetic energy budget, \n",
    "    1. $ \\langle u V \\rangle $ is a two dimensional field which can be seen as a hovmoller diagram.\n",
    "    2. Domain and time mean $u V$\n",
    "2. Single time point, horizontal vertical contour plot.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Planetary scale momentum budget analysis\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "The penalized Fourier method with dof=10 will be used to define the operator $P$ above. These operator will be approximately planetary scale. All spatial derivatives will be computed using second order centered derivatives."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run = get_run('8aa7b')\n",
    "d = xr.open_dataset(run.moments[-1]).sel(z=slice(0,16e3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transport and eddy flux terms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def quadratic(u, w, uw, P=None):\n",
    "    \"\"\" Perform multiscale analysis of uw\n",
    "    \"\"\"\n",
    "    ub = P(u)\n",
    "    up = u - ub\n",
    "    wb = P(w)\n",
    "    wp = w - wb\n",
    "\n",
    "    # quadratic terms\n",
    "    uw_b = P(uw)\n",
    "    r = P(ub*wb)\n",
    "    m = P(up*wp)\n",
    "    \n",
    "    return xr.Dataset({\"tot\": uw_b, \"large\": r, \"small\": m})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the error when applying the approximate operator to quadratic quantities?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "P = lambda x : lowpass(x, 'x', 8)\n",
    "uw_set = quadratic(d.U1, d.W1, d.UW, P=P)\n",
    "ub = P(d.U1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# quadratic terms\n",
    "tot = uw_set.tot\n",
    "r = uw_set.large\n",
    "m = uw_set.small\n",
    "err = tot - r - m"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Err\", float(err.std()), \"resolved\", float(r.std()), \"pert\", float(m.std()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This indicates that there is substantial interaction between the scales, unlike what is predicted by the asymptotic results. Here are vertical slices for the final "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ub[-1].plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tot[-1].plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r[-1].plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lowpass(d.UW[-1], 'x', 8).plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r[-1].plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Up-scale transports of momentum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the regression coefficient of of $r = P(uw)-P(P(u)P(w))$. The regressions estimate is given by $\\beta P(u)_z + \\eta= r$. I have plotted this quantity below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uz = ub.centderiv('z', 'extrap')\n",
    "\n",
    "p = tot - r\n",
    "\n",
    "beta = (uz * (m)).sum(['x', 'time'])/(uz**2).sum(['x','time'])\n",
    "beta = beta.fillna(0.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "beta.sel(z=slice(0,12e3)).plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the kinetic energy impact of the momentum transport:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "beta_u = (ub * (m).sum(['x', 'time'])/(ub**2).sum(['x','time'])\n",
    "beta_u.sel(z=slice(0,12e3)).plot()\n",
    "axhline(0.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Overall the vertical momentum transports damp in the lower atmosphere and amplify in the upper."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This indicates that above an elevation of $z=2000$ m most of the non-resolved vertical transports of horizontal momentum are up-scale! I am not sure if these are large values of momentum diffusivity or not. I should also probably add confidence intervals to this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following is the standard error estimate for the regression coefficient. What is typically used in the atmosphere ocean community?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "resid = beta * uz - p\n",
    "\n",
    "dims = ['x', 'time']\n",
    "\n",
    "sig = (resid**2).mean(dims)\n",
    "se = np.sqrt(sig**2/(uz*uz).mean(dims))\n",
    "\n",
    "se.sel(z=slice(0,12e3)).plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def xhexbin(x, y, **kwargs):\n",
    "    return plt.hexbin(x.values.ravel(), y.values.ravel(), **kwargs)\n",
    "\n",
    "\n",
    "dat =  xr.Dataset({'uz': uz, 'r': m+err})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Down gradient in the lower atmosphere, however the constant negative sign of the transport indicates that the momentum transports could resemble Rayleigh-damping in this layer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = dat.sel(z=1000, method='nearest')\n",
    "xhexbin(df.uz, df.r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a fairly clear up-gradient characteristics in the mid troposphere. The left-hand and right hand clusers are the patterns in the ascent and descent regions, respectively."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "df = dat.sel(z=5000, method='nearest')\n",
    "xhexbin(df.uz, df.r)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
