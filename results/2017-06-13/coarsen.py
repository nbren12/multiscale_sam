"""
coarsen.py infile outfile
"""
import sys
import xarray as xr

from gnl.xarray import coarsen

def calc_coarsedata(ds, x=32):
    def f(A):
        try:
            return coarsen(A, x=x)
        except ValueError:
            return A

    # this step is needed to align grid with moments data
    dc = ds.apply(f)
    dc['x'] -= dc.x[0]

    # compute dry static energy
    # dc['s'] = dc.TABS + dc.z * 9.81/1004

    return dc

def main(infile, outfile):
    print(f"Coarsening {infile}")
    xr.open_dataset(infile)\
        .pipe(calc_coarsedata)\
        .to_netcdf(outfile)

if __name__ == '__main__':
    main(*sys.argv[1:])
