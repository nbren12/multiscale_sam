from glob import glob, os
import numpy as np
from scipy.interpolate import UnivariateSpline
import xarray as xr
import gnl.xarray

class SamRun(object):
    def __init__(self, rev, root="."):
        g = glob("{}/*-{}*".format(root, rev))

        if len(g) == 0:
            raise ValueError("Specified ID not found")
        elif len(g) > 1:
            raise ValueError("Specified ID is not unique")
        else:
            self.rev = g[0]

    @property
    def data2d(self):
        return self.ncglob("OUT_2D")

    @property
    def moments(self):
        return self.ncglob("OUT_MOMENTS")
    @property
    def stats(self):
        return self.ncglob("OUT_STAT")



    def ncglob(self,  path):
        return sorted(glob("{}/{}/*.nc".format(self.rev, path)))

def coarsen(ncfiles, field, **kwargs):
    tb = xr.concat([xr.open_dataset(r)[field]\
                    .coarsen(**kwargs)
                    for r in ncfiles], dim='time')

    return tb

def extractfield(ncfiles, field, **kwargs):
    tb = xr.concat([xr.open_dataset(r)[field]
                    for r in ncfiles], dim='time')

    return tb


## THERMO
kappa = 287/1004
def calc_thv0(T, qv, p):
    kappa = 287/1004
    return T * ( 1 + .622 * qv/1000) * (1000/p)**kappa


def calc_n2(thv0):
    z = thv0.z
    spl = UnivariateSpline(z, thv0).derivative(1)

    n2 = 9.81 * spl(z)/ thv0
    n2.name = 'N2'

    return n2

def calc_der(f, dim):

    x = f[dim].values
    x= np.r_[  2*x[0] - x[1], x, 2*x[-1] - x[-2]]
    spat = xr.DataArray(x[2:] - x[:-2], dims=[dim], coords={dim: f[dim]})

    grad = np.gradient(f, axis=f.get_axis_num(dim))
    grad = xr.DataArray(grad, dims=f.dims, coords=f.coords)

    return grad/spat

def calc_matderiv(f, **kwargs):
    """Compute the material derivative using centered differences

    Parameters
    ----------
    f:
        4D data quantity to take derivative

    **kwargs:
        advecting velocities along dimension

    Returns
    -------
    dfdt:
        total derivative of f


    Example
    -------
    >>> calc_matderiv2d(TABS, x=u, z=w)
    """

    # "velocity" of time is 1/day to account for unit conversions
    kwargs['time'] = 1/86400

    mat = 0 * f

    # compute differences (doesn't handle periodic BCs correctly but who cares)
    grad = np.gradient(f)
    grad = {dim: xr.DataArray(g, dims=f.dims, coords=f.coords) for dim, g in zip(f.dims, grad)}

    # weight velocities appropriately
    spats = {}
    for k in f.dims:
        x = f[k].values
        x= np.r_[  2*x[0] - x[1], x, 2*x[-1] - x[-2]]
        spats[k] = xr.DataArray(x[2:] - x[:-2], dims=[k], coords={k: f[k]})

        grad = np.gradient(f, axis=f.get_axis_num(k))
        grad = xr.DataArray(grad, dims=f.dims, coords=f.coords)

        # compute material derivative
        mat += grad/spats[k] * kwargs[k]

    mat.attrs['units'] = f.units + '/s'

    return mat

def calc_h(dbdt, n2, w, th, p, qrad):
    """
    Parameters
    ----------
    qrad [K / day]


    Returns
    -------
    H [m/s^2/day]
    """
    cp = 1004
    gr = 9.81


    T = th * (p/1000)**kappa

    H = (dbdt + n2*w  - gr / T * qrad/86400) * 86400
    H.attrs['units'] = " m/s^2/d"

    return H

