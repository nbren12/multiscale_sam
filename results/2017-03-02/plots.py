import numpy as np
import matplotlib.pyplot as plt
from samthermo import compute_dT, get_sample_state

plt.rcParams["axes.formatter.limits" ] =  -3, 7 
plt.rcParams["text.usetex"] = True



def plot_varytemp_partial_derivs():
    """compute and plot partial derivatives of T for a temperature range"""

    # compute partial derivs for a range of temperatures
    t = np.r_[200:350:100j]

    def f(t):
        st = get_sample_state(T=t)
        return compute_dT(st)


    pds = np.vstack(f(tt) for tt in t)

    args = ['hl', 'qt', 'qp', 'z', 'p', 'T']
    dfield = dict(zip(args, np.arange(len(args))))

    fig, axs = plt.subplots(2,2, figsize=(6,6/1.61), sharex=True)
    plot_fields = [(f, pds[:,dfield[f]]) for f in args[:4]]


    for ax, (f, p) in  zip(axs.flat, plot_fields):
        lab = r"$\frac{\partial T}{\partial " + f +"}$"
        ax.plot(t,p, label=lab)
        ax.legend()

    plt.tight_layout()

    for ax in axs[-1,:].flat:
        ax.set_xlabel('T [K]')

if __name__ == '__main__':
    plot_varytemp_partial_derivs()
    # plt.savefig("pdplot.pgf")
    plt.show()
    # tikz_save("pdplot.tikz")
