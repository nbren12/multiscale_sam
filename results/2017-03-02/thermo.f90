program thermo
  use thermo_mod

  real eps
  real t
  complex t0
  complex t1,t2


  real p, qt, qp, gamaz
  real facq,facp, hl, dhl, dp

  call dhldt(273., .04, .001, 1000., 1.2, 0.0, hl, dhl, facq, facp, dp)
  print *, 'hl', hl, 'd hl / dt', dhl, 'd hl / dqt ', facq, 'd hl / d qp', facp, 'd hl /dp', dp

end program thermo
