module thermo_mod
  implicit none

  ! copy pasted from params can be replaced with use statement
  ! but this adds annoying dependencies
  public :: calc_ttv
  private

  real, parameter :: cp = 1004.             ! Specific heat of air, J/kg/K
  real, parameter :: ggr = 9.81             ! Gravity acceleration, m/s2
  real, parameter :: lcond = 2.5104e+06     ! Latent heat of condensation, J/kg
  real, parameter :: lfus = 0.3336e+06      ! Latent heat of fusion, J/kg
  real, parameter :: lsub = 2.8440e+06      ! Latent heat of sublimation, J/kg
  real, parameter :: rv = 461.              ! Gas constant for water vapor, J/kg/K
  real, parameter :: rgas = 287.            ! Gas constant for dry air, J/kg/K
  real, parameter :: epsv = 0.622           ! ratio of rgas and rvapor

  real, parameter :: fac_cond = lcond/cp 
  real, parameter :: fac_fus = lfus/cp
  real, parameter :: fac_sub = lsub/cp

  real, parameter :: tbgmin = 253.16    ! Minimum temperature for cloud water., K
  real, parameter :: tbgmax = 273.16    ! Maximum temperature for cloud ice, K
  real, parameter :: tprmin = 268.16    ! Minimum temperature for rain, K
  real, parameter :: tprmax = 283.16    ! Maximum temperature for snow+graupel, K
  real, parameter :: tgrmin = 223.16    ! Minimum temperature for snow, K
  real, parameter :: tgrmax = 283.16    ! Maximum temperature for graupel, K


contains


  complex function esatw(t)
    implicit none
    complex t	! temperature (K)
    complex a0,a1,a2,a3,a4,a5,a6,a7,a8
    data a0,a1,a2,a3,a4,a5,a6,a7,a8 /&
                                !	6.105851, 0.4440316, 0.1430341e-1, &
                                !        0.2641412e-3, 0.2995057e-5, 0.2031998e-7, &
                                !        0.6936113e-10, 0.2564861e-13,-0.3704404e-15/
         6.11239921, 0.443987641, 0.142986287e-1, &
         0.264847430e-3, 0.302950461e-5, 0.206739458e-7, &
         0.640689451e-10, -0.952447341e-13,-0.976195544e-15/
    complex dt
    dt = cmplx(max(-80.,real(t)-273.16), aimag(t))
    esatw = a0 + dt*(a1+dt*(a2+dt*(a3+dt*(a4+dt*(a5+dt*(a6+dt*(a7+a8*dt))))))) 
  end function esatw

  complex function esati(t)
    implicit none
    complex t	! temperature (K)
    complex a0,a1,a2,a3,a4,a5,a6,a7,a8 
    data a0,a1,a2,a3,a4,a5,a6,a7,a8 /&
         6.11147274, 0.503160820, 0.188439774e-1, &
         0.420895665e-3, 0.615021634e-5,0.602588177e-7, &
         0.385852041e-9, 0.146898966e-11, 0.252751365e-14/	
    complex dt
    if(real(t).gt.273.15) then
       esati = esatw(t)
    else if(real(t).gt.185.) then
       dt = t-273.16
       esati = a0 + dt*(a1+dt*(a2+dt*(a3+dt*(a4+dt*(a5+dt*(a6+dt*(a7+a8*dt))))))) 
    else   ! use some additional interpolation below 184K
       dt = cmplx(max(-100.,real(t)-273.16), aimag(t))
       esati = 0.00763685 + dt*(0.000151069+dt*7.48215e-07)
    end if
  end function esati

  function cmin(a, x)
    complex a, x, cmin
    if (real(a) > real(x)) then
       cmin = x
    else
       cmin = a
    end if
  end function cmin

  function crect(x) result(y)
    complex x, y
    if (real(x) < 0.) then
       y = (0.,0.)
    else if(real(x) > 1) then
       y = (1.0, 0.0)
    else
       y = x
    end if
  end function crect

  subroutine calcom(tabs, p, qt, qp, omn, omp, omv)
    ! compute liquid water / ice energy
    ! complex valued for derivative trip
    ! this should be tested against the version inside of SAM

    complex p
    real qt, qp
    real a_pr, a_bg
    complex tabs, omn, omp, omv, qcl, qci, qpl, qpi, qn
    complex qsatt, ec, ei, qsatw, qsati

    intent(in) tabs, p, qt, qp
    intent(out) omn, omp, omv

    a_pr = 1./(tprmax-tprmin)
    a_bg = 1./(tbgmax-tbgmin)

    omp = crect((tabs-tprmin)*a_pr)
    omn = crect((tabs-tbgmin)*a_bg)



    ! compute vapor
    ec = esatw(tabs)
    ei = esati(tabs)



    qsatt = 0.622*(omn*ec/(p-ec)  + (1.-omn)*ei/(p-ei))
    omv =  cmin(cmplx(1., 0.0), qsatt/(qt + 1e-15))



  end subroutine calcom


  !> Compute h_l, t_V and partial derivatives wrt T, qp, qt, p
  !!
  !! Notes
  !! -----
  !! This uses the complex number trick d f(x + iy)/dy = i f'(x) at y=0
  !! Therefore, f'(x) = Imag(f'(x + i eps))/eps where eps is a very small
  !! number. This methods is much more numerical stable than taking a difference
  !! quotient for small eps, which leads to extreme numerical truncation errors.
  subroutine hltv(tabs, p, gamaz, qt, qp, eps, &
       hl, dhl, dhldq, dhldqp,&
       tv, dtv, dtvdq, dtvdqp)

    complex, intent(in) :: tabs, p
    real, intent(in) :: gamaz, qt, qp, eps
    real, intent(out) :: hl, dhl, dhldq, dhldqp
    real, intent(out) :: tv, dtv, dtvdq, dtvdqp

    ! work arrays
    complex omn, omp, omv, chl, ctv
    complex facq, facp

    call calcom(tabs, p, qt, qp, omn, omp, omv)

    !! h_L
    facq = -(1-omv)*((1.-omn)*fac_sub + omn*fac_cond)
    facp = -(omp*fac_cond + (1.-omp)*fac_sub)
    chl = tabs + gamaz + facq * qt + facp * qp

    hl = real(chl)
    dhl = aimag(chl)/eps
    dhldq = real(facq)
    dhldqp = real(facp)

    !! virtual temperature
    ! Tv = T (1 + eps q_v - q_i - q_r - q_p)
    facq  = tabs * (epsv * omv - (1.-omv))
    facp  = - tabs

    ctv = tabs + facq * qt + facp * qp

    tv = real(ctv)
    dtv = aimag(ctv)/eps
    dtvdq = real(facq)
    dtvdqp = real(facp)

  end subroutine hltv


  !> Compute T, T_v and partial derivatives wrt hl, qp, qt, p
  subroutine calc_ttv(tabs, qt, qp, p, rho, gamaz,&
       dtdhl, dtdq, dtdqp, dtdp,& ! hl and partial derivatives
       Tv, dTvdhl, dtvdq, dtvdqp, dtvdp) !  B and partial   derivatives

    real, intent(in) :: tabs, p, qt, qp, rho, gamaz
    real, intent(out) :: dtdhl, dtdq, dtdqp, dtdp
    real, intent(out) :: Tv, dTvdhl, dtvdq, dtvdqp, dtvdp

    ! work arrays
    real hl

    real dhldt, dhldq, dhldqp, dhldp
    real dtvdt
    real eps
    eps = 1e-30

   ! small number

    ! caclulate T derivatives
    call hltv(cmplx(tabs, eps), cmplx(p, 0), gamaz, qt, qp, eps,&
         hl, dhldt, dhldq, dhldqp,&
         tv, dtvdt, dtvdq, dtvdqp)

    ! caclulate p derivatives
    call hltv(cmplx(tabs, 0), cmplx(p, eps), gamaz, qt, qp, eps,&
         hl, dhldp, dhldq, dhldqp,&
         tv, dtvdp, dtvdq, dtvdqp)


    !! Change of variables
    dtdhl = 1./dhldt
    dtdq = - dhldq/dhldt
    dtdqp = - dhldqp/dhldt
    dtdp = - dhldp/dhldt

    dtvdhl = dtvdt *dtdhl
    dtvdq = dtvdq + dtvdt*dtdq
    dtvdq = dtvdp + dtvdt*dtdqp
    dtvdp = dtvdp + dtvdt*dtdp


  end subroutine calc_ttv

end module thermo_mod
