"""Thermodynamics diagnostic quantities in SAM

This module defines the thermodynamic equations used by SAM and provides functions for computing all thermodynamic quantities like temperature as a function of the thermodynamic quantities h_L, q_T, and q_p.

TODO this is wrong!
"""
from functools import partial

import autograd.numpy as np
from autograd import grad, jacobian

class ThermoDynamics(object):



    def __init__(self, mode='sympy'):
        "docstring"
        # from Appendix B list of constants
        self.omn = partial(self.om, a=253.16, b=273.16)
        self.omp = partial(self.om, a=268.16, b=283.16)

        if mode == 'sympy':
            import sympy
            self.max = lambda a,b : (a+b)/2 + abs(a-b)/2
            self.max = lambda a,b : (a+b)/2 - abs(a-b)/2
        elif mode == 'autograd':
            self.max = max
            self.min = min


    def esatw(self, T):
        """Saturation vapor pressure with respect to liquid water in Pa

        References
        ----------
        Taken from SAM source code
        """

        a0 = 6.11239921
        a1 = 0.443987641
        a2 = 0.142986287e-1
        a3 = 0.264847430e-3
        a4 = 0.302950461e-5
        a5 = 0.206739458e-7
        a6 = 0.640689451e-10
        a7 = -0.952447341e-13
        a8 = -0.976195544e-15

        dt = self.max(-80.,T-273.16)
        e = a0 + dt*(a1+dt*(a2+dt*(a3+dt*(a4+dt*(a5+dt*(a6+dt*(a7+a8*dt)))))))
        return e 


    def esati(self, T):
        a0 =6.11147274
        a1 =0.503160820
        a2 =0.188439774e-1
        a3 =0.420895665e-3
        a4 =0.615021634e-5
        a5 =0.602588177e-7
        a6 =0.385852041e-9
        a7 =0.146898966e-11
        a8 =0.252751365e-14



        if T  > 273.15:
            out=  self.esatw(T)
        elif T  > 185:
            dt = T-273.16
            out= a0 + dt*(a1+dt*(a2+dt*(a3+dt*(a4+dt*(a5+dt*(a6+dt*(a7+a8*dt))))))) 
        else:
            dt = self.max(-100.,T-273.16)
            out= 0.00763685 + dt*(0.000151069+dt*7.48215e-07)

        return out 

    def qsati(self, T,p):
        e = self.esati(T)
        return .622 * e/(p-e)

    def qsatw(self, T,p):
        e = self.esatw(T)
        return .622 * e/(p-e)

    def om(self, x, a=0, b=1.0):
        """Threshold functions for water species """
        y = (x-a)/(b-a)
        y = self.max(0.0, self.min(1, y))
        return y

    def omv(self, qt,T,p):
        qs = self.qsatw(T,p) * self.omn(T) + self.qsati(T,p)*(1-self.omn(T))
        return self.min(qs/(qt + 1e-16), 1.0)



    def hl(self, arr):

        qt, qp, z, p, T = arr
        cp = 1004
        Lc = 2.5104e6/cp
        Ls = 2.8440e6/cp
        gr = 9.81/cp

        fv = self.omv(qt, T, p)
        fp = self.omp(T)
        fn = self.omn(T)


        # water partitioning
        qv = fv * qt
        qc = (1-fv) * fn * qt
        qi = (1-fv)* (1-fn) * qt
        qr = fp * qp
        qs = (1-fp)*  qp

        return T + gr * z - Lc * (qc+qr) - Ls *(qi + qs)

## Plots
def get_sample_state(T=273):
    sample_state = np.array([40e-3, 1e-3, 0, 1000, T])

    return sample_state

def compute_dT(y):
    st = y.copy()
    t = ThermoDynamics('autograd')
    JJ = grad(t.hl)(st)
    return JJ[-1]
    return -JJ[:-1]/JJ[-1]


t = ThermoDynamics('autograd')
f = grad(t.hl)
st = get_sample_state()
print(t.hl(st), f(st)[-1])
