import os
import re
import xarray as xr
import attr
import data

configfile: "config.yaml"
run_ids = config.get('run_ids', [])

include: "rules/plots.smk"


def get_raw_stat(wildcards):
    run = wildcards.run
    direc = f"data/raw/{run}/OUT_STAT"
    ncfile = [f for f in os.listdir(direc)][0]
    return os.path.join(direc, ncfile)


rule paper:
    input:
           expand("data/output/{field}.{run}.nc", run=config['run_ids'],\
                  field=['ke_budget', 'pe_budget', 'q2_budget', 'TB']),
           "data/output/triad.2018-05-12-A.csv"
    output: "doc/paper/plots.html"
    shell:
        "jupyter nbconvert --ExecutePreprocessor.timeout=600 \
        --execute doc/paper/plots.ipynb"


rule plot_tb:
    input: "data/processed/{run}/hi/2d/TB.nc"
    output: report("reports/plots/tb/{run}.png", category="TB")
    script: "src/plots/plot_tb.py"

rule plot_stat:
    input: get_raw_stat
    output: report("reports/plots/stat/{run}.png", category="STAT")
    run:
        import matplotlib.pyplot as plt
        stat = xr.open_dataset(input[0])
        fig, [axQ, axT, ax] = plt.subplots(3, 1, figsize=(7,4), constrained_layout=True)
        stat.QV.plot.contourf(x='time', y='z', cmap='inferno', ax=axQ)
        stat.TABS.plot.contourf(x='time', y='z', cmap='inferno', ax=axT)
        fig.savefig(output[0])

rule clean:
    shell: "rm -rf data/processed data/output"


ruleorder: select_interface > split_var > input_2d

rule coarsen:
    input: "data/raw/{run}/data/{f}.nc", "data/raw/{run}/patched.done"
    output: "data/processed/{run}/coarse/{f}.nc"
    params: blocks={'x': 64}
    script: "scripts/coarsen.py"

rule select_interface:
    input: data.files(dim=3)
    output: "data/processed/{run}/interface.nc"
    shell: "ncrcat -D1 -d x,,,64 {input} {output}"

rule split_var:
    input: data.files_budget(dim=3)
    output: "data/processed/{run}/{f}.nc"
    shell: "ncrcat -D1 -v {wildcards.f} {input} {output}"


rule tb:
    input: data.files(dim=2)
    output: "data/output/TB.{run}.nc"
    shell: "ncrcat -D3 -v TB {input} {output}"


rule input_2d:
    output: "data/processed/{run}/hi/2d/{f}.nc"
    run:
        direc = f"data/raw/{wildcards.run}/OUT_2D"
        ncfile = [f for f in os.listdir(direc)
                  if not re.search('MSE', f)][0]
        shell("ncks -v {wildcards.f} {direc}/{ncfile} {output}")

rule patch_z_data:
    output: touch("data/raw/{id}/patched.done")
    script: "scripts/patch_z_data.py"

ke_fields = ['U', 'W', 'RHOWU', 'RHO', 'UPGRAD', 'UEDDY', 'UMISC']
rule ke_budget:
    input: expand("data/processed/{{run}}/{f}.nc", f=ke_fields)
    output: "data/output/ke_budget.{run}.nc"
    script: "scripts/momentum.py"

pe_fields = ["SLI", "SLIEDDY", "SLIMISC", "SLISED", "SLIFLUX", "U", "RHO", "W",
             "TVPRIME", "TABS", "BUOYFLUX"]
rule pe_budget:
    input:  data.files_budget(dim=3)
    output: "data/output/pe_budget.{run}.nc"
    script: "scripts/buoyancy.py"

q2_fields = ['U', 'W', 'RHO', 'QTOT', 'QTOTEDDY', 'QTOTFLUX', 'QTOTMISC',
             'QTOTLSF', 'QTOTSED']
rule moisture_var_budget:
    input: expand("data/processed/{{run}}/{f}.nc", f=q2_fields)
    output: "data/output/q2_budget.{run}.nc"
    script: "scripts/moisture_variance_budget.py"

rule stat:
    input: "data/raw/{run}/data/stat.nc"
    output: "data/processed/{run}/stat.nc"
    shell: "cp {input} {output}"


rule vertical_advection_triads:
    input: w="data/processed/{run}/W.nc", u="data/processed/{run}/U.nc", stat=data.files_stat()
    output: "data/output/triad.{run}.csv"
    script: "scripts/vertical_advection_triads.py"
