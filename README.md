Analyzing multiscale structures in 2D cloud resolving models
============================================================

Setting up the computational environment
----------------------------------------

This project uses three main tools: python, anaconda, and snakemake. The
analysis codes are written in python, anaconda manages external dependencies,
and snakemake automates the computational workflows.

To get started with this project

1. Download the code from bitbucket.

        git clone https://bitbucket.org/nbren12/multiscale_sam

2. Setup the computational environment
   using [miniconda](https://conda.io/miniconda.html)---which you must install
   if you have not already.
   
        cd [project root]
        conda env create environment.yml
    
3. Activate the newly created anaconda environment. The specific command for
   doing this depends on the platform. Try either

        source activate multiscale_sam  # linux
        conda activate multiscale_sam   # mac 

4. Tell python where to find the source files for this project

        cd [project root]
        conda develop .

    This step requires `conda-build`, which can be installed by running

        conda install -n root conda-build

When you come to work on the project, make sure that you have loaded the correct
anaconda environment by executing step 3 again.


Downloading Data
----------------

The data for this project is currently stored on the prince cluster at NYU. The
path to the files is `/archive/ndb245/Data/id`, and the main dataset we analyze
is located at `/archive/ndb245/Data/id/1`. This cluster is connected
to [globus](http://globus.org), which makes it straightforward to obtain the
data. these are the steps

1. Download the globus command line interface
    
        pip install globus-cli

2. Login to the service with the command line utility. If you do not have a
   globus account already, you will need to create one
    
        globus login

3. Download the data by typing

        make data
    
   in the root of this project.
    
This command should downoad all the necessary raw data to `data/raw`.

Running the analysis
--------------------

This project uses [snakemake](link needed) to manage the data processing. To
generate the data needed to generated the plots and run the analyses in each
notebook, simply run

    snakemake analysis

This command should process all of the data `data/raw` and save the necessary
outputs to `data/processed`. This kinds of computations done include computing
buoyancy, and smoothing the data using the Fourier filters discussed in the
paper.
    
Make the plots for the paper
----------------------------

All the figure plots are in a jupyter notebook located at
`doc/paper/plots.ipynb`. Running, `snakemake analysis` should generate all the
necessary daa files to make this plot.

TODO
----


Reports
-------

 - [results/1.1-ndb-plotting-run-with-strat-heating.html]
