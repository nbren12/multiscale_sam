import attr
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.gridspec import GridSpec

import xarray as xr
from xnoah.xarray import coarsen


def snapshot_data(time=60, id="1"):
    # open all the 3d data
    data = xr.open_mfdataset(f"../../data/raw/{id}/data/3d/*.nc",
                             chunks={'time': 1})\
        .sel(time=time, method='nearest')\
        .apply(lambda x: coarsen(x, x=32))\
        .compute()

    tb = xr.open_dataset(f"../../data/raw/{id}/data/2d/TB.nc",
                         chunks={'time': 1})\
           .sel(time=time, method='nearest')\
           .apply(lambda x: coarsen(x, x=32))

    # compute bouyancy
    tv = data.TABS
    tv0 = tv.mean(['x'])
    B = 9.81 * (tv - tv0) / tv0
    B.attrs['units'] = 'm/s^2'
    B = B.to_dataset(name='B')

    # open pressure
    p = xr.open_dataset(f"../../data/raw/{id}/data/stat.nc").p.to_dataset(
        name='p')

    return xr.auto_combine([tb, p, data, B])


@attr.s
class SnapshotPlot(object):
    data = attr.ib()
    tb_height_ratio = attr.ib(default=.2)
    plot_kwargs = {
        'U': dict(cmap='bwr', levels=np.linspace(-50, 50, 11)),
        'B': dict(levels=np.linspace(-.3, .3, 13), cmap='bwr', extend='both'),
        'QN': dict(cmap='bone_r')
    }

    def plot_2d(self, field, ax, cax, label=None, ylabel=False, sharex=None):
        # get plotting arguments for the current field
        data = self.data
        kwargs = self.plot_kwargs.get(field, {})

        ax = plt.subplot(ax, sharex=sharex)
        cax = plt.subplot(cax)

        im = ax.contourf(data.x, data.p, data[field], **kwargs)
        plt.colorbar(im, cax=cax)

        # add label for vertical axis
        # ax.set_ylabel('p [mb]');

        units = data[field].units.strip()

        ax.annotate(
            f"{field} [{units}]", (2, 250),
            size=16,
            bbox=dict(color='w', alpha=.8))

        ax.invert_yaxis()

        ax.set_xlim([data.x.min(), data.x.max()])

        if label is not None:
            ax.set_xlabel(label)
        else:
            ax.xaxis.set_visible(False)

        ax.yaxis.set_major_locator(plt.MaxNLocator(4))
        if ylabel:
            ax.set_ylabel('p [mb]')

    def plot_column(self, gs, fields_2d, **kwargs):
        data = self.data

        tb_ax = plt.subplot(gs[0, 0])
        tb_ax.plot(data.TB.x, data.TB, marker="", color="k")
        tb_ax.xaxis.set_visible(False)
        tb_ax.set_ylabel('TB [K]')

        self.plot_2d(fields_2d[0], gs[1, 0], gs[1, 1], sharex=tb_ax, **kwargs)
        self.plot_2d(
            fields_2d[1],
            gs[2, 0],
            gs[2, 1],
            sharex=tb_ax,
            label='x [1000 km]',
            **kwargs)

    def plot(self):

        fig = plt.figure(figsize=(11, 6))
        gs = GridSpec(
            3,
            2,
            height_ratios=[self.tb_height_ratio, 1, 1],
            width_ratios=[1, .05],
            wspace=.01,
            right=.44)
        self.plot_column(gs, ['U', 'B'], ylabel=True)

        gs = GridSpec(
            3,
            2,
            height_ratios=[self.tb_height_ratio, 1, 1],
            width_ratios=[1, .05],
            wspace=.01,
            left=.56)
        self.plot_column(gs, ['QV', 'QN'], ylabel=False)

        return fig

    @classmethod
    def from_time_id(cls, time=60, id="1", **kwargs):

        data = snapshot_data(time=time, id=id)
        data = data.assign(x=data.x / 1e6)
        return cls(data, **kwargs)
