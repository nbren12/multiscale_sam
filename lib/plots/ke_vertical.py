import matplotlib.pyplot as plt
import xarray as xr


def load_data(filename):
    ke = xr.open_dataset(filename)

    tot = ke.TOT
    delta_t = float(ke.time[-1] - ke.time[0])
    dt = float(ke.time[1] - ke.time[0])
    ke_sum = ke.sum('time') * dt * 86400

    SRC = (tot.isel(time=-1)-tot.isel(time=0))\
         - sum(ke_sum[f] for f in ['PGRAD', 'VERT', 'HORZ'])

    # take average over time interval
    ke_sum = ke_sum / delta_t
    SRC = SRC / delta_t

    return xr.Dataset(
        dict(
            SRC=SRC,
            HORZ=ke_sum.HORZ,
            VERT=ke_sum.VERT,
            PGRAD=ke_sum.PGRAD,
            p=ke.p))


def plot_pane(ds, ax, title='', 
              fields=['VERT', 'HORZ',  'PGRAD', 'SRC'],
              **kwargs):
    for field in fields:
        l, = ax.plot(ds[field], ds.p, label=field, **kwargs)
        ax.set_ylim([ds.p.max(), ds.p.min()])
    ax.set_title(title)
    ax.set_ylabel("p (hPa)")
    return fields


def nc_plot(ax, scale='6', filename="../../data/processed/1/output/ke_budget.nc",
         **kwargs):
    ds = load_data(filename)
    return plot_pane(ds.sel(scale=scale), ax, **kwargs)
