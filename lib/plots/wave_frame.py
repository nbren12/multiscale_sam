from pathlib import Path

import matplotlib.pyplot as plt

import gnl
import numpy as np
import xarray as xr
from lib.material import anelastic_vertdiv
from xnoah.sam import coarsen


def phaseshift(tb, **kwargs):
    return xr.apply_ufunc(
        gnl.phaseshift,
        tb.x,
        tb.time * 86400,
        tb,
        input_core_dims=[['x'], [], ['x']],
        output_core_dims=[['x']],
        kwargs=kwargs,
        dask='allowed')


def wave_frame(u):
    return phaseshift(u.sel(time=slice(75, None)), c=6.0)


def avg(u):
    ub = u.mean('time')
    return ub - ub.mean('x')


def load_data(path=""):
    qstrat_path = Path(path)
    ds = xr.open_mfdataset(str(qstrat_path / "coarse/3d/*.nc"))
    stat = xr.open_mfdataset(str(qstrat_path / "stat.nc"))
    rho = stat.RHO[-1]

    u, w = ds.U, ds.W

    ub = avg(wave_frame(u))
    wb = avg(wave_frame(w))

    uw_p = avg(wave_frame(u * w) - ub * wb)
    efconv = -anelastic_vertdiv(uw_p, rho)

    return xr.Dataset({
        'p': stat.p,
        'u': ub,
        'w': wb,
        'uwp': uw_p,
        'efconv': efconv})


def plot(path):

    # load data and computations
    ds = load_data(path)
    uw_p = ds.uwp
    ub = ds.u
    efconv = ds.efconv * 86400
    cor = (ub * efconv).mean('x') / (ub * ub / 2).mean('x')


    # plot it
    fig, ax = plt.subplots(figsize=(6, 3), dpi=100)
    im = ax.contourf(
        uw_p.x / 1e6, ds.p, uw_p, levels=np.arange(-5, 6) * .04, cmap='RdBu_r')
    plt.colorbar(im, pad=.01, ax=ax)
    ax.contour(ub.x / 1e6, ds.p, ub, colors='black', levels=np.arange(-5, 6) * 5)
    ax.set_ylim([1015, 10])

    plt.figure(figsize=(2, 4))
    plt.plot(cor, ds.p)
    plt.ylim([1014, 100])
    plt.xlim([-1, 1.1])
    plt.xlabel('Growth rate 1/d')
    plt.xticks(
        [-1, -1 / 2, -1 / 5, 1 / 5, 1 / 2, 1], rotation=45)
