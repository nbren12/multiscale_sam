"""Plotting routines"""
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import xarray as xr
from matplotlib.colors import LogNorm

from xnoah.xarray import coarsen

from ..filter import lowpass
from ..budget import decomp
from ..spectrum import compute_space_time_spec

from .snapshot import SnapshotPlot
from . import ke_vertical, climatology, eddy_flux, wave_frame

__all__ = ["SnapshotPlot", "ke_vertical", "climatology", "eddy_flux",
           "wave_frame"]


def plot_line_filters(u, ax=None, dofs=(6, 26), **kwargs):

    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(11, 4.5))

    # u plot
    ax.plot(u.x, u.data, label='U', **kwargs)

    for dof in dofs:
        ub = lowpass(u, 'x', dof)
        ax.plot(u.x, ub, label=dof, **kwargs)

    # X axis labelling
    ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x / 1e6))
    ax.xaxis.set_major_formatter(ticks_x)

    ax.set_xlabel('x [1000 km]')

    # y axis labeling
    ax.set_ylabel('u [m/s]')

    # titles
    # ax.set_title('Zonal velocity at z=3 km')

    # fix xlimits
    x = u.x
    ax.set_xlim([float(x.min()), float(x.max())])

    return ax


def plot_2d_msdecomp(a,
                     planetary_dof=6,
                     synoptic_dof=26,
                     figsize=(6, 6),
                     title_color='k',
                     post=lambda x: x,
                     **kwargs):
    """This function plots the 2d multiscale decomposition

    See Also
    --------
    filter.msdecomp_down
    """

    plot_kwargs = dict(vmin=-20, vmax=20, cmap='bwr')
    plot_kwargs.update(kwargs)

    # subtract the mean
    data = dict(decomp(a, dofs=(planetary_dof, synoptic_dof)))

    fig, axs = plt.subplots(
        1, 3, figsize=figsize, sharey=True, sharex=True, dpi=90)

    scales = [planetary_dof, synoptic_dof, 'resid']
    scale_names = {
        planetary_dof: 'Planetary',
        synoptic_dof: 'Synoptic',
        'resid': 'Mesoscale'
    }

    for ax, (scale), letter in zip(axs.flat, scales, "abcd"):

        # scale is actually an xarray object
        # so we must extract the string name
        scale_name = scale_names[scale]

        # select the scale and plot
        arr = post(data[scale])
        im = ax.pcolormesh(arr.x / 1e6, arr.time, arr.data, **plot_kwargs)

        # improve the axes
        ax.set_xlabel('x [1000 km]')
        ax.set_title(f"{letter}) {scale_name}", color=title_color, loc="left")
        ax.xaxis.set_major_locator(plt.MaxNLocator(4))
        ax.yaxis.set_major_locator(plt.MaxNLocator(10))
    #     rotate_xtick_labels(ax, 45)

    axs[0].set_ylabel('time [days]')
    cb = plt.colorbar(im, ax=list(axs.flat), pad=.02)
    
    return axs, cb


def plot_eddy_flux_snapshot(eddy_fluxconv):
    """Plot the planetary scale eddy flux divergences for U, B, and QV
    """

    p = eddy_fluxconv.p

    # make kwargs for the fields
    # this is where you control the appearance of the plots
    field_kwargs = {}
    # field_kwargs = {
    #     'WUz': dict(cmap='bwr', vmin=-6, vmax=6),
    #     'WBz': dict(cmap='bwr'),
    #     'WQVz': dict(cmap='bwr', vmin=-6, vmax=6),
    # }

    # plot the eddy flux convergencs
    fig, axs = plt.subplots(3, 1, figsize=(7, 6), sharex=True, sharey=True)
    for ax, field in zip(axs.flat, ['WUz', 'WBz', 'WQVz']):
        # load kwargs for field

        kwargs = field_kwargs.get(field, {})
        arr = eddy_fluxconv[field]
        im = ax.pcolormesh(arr.x / 1e3, p, arr, **kwargs)
        plt.colorbar(im, ax=ax)

        # add label for vertical axis
        ax.set_ylabel('p [mb]')
        # ax.set_title(f"{field} [{units}]")

    # invert for pressure plots
    ax.invert_yaxis()
    ax.set_xlabel('x [km]')
    ax.set_yticks([1000, 750, 500, 250])

    plt.suptitle("Synoptic-scale eddy flux divergence")


def nc_plot_u_msdecomp_line(filename, ax=None, **kwargs):
    """Plot the multiscale decomposition of the u field at 3000 m
    """

    if ax is None:
        _, ax = plt.subplots(figsize=(7, 3))

    U = xr.open_dataarray(filename, chunks={'time': 1})\
          .pipe(lambda x: coarsen(x, x=32))\
          .sel(time=80, method='nearest')\
          .compute()

    u = U.sel(z=3000, method='nearest')
    return plot_line_filters(u, ax=ax, dofs=(6, 12, 24, 48), **kwargs)


def nc_plot_2d_msdecomp(filename, **kwargs):
    tb = xr.open_dataarray(filename)
    tb = coarsen(tb, x=32)
    return plot_2d_msdecomp(tb, **kwargs)


def nc_plot_spectrum(filename, **kwargs):
    tb = xr.open_dataarray(filename)
    tb = coarsen(tb, x=32).compute()
    pw = compute_space_time_spec(tb, )

    return plot_spectrum(pw, **kwargs), pw


def nc_plot_hovmoller(filename, ax=None, **kwargs):
    tb = xr.open_dataarray(filename)
    tb = coarsen(tb, x=16).compute()
    tb['x'] /= 1e6
    return tb.plot(ax=ax, **kwargs)


def plot_spectrum(pw, ax=None, add_labels=True, annotate=True, **kwargs):

    if ax is None:
        _, ax = plt.subplots()

    pw = pw.sel(om=slice(0, 1.2), k=slice(-30, 30))
    L = pw.n * pw.dx

    k, om, pw = pw.k, pw.om, pw.data

    plot_kwargs = dict(cmap='hot_r', norm=LogNorm(), shading='gouraud')
    plot_kwargs.update(kwargs)

    im = ax.pcolormesh(k, om, pw, **plot_kwargs)
    ax.set_ylim([om.min(), om.max()])
    ax.set_xlim([k.min(), k.max()])
    # plt.pcolormesh(k, om, np.log(pw))

    # add lines for dispersion relatship
    # om/k=c , om = k c,
    # plt.plot((0, 0), (0, ))

    for c in [5, 10, 25, 50]:
        ax.plot((0, -50), (0, 50 * c * 86400 / L), 'k--', alpha=.5)
        ax.plot((0, 50), (0, 50 * c * 86400 / L), 'k--', alpha=.5)

        # labels for reference curves
        x = {5: 12, 10: 9, 25: 6, 50: 3.5}[c]
        y = x * c * 86400 / L

        if annotate:
            ax.annotate(
                f"{c} m/s", (x, y), bbox=dict(fill='full', color='white'))

    if add_labels:
        # plt.colorbar(im, ax=ax)
        ax.set_xlabel('Zonal wave number')
        ax.set_ylabel('Cycles per day')

    return ax, im
