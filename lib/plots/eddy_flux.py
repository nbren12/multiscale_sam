import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

import xarray as xr

from ..filter import eddy_flux, lowpass
from ..material import anelastic_eddy_flux_div, anelastic_vertdiv


def compute_eddy_fluxconv(data, rho=None):
    data_vars = {
        'W' + field + 'z': -anelastic_vertdiv(eddy_flux(data.W, data[field]),
                                              rho) * 86400
        for field in ['U', 'B', 'QV']
    }
    return xr.Dataset(data_vars)


def plot_eddy_flux(*args,
                   field_levels=None,
                   title=None,
                   ax=None,
                   fluxdiv_levels=None,
                   clabel=''):

    def smooth(f):
        return lowpass(f, dof=6)

    u, w, rho, p = args

    # _, wp = last(decomp(w, dofs=(6, 26)))
    # _, up = last(decomp(u, dofs=(6, 26)))

    wp = w - smooth(w)
    up = u - smooth(u)

    ef = smooth(-anelastic_eddy_flux_div(wp, up, rho))
    ef = ef - ef.mean('x')

    if not ax:
        fig, ax = plt.subplots(1, 1, figsize=(3.5, 6), sharey=True)

    im = ax.contourf(ef.x, p, ef.T * 86400, levels=fluxdiv_levels, cmap='bwr')
    cb = plt.colorbar(im, pad=.01, ax=ax)
    cb.set_label(clabel)

    ax.contour(ef.x, p, lowpass(u) - u.mean('x'), levels=field_levels,
               colors='black')
    # plt.clabel(cs);
    plt.ylim([1000, 100])

    if title is None:
        title = u.name

    # xaxis
    ticks = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x / 1e6))
    ax.xaxis.set_major_formatter(ticks)

    # y axis
    ax.set_ylabel('p (hPa)')
    ax.set_ylim([p.max(), p.min()])

    return ax


def plot_eddy_flux_key(key, data, **kwargs):
    return plot_eddy_flux(data[key], data.W, data.RHO, data.p, **kwargs)


def plot(data):
    """Plot Eddy Flux convergence and smooothed fields

    Parameters
    ----------
    data : xr.Dataset
        This should be a single time snapshot with the following fields:
        U, QV, B, RHO, and p.
    """
    # data = get_data(**kwargs)
    fig, axs = plt.subplots(
        3, 1, figsize=(3.5, 5), sharex=True, sharey=True, dpi=100)

    plot_eddy_flux_key(
        'U',
        data,
        ax=axs[0],
        fluxdiv_levels=np.arange(-5, 6) * 1,
        field_levels=np.arange(-5, 6) * 5,
        clabel='m/s/day')

    plot_eddy_flux_key(
        'QV',
        data,
        ax=axs[1],
        title=r'$q_v$',
        fluxdiv_levels=np.arange(-5, 6) * .5,
        field_levels=np.arange(-5, 6) * .5,
        clabel='g/kg/day')

    plot_eddy_flux_key(
        'B',
        data,
        ax=axs[2],
        fluxdiv_levels=np.arange(-5, 6) * .02,
        field_levels=np.arange(-10, 10) * .05,
        clabel='m/s$^2$/day')

    axs[0].set_title(r"a) U (m/s)", loc="left")
    axs[1].set_title(r"b) $q_v$ (g/kg) ", loc="left")
    axs[2].set_title(r"c) B (m/s$^2$)", loc="left")
    axs[-1].set_xlabel('x (1000 km)')

    plt.tight_layout()
