import xarray as xr
import matplotlib.pyplot as plt
from xnoah import copy_attrs, swap_coord
from lib.thermo import calc_n2


def get_clim_mean(path):
    stat_orig = xr.open_dataset(path)

    stat = stat_orig.sel(time=slice(40, None)).mean('time')
    stat = stat.assign(N2=calc_n2(stat.TABS))
    stat = swap_coord(stat, {'z': 'p'})
    copy_attrs(stat_orig, stat)

    # combine the TTEND and the radiation
    qrad = stat.TTEND + stat.RADQR
    stat['QRAD'] = qrad.assign_attrs(units='K/d')
    return stat

def _adjust_spines(ax):
    ax.spines['top'].set_color('none')
    ax.spines['right'].set_color('none')

def plot_profiles(stat, axs=None, **kwargs):
    if axs is None:
        fig, axs = plt.subplots(1, 4, sharey=True, constrained_layout=True)

    plot_kwargs = dict(marker='')
    plot_kwargs.update(kwargs)

    letters = 'abcdefghi'

    for k, var in enumerate([stat.QRAD, stat.TABS, stat.N2, stat.QV,
                             stat.U]):

        axs[k].plot(var, var.p, **plot_kwargs)
        if var.name == 'N2':
            axs[k].set_xscale('log')
            axs[k].set_xticks([1e-6, 1e-5, 1e-4, 1e-3, 1e-2])
        else:
            axs[k].xaxis.set_major_locator(plt.MaxNLocator(4))

        name = {
            'QRAD': r'$Q_{rad}$',
            'TABS': r'$T$',
            'N2': r'$N^2$',
            'QV': r'$q_v$',
            'QN': r'$q_c$',
            'U': 'u'
        }[var.name]

        axs[k].set_title(f'{letters[k]}) {name}', loc="left")
        axs[k].set_xlabel(f'${var.units}$')


    return axs


def nc_plot_profiles(path, **kwargs):
    stat = get_clim_mean(path).sel(p=slice(50))
    return plot_profiles(stat, **kwargs)


def plot(arg):
    fig, axs = plt.subplots(1, 5, figsize=(5.5, 2.8), sharey=True, constrained_layout=True)

    for (title, path) in arg:
        nc_plot_profiles(path, axs=axs.flat, label=title)

    for ax in axs.flat:
        _adjust_spines(ax)

    # labels and styling
    if not axs[0].yaxis_inverted():
        axs[0].invert_yaxis()
    axs[0].set_ylabel('p [mb]')

    axs[-1].legend(loc=(1.1, .7))
    # plt.subplots_adjust(hspace=.4)

    for ax in axs.flat:
        plt.setp(ax.xaxis.get_majorticklabels(), rotation=60)

