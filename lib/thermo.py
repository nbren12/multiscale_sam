"""Thermodynamic calculations
"""
import numpy as np
import xarray as xr
from scipy.interpolate import UnivariateSpline
from metpy.constants import g as grav, Cp_d, Rd, Lv
from metpy.units import units

Q_ = units.Quantity
kappa = (Rd / Cp_d).to("1").magnitude
gr = grav.magnitude

gamma = (grav / Cp_d).to("K/m").magnitude
kappa = 287 / 1004

Lvcp = (Lv/Cp_d).to("K / (g/kg)").magnitude


def scales(L, Ht=16e3, N=.01):
    H = Ht / pi
    c = N * H
    T = L / c
    W = H / T
    B = N * N * H
    PSI = c * H
    return dict(L=L, H=H, N=N, c=c, T=T, W=W, B=B, PSI=PSI)


def calc_thv0(T, qv, p):
    return T * (1 + .622 * qv / 1000) * (1000 / p)**kappa


def calc_b(t):
    t0 = t.sel(time=slice(-50, None)).mean(['time', 'x'])
    b = gr * (t - t0)/t0
    return b

def calc_T(th, p):
    return th * (p / 1000)**kappa


def calc_mse(t, qv):
    cp = Cp_d.to("J/kg/K").magnitude
    lv = Lv.to("J/g").magnitude

    return t* cp + lv * qv + gr * t.z


def calc_dse(t):
    return t*cp + g*r * t.z


def calc_n2(T):
    z = T.z
    spl = UnivariateSpline(z, T, s=0).derivative(1)

    n2 = gr * (gamma + spl(z)) / T
    n2.name = 'N2'
    n2.attrs['units'] = "1/s^2"

    return n2


def calc_stream(u, adz):
    psi = (adz * u).cumsum('z')
    psi -= psi.isel(z=0)

    return psi


def calc_h(dbdt, n2, w, th, p, qrad):
    """
    Parameters
    ----------
    qrad [K / day]


    Returns
    -------
    H [m/s^2/day]
    """

    T = th * (p / 1000)**kappa

    H = (dbdt + n2 * w - gr / T * qrad / 86400) * 86400
    H = (dbdt + n2 * w) * 86400
    H.attrs['units'] = " m/s^2/d"

    return H


def calc_layer_mass(rho):
    z = rho.z.values
    zh = np.r_[0, (z[1:] + z[:-1]) / 2, z[-1] * 1.5 - .5 * z[-2]]
    return xr.DataArray(np.diff(zh) * rho.values, dims='z', coords={'z': z})


def virtual_temperature(tabs, qv):
    return tabs * (1 + .61 * qv / 1000)


def moist_static_energy(tabs, qv):
    return tabs + gamma * tabs.z + Lvcp * qv


def mass_weighted_integral(f, rho, dim='z'):
    """Compute the mass weighted integral
    """
    drho = calc_layer_mass(rho)
    return (f*drho).sum(dim)

