#!/usr/bin/env python
import click
import netCDF4 as nc


def patch_z_data(input, correct, varname):

    infile = nc.Dataset(input, "r+")
    correctfile = nc.Dataset(correct, "r")

    correct_data = correctfile.variables[varname][:]
    infile.variables[varname][:] = correct_data

    # close files
    correctfile.close()
    infile.close()


@click.command()
@click.argument("input")
@click.argument("correct")
@click.option("--varname", "-v", default='z')
def main(*args, **kwargs):
    patch_z_data(*args, **kwargs)


if __name__ == '__main__':
    main()
