#!/usr/bin/env python
"""Multiscale decomposition using penalized fourier method
"""
import sys
import time
from functools import partial

import numpy as np
import pandas as pd
import xarray as xr

from xnoah.xarray import coarsen


def compute_penal(n, dof, p=4.0):
    """Compute penalization coefficent of low-pass filter

    This function returns the operator resulting from applying the the filter
        f(k) = 1/(1+exp^a k^p)
    in spectral space.

    Parameters
    ----------
    n: int
        number of spatial grid points
    dof: int
        number of degrees of freedom
    p: float, optional
        power to be used in filter (default: p=6)

    Returns
    -------
    f: np.ndarray
         filter to apply in fourier space
    a: float
         penalization coefficent

    """
    import scipy.fftpack as fft
    from scipy.optimize import fsolve

    I = np.eye(n)
    F = fft.fft(I, axis=-1)
    IF = fft.ifft(I, axis=-1)
    k = fft.fftfreq(n, d=1 / n)

    def filt(a, p):
        # use exp(a) for numerical purposes
        return 1 / (1 + np.exp(a) * np.abs(k)**p)

    def f(a):
        N = filt(a, p)
        return np.sum(N) - dof

    a = fsolve(f, 0)
    return filt(a, p), float(a)


def fourier_deriv(n, d):
    return 2 * np.pi * 1j * np.fft.fftfreq(n,d=d)


def lowpass(A, dim='x', dof=6, order=0, d=1.0):

    # dimension information
    x = A.coords[dim]
    xnum = A.get_axis_num(dim)
    n = len(x)

    # compute filter with proper shape
    f, _ = compute_penal(n, dof)
    # take derivative
    ik = fourier_deriv(n, d)
    f = f * ik**order
    f.shape = [n if dim =='x' else 1 for dim in A.dims]

    # # prep data
    # if A.chunks is None:
    #     da = np


    fA = np.fft.fft(A.data, axis=xnum)
    iA = np.real(np.fft.ifft(fA * f, axis=xnum))

    return xr.DataArray(iA, dims=A.dims, coords=A.coords)


def msdecomp_up(a):
    """multiscale decomposition starting with smallest scale"""
    px_1 = lowpass(a, 'x', 40)
    px_2 = lowpass(px_1, 'x', 10)

    ind = pd.Index(['p', 's', 'm'], name='scale')
    return xr.concat([px_2, px_1 - px_2, a - px_1], dim=ind)


def msdecomp_down(a, n=(6, 20), demean=False):
    """multiscale decomposition starting with largest scale"""

    if demean:
        a = a - a.mean('x')

    px_1 = lowpass(a, 'x', n[0])
    a = a - px_1
    px_2 = lowpass(a, 'x', n[1])
    a = a - px_2

    ind = pd.Index(['Planetary', 'Synoptic', 'Mesoscale'], name='scale')
    return xr.concat([px_1, px_2, a], dim=ind)


def quadratic(u, w, uw, P=None):
    """ Perform multiscale analysis of uw
    """
    ub = P(u)
    up = u - ub
    wb = P(w)
    wp = w - wb

    # quadratic terms
    uw_b = P(uw)
    r = P(ub * wb)
    m = P(up * wp)

    return xr.Dataset({"tot": uw_b, "large": r, "small": m})


def eddy_flux(w, u, planetary_dof=6):
    """Compute eddy flux term of two terms using the planetary scale equations

    This method can optionally coarsen the data before performing the filtering. This option is useful for large datasets.
    """
    # u = coarsen(u, x=coarsening).compute()
    # w = coarsen(w, x=coarsening).compute()

    up = u - lowpass(u, 'x', planetary_dof)
    wp = w - lowpass(w, 'x', planetary_dof)

    arr = lowpass(up * wp, 'x', planetary_dof)
    # add some metadata
    arr.attrs['units'] = u.units.strip() + '/s'

    return arr
