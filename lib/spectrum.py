import numpy as np
import xarray as xr
from scipy.fftpack import fft2, fftfreq, fftshift
from scipy.ndimage import gaussian_filter


def compute_space_time_spec(tb):

    arr = tb.data - tb.data.mean()
    dt = float(tb.time[1] - tb.time[0])
    dx = float(tb.x[1] - tb.x[0])

    nt, nx = arr.shape

    om = fftfreq(nt, d=dt)
    k = fftfreq(nx, d=1/nx)
    fk = fft2(arr)
    pw = np.real(fk * fk.conj())

    # smooth the spectra
    pw = np.exp(gaussian_filter(np.log(pw), [1.5, 1.5]))

    # fftshift the data
    k = fftshift(k)
    pw = fftshift(pw, axes=(1,))

    # flip the data
    # +w t + k x is westward
    # +w t - kx is eastward
    pw = pw[:, ::-1]

    # om = fftshift(om)
    # pw = fftshift(pw, axes=(0,1))

    # remove planetary scale
    pw = pw[:,k!=0]
    k = k[k!=0]

    # use only positive time-frequency components
    inds = om >=0
    om = om[inds]
    pw = 2 *pw[inds,:]

    da = xr.DataArray(pw, dims=['om', 'k'],
                      coords={'k': k, 'om':om})

    da.attrs['dx'] = dx
    da.attrs['dt'] = dt
    da.attrs['n'] = nx

    return da
    # return k, om, pw
