import numpy as np
import pytest

import xarray as xr

from .budget import decomp, tracer_budget


def assert_xr_close(a, b):
    a, b = xr.broadcast(a, b)
    np.testing.assert_allclose(a.values, b.values)


@pytest.mark.parametrize('dofs', [(3, 24), (24, 3)])
def test_decomp(dofs):

    nx = 85
    ny = 20
    x = np.r_[:nx]
    da = xr.DataArray(np.random.rand(nx, ny), dims=['x', 'y'], coords={'x': x})

    # the sum of the compoents should give the original value
    vals = [val for key, val in decomp(da, dofs=dofs)]
    vals_sum = sum(vals)
    assert_xr_close(vals_sum, da)


def test_tracer_budget():

    # initialize mock arrays
    nx = 85
    ny = 10
    x = np.r_[:nx]
    a, b, c = [
        xr.DataArray(np.random.rand(nx, ny), dims=['x', 'y'], coords={
            'x': x
        }) for _ in range(3)
    ]

    src = -b - c

    # Assert L(a*b + a*c + a*d) = 0 if b + c + d = 0
    terms = [
        ('b', b, {}),
        ('c', c, {}),
        ('src', src, {}),
    ]

    ds = tracer_budget(a, extra_terms=terms)
    ans = ds.b + ds.c + ds.src
    np.testing.assert_array_almost_equal(ans.values, 0)
