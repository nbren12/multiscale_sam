import itertools
from toolz import curry
import attr
import xarray as xr
from xnoah.xcalc import centderiv
from .filter import lowpass
from .material import anelastic_vertdiv
from .thermo import mass_weighted_integral
from tqdm import tqdm
import pandas as pd

import logging

logger = logging.getLogger(__name__)

planetary_dof = 3


def smooth(f, **kwargs):
    dx = float(f.x[1] - f.x[0])
    return lowpass(f, d=dx, **kwargs)


@curry
def project(f, **kwargs):
    if 'x' in f.dims:
        return smooth(f, **kwargs) - f.mean('x')
    else:
        return 0 * f


@curry
def vertical_average(f, rho):
    return mass_weighted_integral(f, rho)/mass_weighted_integral(1, rho)


def decomp(f, dofs=(), **kwargs):
    """Decompose a variables using filter banks

    Parameters
    ----------
    f : xr.DataArray
        input data array
    dofs : seq of int
        tuple containing filter degrees of freedom to apply
    **kwargs :
        passed to `smooth`

    Yields
    ------
    key, val :
        multiscale decomposition. total number is len(dofs) + 2
    """
    mean = f.mean('x')

    yield 'mean', mean

    resid = f - mean
    for dof in dofs:
        x = smooth(resid, dof=dof, **kwargs)
        yield dof, x
        resid -= x

    yield 'resid', resid


def triad_interactions(w, f, dofs=(planetary_dof,)):
    """Yield the triad interactions of wf
    """
    # compute decompositions
    w_decomp = decomp(w, dofs=dofs)
    f_decomp = decomp(f, dofs=dofs)

    for (wscale,  wval), (fscale, fval) in itertools.product(w_decomp, f_decomp):
        fw = fval * wval
        if 'x' in fw.dims:
            for scale, val in decomp(fw, dofs=dofs):
                yield (scale, wscale, fscale), val
        else:
            # the (mean, mean) interaction has not spatial dependence
            yield ('mean', wscale, fscale), fw


def decompose_vertical_advection(w, f, dofs=(planetary_dof,)):
    """Decompose the vertical advection term into its multiscale contributions

    """

    fz = centderiv(f, dim='z', boundary='reflect')
    fz_decomp = dict(decomp(fz, dofs=dofs))

    n = (len(dofs) + 2)**3

    for key, wf in tqdm(triad_interactions(w, f, dofs=dofs),
                        total=n):
        scale = key[0]
        yield key, fz_decomp[scale] * wf


def _mean_budgets_to_df(budget):
    dof_name_map = {'mean': 'Mean', planetary_dof: 'P', 26: 'S', 'resid': 'M'}
    output = []
    for key, val in budget.items():
        means = {
            'target': dof_name_map[key[0]],
            'W Scale': dof_name_map[key[1]],
            'U Scale': dof_name_map[key[2]],
            'value': val
        }
        output.append(means)
    return pd.DataFrame.from_records(output)


def compute_vertical_triads(rho, w, u, dofs=(planetary_dof, 26)):
    triads = decompose_vertical_advection(w, u, dofs=dofs)

    def allmean(val):
        avg =  vertical_average(val, rho)
        allavg = avg.mean(set(val.dims) & set(['x', 'time']) )
        return float(allavg)

    mean_triads = {key: allmean(val)*86400 for key, val in triads}
    return _mean_budgets_to_df(mean_triads)


def _yield_terms_with_names(term_list, **kwargs):

    decomps = []
    names = []
    for name, val, decomp_kwargs in term_list:
        kw = {}
        kw.update(decomp_kwargs)
        kw.update(kwargs)
        decomps.append(decomp(val, **kw))
        names.append(name)

    for terms in zip(*decomps):
        # terms is list of (scale, name, val) tuples
        scale = ""
        term_dict = {}
        for name, (cur_scale, val) in zip(names, terms):
            if not scale:
                scale = cur_scale
            elif cur_scale != scale:
                raise ValueError
            term_dict[name] = val
        yield scale, term_dict


def product(x, y, dofs=(planetary_dof, 26)):

    x_decomp = decomp(x, dofs)
    y_decomp = decomp(y, dofs)

    output = {}
    for (scale_x, xa), (scale_y, ya) in zip(x_decomp, y_decomp):
        if scale_x != scale_y:
            raise ValueError

        prod = (xa * ya)
        if scale_x != 'mean':
            prod = prod.mean('x')
        output[str(scale_x)] = prod

    return xr.Dataset(output).to_array('scale')


def tracer_budget(f,
                  extra_terms=(),
                  planetary_dof=planetary_dof,
                  synoptic_dof=26):
    """Compute the large-scale kinetic energy budget from basic inputs

    Parameters
    ----------
    u : (m/s)
        the horizontal velocity
    w : (m/s)
        the vertical velocity
    f :
        the field to advect around
    extra_terms : list ot tuple
         list of tuples with the form (name, val, kwargs)
         and `kwargs` is passed on to `decomp`.
    planetary_dof : int
        number of degrees of freedom to use for the smoothing operation

    Notes
    -----
    See Eq. 10 of the paper.
    """
    dofs = (planetary_dof, synoptic_dof)

    budgets = []

    def average(x):
        # x = vertical_average(x, rho)
        if 'x' in x.dims:
            return x.mean('x')
        else:
            return x

    term_list = [
        ('f', f, {}),
    ]

    term_list.extend(extra_terms)

    for scale, terms in _yield_terms_with_names(term_list, dofs=dofs):
        logger.info(f"Processing scale {scale}")
        # extract the f value
        f_scale = terms.pop('f')
        # individual terms
        scale_budget = {key: average(f_scale * x) for key, x in terms.items()}
        scale_budget['TOT'] = average(f_scale**2/2)
        scale_budget = xr.Dataset(scale_budget)\
                         .assign_coords(scale=str(scale))\
                         .expand_dims('scale')

        budgets.append(scale_budget)

    return xr.merge(budgets)


def compute_budget(u, **kwargs):
    """A wrapper for tracer budget that takes keyword arguments"""
    terms = []
    for key, val in kwargs.items():
        terms.append((key, val, {}))

    return tracer_budget(u, extra_terms=terms)


def compute_integrated_feedbacks(budget):
    """Integrate the terms in budget forward in time

    Also computes residual.
    """
    time = budget.time
    dt = (time[1] - time[0]) * 86400

    integrated = budget.cumsum('time') * dt
    integrated_feedbacks = integrated.drop('TOT').to_dataframe()
    integrated_feedbacks.index = time.data

    resid = budget.TOT - integrated_feedbacks.sum(axis=1)
    # add back in TOT and resid before returning
    return integrated_feedbacks.assign(TOT=budget.TOT, SRC=resid)


def cumulative_budget(ke_budget):
    drop_vars = ['TOT']

    ds = ke_budget.drop(drop_vars)
    dt= 86400 * (ke_budget.time[1] - ke_budget.time[0])
    cum = ds.cumsum(dim='time')*dt
    cum =  cum.assign_coords(time=ke_budget.time)

    # compute residual
    known_src_sum = cum.to_array(dim='var').sum('var')
    cum['SRC'] = ke_budget.TOT - known_src_sum - ke_budget.TOT.isel(time=0)

    return cum


def melt(ds):
    """Melt a dataset into a long-format dataframe

    This dataframe is suitable for plotting in libraries like seaborn.
    """
    df =  ds.to_array(name='value').to_dataframe()

    # remove coordinates which ended up in columns
    for x in df.columns:
        if x in ds.coords:
            df = df.drop(x, axis=1)

    return df.reset_index()
