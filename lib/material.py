"""Functions for computing the material derivative
"""
import numpy as np
import xarray as xr
from xnoah.xcalc import centderiv


def anelastic_vertdiv(fz, rho):
    """Compute vertical divergence in anelastic framework"""
    if rho.ndim > 1:
        raise ValueError(f"rho should have 1 dimension got {rho.ndim}")
    return centderiv(rho * fz, dim='z', boundary='nearest') / rho


def matderiv(u, w, f):
    """Compute the material derivative using centered differences """

    assert f.time.units == 'd'

    dx = centderiv(f, 'x', boundary='periodic')
    dz = centderiv(f, 'z', boundary='nearest')
    dt = centderiv(f, 'time', boundary='nearest') / 86400.0
    return dt + u * dx + w * dz


from lib.filter import lowpass


def get_dz(z):
    zext = np.hstack((-z[0], z, 2.0 * z[-1] - 1.0 * z[-2]))
    zw = .5 * (zext[1:] + zext[:-1])
    dz = zw[1:] - zw[:-1]

    return dz


def eddy_flux_div_np(w, u, rho, z):
    """Use first order upwinding to compute the derivative"""
    dz = get_dz(z)

    f = u * rho
    # pad the data
    pad = [(0, 0)] * f.ndim
    pad[-1] = (1, 2)
    f = np.pad(f, pad, 'edge')

    pad = [(0, 0)] * w.ndim
    pad[-1] = (0, 1)
    w = np.pad(w, pad, 'constant')

    f_int = (f[..., 1:] + f[..., :-1]) / 2

    F = np.maximum(w, 0.0) * f_int[..., :-1] + np.minimum(w,
                                                          0) * f_int[..., 1:]

    return np.diff(F, axis=-1) / dz / rho


def anelastic_eddy_flux_div(w, u, rho):
    return xr.apply_ufunc(
        eddy_flux_div_np,
        w,
        u,
        rho,
        rho.z,
        input_core_dims=[['z']] * 4,
        output_core_dims=[['z']])
