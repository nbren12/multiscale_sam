import xarray as xr
import numpy as np
from xnoah import centderiv
from .thermo import calc_layer_mass


def diff_np(u):
    return np.roll(u, -1, axis=-1) - u


def diff(u, dim):
    return xr.apply_ufunc(
        diff_np,
        u,
        input_core_dims=[[dim]],
        output_core_dims=[[dim]],
        dask='allowed')


def divergences(u, f, rhowf, rho):
    layer_mass = calc_layer_mass(rho)
    VDIV = diff(rhowf, 'z')/layer_mass
    HDIV = centderiv(u*f, 'x')
    return VDIV, HDIV
